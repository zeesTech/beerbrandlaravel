<?php

use Illuminate\Database\Seeder;

class maltStyleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('malt_style')->insert([
            ['name' => 	'TRADITIONAL'],
            ['name' => 	'UNIQUE'],
            ['name' => 	'EXPERIMENTAL']
        ]);
    }
}
