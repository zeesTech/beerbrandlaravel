@extends('layouts.app')

@section('content')

    <link href="asset/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="asset/fonts/fontello/css/fontello.css" rel="stylesheet" type="text/css">
    <link href="asset/css/global.css" rel="stylesheet" type="text/css">
	
	<style>
	.Section-UserProfile{
		padding: 0px 0 0 0 !important;
	}
	.container, .container-fluid, .container-sm, .container-md, .container-lg, .container-xl {
		padding-left: 0.5rem !important;
	}
	
	div.ChatDropdown-List {
		width: 291px;
		height: 700px;
		overflow: auto;
	}
	
	div.UserMessage-Section {
		width: 100%;
		height: 600px;
		overflow: auto;
	}
	
	</style>
<div class="container-fluid">

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Messages</h6>
  </div>
  <div class="card-body">

   <section class="Section-UserProfile">
   	<div class="container-fluid pl-5 pr-5">
   	 <div class="row">
   	 
	  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
	   <div class="ChatLeft-Container">
	   	
	   	<div class="ChatSearch">
	   	 <!--<i class="fa fa-search"></i>
		 <input type="text" id="" name="" placeholder="Search...">
	   	 <button class="addNewMessage"><img src="asset/image/icon-plus.svg"></button>-->
		 <button style="width: 100%;font-weight: bold;">Messages</button>
	   	</div>
	   	
		<div class="ChatDropdown-List">
			<ul>
				<?php
					if(!empty($data) && isset($data['contactList']) && !empty($data['contactList']))
					{
						$contactListArray = array();
						foreach($data['contactList'] as $key => $contactList)
						{
							if(in_array($contactList['id'],$contactListArray))
								continue;
							
							$className = (empty($contactListArray)) ? "active" : "";
							
							$contactListArray[] = $contactList['id'];
				?>	
							<li class="{{$className}}" data-id="{{$contactList['id']}}" id="li{{$contactList['id']}}">
								<a href="javascript:void(0)" onclick="loadMessage({{$contactList['id']}})">
									<div class="ChatDropdown-Head">
										<img src="assets/logo.png" width="36" height="36" alt=""/>
										<h1><?php echo $contactList['name'];?> <br><span><?php echo substr($contactList['message'],0,25);?></span></h1>
									</div>
								</a>
							</li>
				<?php
						}
					}
					else
					{
				?>
						<li class="emptyContactList">
							<a href="#">
								<div class="ChatDropdown-Head">
									<h1><span>Empty Contact List</span></h1>
								</div>
							</a>
						</li>
				<?php
					}
				?>
				
				
				<!--<li class="">
					<a href="#">
						<div class="ChatDropdown-Head">
							<img src="asset/image/default-image.png" width="36" height="36" alt=""/>
							<h1>Jeannette Shelton <br><span>Hi John, Wants to talk to you</span></h1>
						</div>
					</a>
				</li>-->
			</ul>
	   	</div>
	   	
	   </div>
	  </div>
	  	
	  <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
	   <div class="ChatRight-Container">
	   	
	   	<div class="ChatRight-TopCon">
	   	 
	   	 <div class="ChatLeft-Head">
	   	  <img src="asset/image/finance-that-tag.svg" alt=""/>
	   	  <h1>{{ Auth::user()->name }}<br> 
	   	  <small id="last_message_time"><?php echo date("h:i A")?></small></h1>
	   	 </div>
	   	</div>
	   	
	   	<div class="UserMessage-Section">
			
			<!--
			<div class="UserChat-Left">
				<img src="assets/logo.png" width="70" height="70" alt=""/>
				<div class="InnerChat-Head">
					<h1>Is this Car still available?</h1>
					<h2>10:25 AM</h2>
				</div>
			</div>
			
			<div class="UserChat-Left">
				<img src="assets/logo.png" width="70" height="70" alt=""/>
				<div class="InnerChat-Head">
					<h1>I’m interested in this property.</h1>
					<h2>10:25 AM</h2>
				</div>
			</div>
	   	 
			<div class="UserChat-Right">
				<img src="asset/image/default-image.png" width="70" height="70" alt=""/>  
				<div class="InnerReply-Head">
					<h1>Yes, It is available. How can I help you?</h1>
					<h2>10:25 AM</h2>
				</div>
			</div>
	   	 
			<div class="UserChat-Right">
				<img src="asset/image/default-image.png" width="70" height="70" alt=""/>
				<div class="InnerReply-Head">
					<h1>Yes,</h1>
					<h2>10:25 AM</h2>
				</div>
			</div>

			<div class="UserChat-Right">
				<img src="asset/image/default-image.png" width="70" height="70" alt=""/>
				<div class="InnerReply-Head">
					<img src="asset/image/chat-img-4.png" width="198" height="128" alt=""/>
					<h2>10:25 AM</h2>
				</div>
			</div>-->
	   	 
	   	</div>
	   	
	   	<form action="javascript:void(0)" method="post" id="message_form">
	   	 <div class="row">
	   		
	   	  <div class="Emojifile-List">
	   	   <ul>
	   	  	   	   	
	   	   	<!--<li>
	   	   	  	
	   	   	 <div class="image-upload">
    		  <label for="file-input">
        	   <i class="icon-file-attech"></i>
    		  </label>

    		  <input id="file-input" type="file" accept=".png , .jpg , .jpeg , .gif "  onchange="readURL(this)"/>
		     </div>
	   	   	  	
	   	    </li>-->
	   	   	  
	   	   </ul>
	   	  </div>
	   	   
	   	  <div class="ChatMessage-Search">
			<!--<img id="loadAttachment"/>-->
	   	   <input type="hidden" id="revicerID" name="revicerID" >
	   	   <input type="text" class="message-input" id="message-input" name="message-input" placeholder="Write your message">
	   	  </div>
	   	  <div class="ChatMessage-Btn">
		  
	   	   <button class="submit">Send</button>
	   	  </div>
	   	 
	   	 </div>
	   	</form>
	   	
	   </div>
	  </div> 	
		   	      	   
   	 </div>
   	</div>
   </section>
      


  </div>
</div>

</div>

	<div class="modal fade in" id="addNewMessagePopup" data-backdrop="static">
	  <div class="modal-dialog modal-md">
		<div class="modal-content">
			<form action="javascript:void(0)" id="addNewMessageform" method="POST" enctype="multipart/form-data">
			  <div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Create Message</h4>
				<button type="button" class="close" onclick="resetMessageform()" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <div class="modal-body">
				<div class="row">
					<div class="col-md-12" >
						<div class="form-group" >
							<select class="form-control" name="userID" id="userID">
								<option value="">Select Contact</option>
								<?php
									if(!empty($data) && isset($data['user']) && !empty($data['user']))
									{
										foreach($data['user'] as $index => $user)
										{
								?>
										<option value="<?php echo $user['id']; ?>" ><?php echo $user['name']; ?></option>
								<?php
										}
									}
								?>
							</select>
						</div>
                    </div>
				</div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="customAdd-Btn" id="saveBtn" onclick="CreateMessage()">Create</button>
				<button type="reset" class="customClose-Btn" onclick="resetMessageform()">Close</button>
			  </div>
			</div>
		</form>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>
	
<script>
var  scrollPostion = "150000";

jQuery(document).ready(function (){
	$('.ChatDropdown-List > ul > li.active > a').trigger('click');
	//window.scrollTo(-100,document.body.scrollHeight);
	window.scrollTo(0, 170);
	setInterval(function(){ 
		loadNewMessages(); 
	},5000);
});

function newMessage() {
	var message = $(".message-input").val();
	if($.trim(message) == '') {
		return false;
	}
		
	var time = formatAMPM(new Date());

	var messageHtml = '<div class="UserChat-Left">'+
			  '<img src="asset/image/default-image.png" width="70" height="70" alt=""/>'+
			  '<div class="InnerChat-Head">'+
			   '<h1>'+message+'</h1>'+
			   '<h2>'+time+'</h2>'+
			  '</div>'+
	   	 '</div>';
	
	$(".UserMessage-Section").append(messageHtml);
	$(".UserMessage-Section").animate({ scrollTop: scrollPostion }, "fast");
	$('.ChatDropdown-List > ul > li.active > a > div.ChatDropdown-Head > h1 > span').html(message.substring(0, 25));
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var formData = $("#message_form").serialize();
	var type = "POST";
	var ajaxurl= "/saveMessageNew";
	
	$('.message-input').val(null);
	
	$.ajax({
		type: type,
		url: ajaxurl,
		data: formData,
		dataType: 'json',
		success: function (data) {
							
		},
		error: function (data) {
			console.log(data);
		}
	});
	
};

$('.submit').click(function() {
	newMessage();
});

$(window).on('keydown', function(e) {
  if (e.which == 13) {
    newMessage();
    return false;
  }
});

function readURL(input) 
{	
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#loadAttachment').attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}

function formatAMPM(date) {
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var ampm = hours >= 12 ? 'PM' : 'AM';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0'+minutes : minutes;
	var strTime = hours + ':' + minutes + ' ' + ampm;
	return strTime;
}

$(".ChatDropdown-List ul li").click(function() {
	$(".ChatDropdown-List ul li").removeClass("active");
	$(this).addClass("active");
});

$('.addNewMessage').click(function() {
	$("#addNewMessagePopup").modal('show');
});

function resetMessageform()
{
	$("#addNewMessageform")[0].reset();
	$("#addNewMessagePopup").modal('hide');
}

function CreateMessage()
{
	var userID 		= $( "#userID option:selected" ).val();
	var userName 	= $( "#userID option:selected" ).text();
	$(".ChatDropdown-List ul li").removeClass("active");
	
	$(".ChatDropdown-List ul li.emptyContactList").remove();
	$(".UserMessage-Section > div.emptyMessage").remove();
	
	if ($('#li'+userID).length)
	{
		$(".ChatDropdown-List ul li").removeClass("active");
		$('#li'+userID).addClass("active");
	}
	else
	{
		var liHtml = '<li class="active" data-id="'+userID+'" id="li'+userID+'">'+
						'<a href="javascript:void(0)" onclick="loadMessage('+userID+')">'+
							'<div class="ChatDropdown-Head">'+
								'<img src="asset/image/default-image.png" width="36" height="36" alt=""/>'+
								'<h1>'+userName+'<br><span>No messages</span></h1>'+
							'</div>'+
						'</a>'+
					'</li>';
					
		$(".ChatDropdown-List > ul").prepend(liHtml);
	}
	
	
	resetMessageform();
	loadMessage(userID);
}

function loadMessage(userID)
{
	if(userID == "")
		return false;
		
	$("#revicerID").val(userID);
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var formData = $("#message_form").serialize();
	var type = "POST";
	var ajaxurl= "/loadChats";
		
	$.ajax({
		type: type,
		url: ajaxurl,
		data: { "userID" : userID },
		dataType: 'json',
		success: function (data) {
			$(".UserMessage-Section").html('');				
			$(".UserMessage-Section").html(data.html);	
			$(".UserMessage-Section").animate({ scrollTop: scrollPostion }, "fast");

			var last_message = $('.UserMessage-Section > div:last > div > h1').text().substring(0, 25);;
			$('.ChatDropdown-List > ul > li.active > a > div.ChatDropdown-Head > h1 > span').text(last_message);
		},
		error: function (data) {
			console.log(data);
		}
	});
	
}

function loadContact()
{
	return false;
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var type = "POST";
	var ajaxurl= "/loadChatContact";
		
	$.ajax({
		type: type,
		url: ajaxurl,
		dataType: 'json',
		success: function (data) {
			$(".ChatDropdown-List > ul ").html('');				
			$(".ChatDropdown-List > ul").html(data.html);	
			$('.ChatDropdown-List > ul > li.active > a').trigger('click');
		},
		error: function (data) {
			console.log(data);
		}
	});
	
}

function loadNewMessages()
{
	var userID = $("#revicerID").val();	
	if(userID == "")
		return false;
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var formData = $("#message_form").serialize();
	var type = "POST";
	var ajaxurl= "/loadChats";
		
	$.ajax({
		type: type,
		url: ajaxurl,
		data: { "userID" : userID },
		dataType: 'json',
		success: function (data) {
			$(".UserMessage-Section").html('');				
			$(".UserMessage-Section").html(data.html);	
			$(".UserMessage-Section").animate({ scrollTop: scrollPostion }, "fast");
		},
		error: function (data) {
			console.log(data);
		}
	});
	
}

</script>

@endsection
