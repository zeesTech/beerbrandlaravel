@extends('layouts.app')

@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    #table_id thead {
        background-color: #FF6000;
        color: white;
    }

    #table_id thead tr {
        height: 60px;
    }
    #table_id .even { background-color: white }
    #table_id .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    #table_id tbody tr {
        height: 60px;
    }
	
	.come-from-modal.left .modal-dialog,
	.come-from-modal.right .modal-dialog {
		position: fixed;
		margin: auto;
		width: 420px;
		height: 100%;
		-webkit-transform: translate3d(0%, 0, 0);
		-ms-transform: translate3d(0%, 0, 0);
		-o-transform: translate3d(0%, 0, 0);
		transform: translate3d(0%, 0, 0);
	}

	.come-from-modal.left .modal-content,
	.come-from-modal.right .modal-content {
		height: 100%;
		overflow-y: auto;
		border-radius: 0px;
	}

	.come-from-modal.left .modal-body,
	.come-from-modal.right .modal-body {
		padding: 15px 15px 80px;
	}
	.come-from-modal.right.fade .modal-dialog {
		right: -320px;
		-webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
		-moz-transition: opacity 0.3s linear, right 0.3s ease-out;
		-o-transition: opacity 0.3s linear, right 0.3s ease-out;
		transition: opacity 0.3s linear, right 0.3s ease-out;
	}

	.come-from-modal.right.fade.in .modal-dialog {
		right: 0;
	}
	.color{
		background-color: #FF6000;
		color:#FFF;
	}
	.bold{
		font-weight : bold;
	}
	.tink{
		font-weight : 300;
	}
	
	.outerImg
    {
      position: relative;
      top: 0;
      left: 0;
    }
    .innerImg
    {
      position: absolute;
      top: 70px;
	  left: 140px;
    }
	
</style>



<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Update Beers Status</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
	<div class="row">
		<div class="col-md-7">
			<h6 class=" m-0 font-weight-bold text-primary">Update Beers Status</h6>
		</div>
		<div class="col-md-5">
			<a href="javascript:void(0)" onclick="updateStatus()" class="customAdd-Btn text-uppercase float-right">Update Status</a>
		</div>
	</div>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="table_id" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Sr #</th>
            <th>Beer Name</th>
            <th>Beer Style</th>
            <th>Keg Size</th>
            <th>Unit Cost</th>
            <th>Beer Status</th>
          </tr>
        </thead>
       
        <tbody>
		<?php
			$i = 0;
			foreach($data as $dt):
			$i++;
			$imgPath 	= (isset($dt->img_unique_name) && !empty($dt->img_unique_name)) ? asset('uploads/'.$dt->img_unique_name) : false;
			$notes 		= (isset($dt->notes) && !empty($dt->notes)) ? $dt->notes : false;
			$status 	= (isset($dt->status) && !empty($dt->status)) ? $dt->status : false;
			$beerStatusArray = array( "1" => "Brew this Beer" , "2" => "Brewery Matching" , "3" => "Brewing in Progress" , "4" => "Ready for Order" );
			$BeerStatus = (!empty($beerStatusArray) && isset($beerStatusArray[$dt->beer_status]) && !empty($beerStatusArray[$dt->beer_status])) ? $beerStatusArray[$dt->beer_status] : false;
			$style = "";
			//if(!empty($status))
				//$style = "background-color:#e3e6f0";
		?>
                    <tr style="{{$style}}">
						<input type="hidden" id="imgPath_<?php echo $i;?>" value="{{$imgPath}}"/>
						<input type="hidden" id="notes_<?php echo $i;?>" value="{{$notes}}"/>
                        <td>{{$i}}</td>
                        <td id="beer_name_<?php echo $i;?>">{{$dt->beer_name}}</td>
                        <td id="beer_style_name_<?php echo $i;?>">{{$dt->beer_style_name}}</td>
                        <td id="keg_size_<?php echo $i;?>">{{$dt->keg_sizes_name}}</td>
                        <td id="unit_cost_<?php echo $i;?>">{{$dt->unit_cost}}</td>
                        <td id="unit_status_<?php echo $i;?>">
							<?php
								if(!empty($dt->beer_status) && $dt->beer_status > 1)
								{
							?>
									<input type="hidden" name="beerIds[]" value="<?php echo $dt->beer_id;?>"/>
									<select class="form-control" placeholder="Select Status" name="status_id[]" id="status_id_<?php echo $i;?>" >
								<?php
									if(!empty($beerStatusArray))
									{
										for($k=2;$k<=count($beerStatusArray);$k++)
										{	
											$selected = "";
											if($dt->beer_status == $k)
												$selected = "selected";
											echo  '<option value="'.$k.'" '.$selected.'>'.$beerStatusArray[$k].'</option>';
										}
									}
							?>
									</select>
							<?php
								}
								else
								{
									echo $BeerStatus;
								}
								?>
							
						</td>
                    </tr>
            <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>

<script>
    $(document).ready( function () {
        $('#table_id').DataTable({
            searching: false, info: false,
            aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ -1 ]
                            }
            ]
        });
    } );
	
	function loadBeer(e)
	{
		var id = $(e).attr('data-id');
		if(id == '')
			return false;
			
		var beer_name = $("#beer_name_"+id).text();
		var beer_style_name = $("#beer_style_name_"+id).text();
		var keg_size = $("#keg_size_"+id).text();
		var abv_name = $("#abv_name_"+id).text();
		var ibu_name = $("#ibu_name_"+id).text();
		var unit_cost = $("#unit_cost_"+id).text();
		var imgPath = $("#imgPath_"+id).val();
		var notes = $("#notes_"+id).val();
		
		$("#name").text(beer_name);
		$("#beer_style").text(beer_style_name);
		$("#ibu").text(ibu_name);
		$("#abv").text(abv_name);
		$("#keg_size").text(keg_size);
		$("#notes").text(notes);
		
		$("#ImgPath").attr("src","");
		if(imgPath != '')
			$("#ImgPath").attr("src",imgPath);
	}

	function deleteRow(e)
	{
		var id = $(e).attr('data-beerID');
		if(id == '')
			return false;
		
		if (confirm('Do you really want to delete this beer?')) {
			
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});
			
			var type = "POST";
			var ajaxurl = '/BeerDelete';
			$.ajax({
				type: type,
				url: ajaxurl,
				data : {"id" : id },
				dataType: 'json',
				success: function (data) {
					location.reload();
				},
				error: function (data) {
					console.log(data);
				}
			});	
		}
	}
	
	function BrewBeer(e)
	{
		var id = $(e).attr('data-beerID');
		if(id == '')
			return false;
		
		if (confirm('If you want us to brew this beer, the next step will be to match your recipe with a brewing partner.  This is exciting!  Similar to how you order beer currently, you will place your order through the site and we will deliver your beer.  Please allow 4 weeks for the initial order to be created, after that we will continue to make this beer for you! Do you want us to begin brewing this beer? ')) {
			window.location.href = "brew_beer/"+id;
		}
	}
	
	function StopBrewBeer(e)
	{
		var id = $(e).attr('data-beerID');
		if(id == '')
			return false;

		if (confirm('Do you really want to stop brewing this beer?')) {		
			window.location.href = "stop_brew_beer/"+id;
		}
	}

	function updateStatus()
	{
        var BeerIds = $("input[name='beerIds[]']").map(function(){return $(this).val();}).get();
        var StatusIds = $("select[name='status_id[]']").map(function(){return $(this).val();}).get();
		
		if(BeerIds == '')
			return false;
		
		if (confirm('Do you really want to change the status?')) {
			
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});
			
			var type = "POST";
			var ajaxurl = '/BeerStatusUpdate';
			$.ajax({
				type: type,
				url: ajaxurl,
				data : {"BeerIds" : BeerIds , "StatusIds" : StatusIds},
				dataType: 'json',
				success: function (data) {
					location.reload();
				},
				error: function (data) {
					console.log(data);
				}
			});	
		}		
	}
	
</script>
@endsection
