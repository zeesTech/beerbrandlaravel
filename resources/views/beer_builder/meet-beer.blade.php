@extends('layouts.app')

@section('content')
<style>
	.login,
		.image {
		width: 100%;  
		min-height:   100vh;
		}
		
	#exampleModal{
		margin: 0px auto;
		text-align: center;
	}	
	.outerImg
    {
      position: relative;
      top: 0;
      left: 0;
    }
    .innerImg
    {
      position: absolute;
      top: 110px;
      left: 212px;
    }
</style>
<link rel="stylesheet" href="{{URL::asset('assets/css/style.css')}}">
<div class="container-fluid">
	<div class="MeetBeer-Container card shadow mb-4">
	<div class="row">
		
	<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
		<div class="MeetBeer-Large" style="position: relative; left: 0; top: 0;">
		<img class="outerImg" src="{{URL::asset('assets/Image 1.png')}}" width="468" height="468" alt=""/>
		<?php	
			if(isset($data['brands']) && !empty($data['brands']) && isset($data['brands']['img_unique_name']) && !empty($data['brands']['img_unique_name']))
			{
				$src = asset('uploads/'.$data['brands']['img_unique_name']);
				
		?>
			<img class="innerImg" src="{{ $src }}" width="150px" height="150px"/>
		<?php
			}
		?>
		
		</div>
					
		<!--<div class="MeetBeer-List">
			<h3>Name:</h3>
			<h4>{{$beer_data['beer_name']}}</h4>
				
			<ul>
				
			<li>
			<h1>Beer Style:</h1>
			<h2>{{$data['beer_style']['name']}}</h2>
			</li>
				
			<li>
			<h1>KEG Size:</h1>
			<h2>{{$data['keg_size']['name']}}</h2>
			</li>
				
			<li>
			<h1>Portion Cost:</h1>
			<h2>${{ (!empty($beer_data['total_cost'])) ? $beer_data['total_cost'] : 0 }}</h2>
			</li>
				
			<li>
			<h1>KEG Cost:</h1>
			<h2>${{ (!empty($beer_data['unit_cost'])) ? $beer_data['unit_cost'] : 0 }}</h2>
			</li>
			
			</ul>
			
			<h3>Beer Notes:</h3>
			<h4>{{$beer_data['notes']}}</h4>	 
		
			<br>
			
			<h3>Beer Motivation :</h3>
			<h4>{{$beer_data['like_beer']}}</h4>	 
			
			
		</div>-->
		
	</div>
		
	<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
		
		<form id="save_beer" action="{{ route('beer.store') }}" method="post">
		@csrf
		<div class="row">
		
		<div class="col-md-12">
		<div class="MeetUp-Head">
			<h1>Meet Your Beer</h1>
		</div>
		</div>
			
		<div class="col-md-12">
		<div class="MeetUp-Form">
			<label>Beer Name</label>
			<input type="text" id="" name="" readonly value="{{$beer_data['beer_name']}}" placeholder="Beer Name">
		</div>
		</div>
			
		<div class="col-md-6">
		<div class="MeetUp-Form">
			<label>Beer Style</label>
			<input type="text" id="" name="" readonly placeholder="Beer Style" value="{{$data['beer_style']['name']}}">
		</div>
		</div>
		
		<div class="col-md-6">
		<div class="MeetUp-Form">
			<label>Keg Size</label>
			<input type="text" id="" name="Keg Size" readonly placeholder="" value="{{$data['keg_size']['name']}}">
		</div>  
		</div>
		
		<div class="col-md-6">
		<div class="MeetUp-Form">
			<label>Cost</label>
			<input type="text" id="" readonly name="cost" placeholder="Cost" value="${{ (!empty($beer_data['total_cost'])) ? $beer_data['total_cost'] : 0 }}">
		</div>  
		</div>
		
		<div class="col-md-6">
		<div class="MeetUp-Form">
			<label>Portion Cost Per Ounce</label>
			<input type="text" id="" readonly name="unit_cost" placeholder="Portion Cost Per Ounce" value="${{ (!empty($beer_data['unit_cost'])) ? $beer_data['unit_cost'] : 0 }}">
		</div>  
		</div>
		
		<div class="col-md-6">
			<div class="MeetUp-Form">
				<div class="MeetUp-Btn">
					<a href="{{ route('beer.index')}}">Beer Portfolio</a>
				</div>
			</div>  
		</div>
				
		</div>
		</form>
	
	</div>
	</div>  
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-body" style="padding: 50px;">
				<img src="{{URL::asset('assets/XMLID_1840_.png')}}" alt="" style="width: 35%;">
				<p style="color:#FF6000;font-size: 35px;font-weight:bold;">Saved To Your Brands</p>
				<div class="MeetUp-Btn">
					<a href="{{ route('brand.index')}}">VIEW ALL BRANDS</a>
				</div>
		</div>
    </div>
  </div>
</div>
<script>
	$("#save_beer").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
			dataType: "JSON",
            success: function(response) {
				if (response.data != '') {
					console.log('heloo');
					$('#exampleModal').modal('show');
				}
                console.log(response.data); // show response from the php script.
            }
        });
    });
	
</script>
@endsection