<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offerlog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
	use SoftDeletes;
    protected $fillable = [
        'batch_id','partner_id'
    ];
}
