<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Beers;
use App\Brands;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

	public function Dashboard()
    {
		$user_id = Auth::user()->id;
		$data['TotalBrands'] = Brands::where(array('user_id' => $user_id))->count();
		$data['TotalBeers'] = Beers::where(array('user_id' => $user_id))->count();
		$data['TotalInactiveBeers'] = Beers::where(array('user_id' => $user_id,'status' => 0 ))->count();
		$data['TotalActiveBeers'] = $data['TotalBeers'] - $data['TotalInactiveBeers'];
        return view('beer_builder/dashboard',$data);
    }
	
	public function updateProfile(Request $request)
    {
		$id = auth()->user()->id;
		if(!empty($id))
		{
			$input = $request->all();

			if($request->hasFile('image'))
			{
				$fileobj				= $request->file('image');
				$image_original_name 	= $fileobj->getClientOriginalName('image');
				$file_extension_name 	= $fileobj->getClientOriginalExtension('image');
				$image_unique_name 		= time().rand(1000,9999).'.'.$file_extension_name;
				$destinationPath		= public_path('/uploads/');
				$fileobj->move($destinationPath,$image_unique_name);
				
				$input['img_actual_name'] 	= $image_original_name;
				$input['img_unique_name'] 	= $image_unique_name;
			}

			unset($input['_token']);
			unset($input['image']);
			unset($input['_method']);
		
			$input['name'] = $input['fname'] . " " .$input['lname'];
			User::where('id', $id)->update($input);
		}
		
		return redirect('/profile');
    }
}
