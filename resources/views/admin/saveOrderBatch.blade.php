@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Batches Being Built</h1>
	</div>

	<!-- Content Row -->

	<div class="row">

		<div class="col-xl-8 col-lg-7">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h6 class="m-0 font-weight-bold text-primary">Batches Being Built</h6>
				</div>
				<!-- Card Body -->
				<div class="card-body">
					<form action="{{ route('SaveOrderBatch') }}" method="post">
						@csrf
						<input type="hidden" name="id" value="{{ (!empty($orderData) && isset($orderData->id)) ? $orderData->id : 0 }}"/>
						<div class="form-group">
							<input type="text" id="name" class="form-control" placeholder="Batch Name" name="name" value="{{ (!empty($orderData) && isset($orderData->name)) ? $orderData->name : '' }}" required autofocus autocomplete="off">
						</div>
						<div class="form-group">
							<select class="form-control" placeholder="Beer Style" name="beer_style_id" required autofocus >
								<option>Beer Style</option>
								@foreach($beer_styles as $dt)
									<option value="{{$dt['id']}}" {{ (!empty($orderData) && isset($orderData->beer_style_id) && $orderData->beer_style_id == $dt->id ) ? 'selected' : '' }} >{{$dt['name']}}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<select class="form-control" placeholder="Brewer's Inspiration" name="brewers_inspiration_id" required autofocus>
								<option>Brewer's Inspiration</option>
								@foreach($brewers_inspiration as $dt)
									<option value="{{$dt['id']}}" {{ (!empty($orderData) && isset($orderData->brewers_inspiration_id) && $orderData->brewers_inspiration_id == $dt->id ) ? 'selected' : '' }}  >{{$dt['name']}}</option>
								@endforeach
							</select>
						</div>
						
						<div class="form-group">
							<select class="form-control" placeholder="Recipe" name="reciepe_id" required autofocus>
								<option>Recipe</option>
								@foreach($reciepes as $dt)
									<option value="{{$dt['id']}}" {{ (!empty($orderData) && isset($orderData->reciepe_id) && $orderData->reciepe_id == $dt->id ) ? 'selected' : '' }} >{{$dt['beer_name']}}</option>
								@endforeach
							</select>
						</div>
						
						<div class="form-group">
							<textarea type="text" class="form-control" rows="5" cols="1" placeholder="Notes" name="notes" autofocus="" >{{ (!empty($orderData) && isset($orderData->notes)) ? $orderData->notes : '' }}</textarea>
						</div>
						
						<div class="btn-inline">
							<button class="btn btn-lg btn-bbb-notActive btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection
