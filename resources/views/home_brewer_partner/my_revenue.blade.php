@extends('layouts.app')

@section('content')

 <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Optional JavaScript -->

<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    #table_id thead {
        background-color: #FF6000;
        color: white;
    }

    #table_id thead tr {
        height: 60px;
    }
    #table_id .even { background-color: white }
    #table_id .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    #table_id tbody tr {
        height: 60px;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
	<div class="container">
                
		<div class="row" style="margin-top: 30px;">
		
			<div class="col-md-4"></div>
			<div class="col-md-4">
			 <div class="logo">
				<p style="color: #FF6000 !important;
				text-align: center;
				font-size: 33px;
				font-style: bold;
				font-weight: bold;
				font-style: normal;
				margin: 0px 0px 20px 0;">Total Revenue</p>
			</div>
			</div>
			<div class="col-md-4"></div>
			
			<div class="col-md-12 pl-0 pr-0">
			 <div class="my-revenue-container">
			  <div class="row">
				 
			    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
				<div class="Revenue-Head">
				 <h1 id="grand_total_revenue">$0</h1>
				 <p>Total Earnings</p>
				</div>
			   </div>
			   
			   <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
				<div class="Revenue-Head">
				 <h1 id="total_order">0</h1>
				 <p>Total Orders</p>
				</div>
			   </div>
				  
			   <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
				<div class="Revenue-Head">
				 <h1 id="pending_revenue">$0</h1>
				 <p>Pending Amount</p>
				</div>
			   </div>
				  
			   <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
				<div class="Revenue-Head">
				 <h1 id="pending_order">0</h1>
				 <p>Pending Orders</p>
				</div>
			   </div>
				  
			  </div>
			 </div>
			</div>
			
		</div>

		<div class="row tabledesign">
			<table class="table table-bordered" id="table_id" class="display">
				<thead>
					<tr>
						<th>Batch ID</th>
						<th>Keg Quantity (1/2 BBL)</th>
						<th>½ BBL Revenue</th>
						<th>Keg Quantity (1/6 BBL)</th>
						<th>1/6 BBL Revenue</th>
						<th>Total Beer Revenue</th>
						<th>Beer Style & Recipe</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						$i = 0; 
						$grand_total_revenue = 0; 
						$total_order = 0; 
						$pending_revenue = 0; 
						$pending_order = 0; 
						$StatusArray = array( 1 => "Ready to brew", 2 => "Brewing", 3 => "Ready for Pickup", 4 => "Completed");
						if(isset($data['revenue']) && !empty($data['revenue'])):
							foreach($data['revenue'] as $dt):
								$i++;
								$batch_id 				= (isset($dt->batchID) && !empty($dt->batchID)) ? $dt->batchID : "-";
								$keg_1_2_size_counter 	= (isset($dt->keg_1_2_size_counter) && !empty($dt->keg_1_2_size_counter)) ? $dt->keg_1_2_size_counter : 0;
								$keg_1_2_size_revenue 	= (isset($dt->keg_1_2_size_revenue) && !empty($dt->keg_1_2_size_revenue)) ? $dt->keg_1_2_size_revenue : 0;
								$keg_1_6_size_counter 	= (isset($dt->keg_1_6_size_counter) && !empty($dt->keg_1_6_size_counter)) ? $dt->keg_1_6_size_counter : 0;
								$keg_1_6_size_revenue 	= (isset($dt->keg_1_6_size_revenue) && !empty($dt->keg_1_6_size_revenue)) ? $dt->keg_1_6_size_revenue : 0;
								$beer_style_name 		= (isset($dt->beer_style_name) && !empty($dt->beer_style_name)) ? $dt->beer_style_name : "-";
								$reciepe_name 			= (isset($dt->reciepe_name) && !empty($dt->reciepe_name)) ? $dt->reciepe_name : "-";
								
								$total_revenue = $keg_1_2_size_revenue + $keg_1_6_size_revenue;
								
								$grand_total_revenue += $total_revenue;
								$total_order++;
								
								if($dt->order_status != 4)
								{
									$pending_revenue += $total_revenue;
									$pending_order++;
								}

								$status = (!empty($StatusArray) && isset($StatusArray[$dt->order_status]) && !empty($StatusArray[$dt->order_status])) ? $StatusArray[$dt->order_status] : "-";
					?>
							<tr>
								<td >{{$batch_id}}</td>
								<td >{{$keg_1_2_size_counter}}</td>
								<td >${{$keg_1_2_size_revenue}}</td>
								<td >{{$keg_1_6_size_counter}}</td>
								<td >${{$keg_1_6_size_revenue}}</td>
								<td >${{$total_revenue}}</td>
								<td >{{$beer_style_name}} & {{$reciepe_name}}</td>
								<td >{{$status}}</td>
							</tr>
				<?php
						endforeach;
					endif;
				?>
				</tbody>
			</table>
		</div>
    </div>
</div>

<script>
    $(document).ready( function () {
        $('#table_id').DataTable({
            searching: false, info: false,
            aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ -1 ]
                            }
            ]
        });
		
		$("#grand_total_revenue").text("<?php echo number_format($grand_total_revenue); ?>$");
		$("#total_order").text("<?php echo number_format($total_order); ?>");
		$("#pending_revenue").text("<?php echo number_format($pending_revenue); ?>$");
		$("#pending_order").text("<?php echo number_format($pending_order); ?>");
    } );
</script>
@endsection
