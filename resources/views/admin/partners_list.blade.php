@extends('layouts.app')

@section('content')
<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    #table_id thead {
        background-color: #FF6000;
        color: white;
    }

    #table_id thead tr {
        height: 60px;
    }
    #table_id .even { background-color: white }
    #table_id .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    #table_id tbody tr {
        height: 60px;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Partners List</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Partners List</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="table_id" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Sr #</th>
            <th>Name</th>
            <th>Type</th>
			<th>Eamil</th>
			<th>Phone</th>
			<th>Address</th>
			<th>Biography</th>
			<th>Notes</th>
			<th>Action</th>
          </tr>
        </thead>
       
        <tbody>
		@if(!empty($data))
		@php $i =0; @endphp
			@foreach($data as $dt)
				@php $i++; @endphp
				<tr>
					<td>{{$i}}</td>
					<td>{{$dt->name}}</td>
					<td>{{$dt->role_name}}</td>
					<td>{{$dt->email}}</td>
					<td>{{$dt->phone}}</td>
					<td>{{$dt->address}}</td>
					<td>{{$dt->biography}}</td>
					<td>{{$dt->notes}}</td>
					<td>
						<div class="dropdown no-arrow">
							<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink{{$i}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink{{$i}}">
								<a class="dropdown-item" href="javascript:void(0)" onclick="editNotes(this)" data-id="{{$dt->id}}" data-notes="{{$dt->notes}}">Edit Notes</a>
							</div>
						</div>
					</td>
				</tr>
            @endforeach
		@endif
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>

	<div class="modal fade in" id="EditPopup" data-backdrop="static">
	  <div class="modal-dialog modal-md">
		<div class="modal-content">
			<form action="javascript:void(0)" id="Editform" method="POST" enctype="multipart/form-data">
			  <input type="hidden" name="user_id" id="user_id" />
			  <div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Edit</h4>
				<button type="button" class="close" onclick="resetEditform()" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <div class="modal-body">
				<div class="row">
					<div class="col-md-12" >
						<div class="form-group" >
							<textarea class="form-control" name="notes" id="notes" rows="5" placeholder="Notes"></textarea>
						</div>
                    </div>
				</div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="customAdd-Btn" id="saveBtn" onclick="SaveNotes()">Save</button>
				<button type="reset" class="customClose-Btn" onclick="resetEditform()">Close</button>
			  </div>
			</div>
		</form>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>



<script>
    $(document).ready( function () {
        $('#table_id').DataTable({
            searching: false, info: false,
            aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ -1 ]
                            }
            ]
        });
    } );
	
function editNotes(e)
{
	var id 		= $(e).attr('data-id');
	var notes 	= $(e).attr('data-notes');
	if(id == '')
		return false;
		
	$("#user_id").val(id);
	$("#notes").text(notes);
	$("#EditPopup").modal('show');
}

function SaveNotes()
{	
	var user_id = $("#user_id").val();
	if(user_id == '')
	{
		alert("Something went wrong.!");
		return false;
	}
			
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var formData = $("#Editform").serialize();
	var type = "POST";
	var ajaxurl= "/saveNotes";
	
	$.ajax({
		type: type,
		url: ajaxurl,
		data: { "user_id" : user_id , "notes" : $("#notes").val() },
		dataType: 'json',
		success: function (data) {
			alert("Data Successfully Updated.!");
			$("#EditPopup").modal('hide');
			location.reload();
		},
		error: function (data) {
			console.log(data);
		}
	});
	
}

function resetEditform()
{
	$("#Editform")[0].reset();
	$("#user_id").val('');
	$("#EditPopup").modal('hide');
}

</script>
@endsection
