@extends('layouts.app')

@section('content')
<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    #table_id thead {
        background-color: #FF6000;
        color: white;
    }

    #table_id thead tr {
        height: 60px;
    }
    #table_id .even { background-color: white }
    #table_id .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    #table_id tbody tr {
        height: 60px;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Home Brewer Submission</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Home Brewer Submission</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive" style="display:none;">
      <table class="table table-bordered" id="table_id" width="100%" cellspacing="0">
        <thead>
          <tr>
            <!-- <th>Quantity</th> -->
            <th>Tank Available</th>
            <th>Tank Size (BBLs)</th>
            <th>Brew Date</th>
            <th>Pickup Date</th>
            <th>Batch Fill</th>
            <th>1/2 BBL Qty</th>
            <th>1/6 BBL Qty</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
       
        <tbody>
		@if(!empty($data))
        @foreach($data as $dt)
                    <tr>
                        <!-- <td>02</td> -->
                        <td>{{$dt->beer_name}}</td>
                        <td>{{$dt->beer_style_name}}</td>
                        <td>5 GALLON</td>
                        <td>{{$dt->package_type}}</td>
                        <td>{{$dt->abv_name}}</td>
                        <td>{{$dt->ibu_name}}</td>
                        <td>{{$dt->unit_cost}}</td>
						<td>$399.00</td>
                        <td><a href="{{ route('beer.meet_beer',['id' => $dt->beer_id]) }}" style="color:#FF6000;">View</a></td>
                    </tr>
                    @endforeach
		@endif
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>

<script>
    $(document).ready( function () {
        $('#table_id').DataTable({
            searching: false, info: false,
            aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ -1 ]
                            }
            ]
        });
    } );
</script>
@endsection
