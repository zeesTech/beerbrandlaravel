<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_notification', function (Blueprint $table) {
            $table->id();
			$table->unsignedInteger('user_id');
            $table->Integer('notification_type_id');
            $table->Integer('sender_id');
            $table->Integer('receiver_id');
            $table->string('action',50)->nullable();
			$table->Integer('action_id');
			$table->tinyInteger('isRead')->default(0);
            $table->text('msg')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_notification');
    }
}
