@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <!-- Page Heading -->
 <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Edit Beer</h1>
  </div>

  <!-- Content Row -->

  <div class="row">

    <!-- Area Chart -->
    <div class="col-xl-8 col-lg-7">
      <div class="card shadow mb-4">
        <!-- Card Body -->
        <div class="card-body">
        <form action="{{ route('updateBeer') }}" method="post">
                @csrf
					<input type="hidden" name="id" value="{{ $beer->id }}"/>
					<div class="row">
						<div class="form-group col-md-6">
						  <select class="form-control" placeholder="Brand" name="brand_id" required autofocus>
						  <option>Select Brand</option>
						  @foreach($brands as $dt)
							<option value="{{$dt['id']}}" {{ $dt['id'] == $beer->brand_id ? 'selected' : '' }} >{{$dt['brand_name']}}</option>
						  @endforeach
						  
						</select>
						</div>
						<div class="form-group col-md-6">
						  <input type="text" id="beername" class="form-control" value="{{ $beer->beer_name }}" placeholder="Name Your Beer" name="beer_name" required autofocus>
						</div>
                    </div>
					
					<div class="row">
						<div class="form-group col-md-6">
						  <select class="form-control" placeholder="Beer Style" name="beer_style_id" required autofocus>
						  <option>Beer Style</option>
						  @foreach($beer_styles as $dt)
							<option value="{{$dt['id']}}" {{ $dt['id'] == $beer->beer_style_id ? 'selected' : '' }} >{{$dt['name']}}</option>
						  @endforeach
						  
						</select>
						</div>

						<div class="form-group col-md-6">
						  <select class="form-control" placeholder="Brewer's Inspiration" name="brewers_inspiration_id" required autofocus>
						  <option>Brewer's Inspiration</option>
						  @foreach($brewers_inspiration as $dt)
							<option value="{{$dt['id']}}" {{ $dt['id'] == $beer->brewers_inspiration_id ? 'selected' : '' }}  >{{$dt['name']}}</option>
						  @endforeach
						</select>
						</div>
                    </div>
                  
                    <div class="form-group hide" style="display:none;">
                      <select class="form-control" placeholder="ABV %"  name="abv_id" autofocus style="margin-right: 10px;">
                      <option value="0">ABV %</option>
                      @foreach($abv as $dt)
                        <option value="{{$dt['id']}}" {{ $dt['id'] == $beer->abv_id ? 'selected' : '' }} >{{$dt['name']}}</option>
                      @endforeach
                    </select>

					  <select class="form-control" placeholder="IBU’S"  name="ibu_id" autofocus style="margin-right: 10px;">
                      <option value="0">IBU'S</option>
                      @foreach($ibu as $dt)
                        <option value="{{$dt['id']}}" {{ $dt['id'] == $beer->ibu_id ? 'selected' : '' }} >{{$dt['name']}}</option>
                      @endforeach
                    </select>
					
                    </div>

                    <div class="form-group hide" style="display:none;width: 100%;">
                      
                      <select class="form-control hide" placeholder="HOP Profile" name="hop_id" autofocus>
                        <option value="0">HOP Profile</option>
                        @foreach($hop_profile as $dt)
                        <option value="{{$dt['id']}}" {{ $dt['id'] == $beer->hop_id ? 'selected' : '' }} >{{$dt['name']}}</option>
                      @endforeach
                      </select>
					  
					  <select class="form-control hide" placeholder="Malt Profile" name="malt_style_id" autofocus>
                        <option value="0">Malt Profile</option>
                        @foreach($malt_style as $dt)
                        <option value="{{$dt['id']}}" {{ $dt['id'] == $beer->malt_style_id ? 'selected' : '' }} >{{$dt['name']}}</option>
                      @endforeach
                      </select>
					  
                    </div>

					<div class="form-group" style="display: inline-flex;width:100%">
                      <label class="label-control" for="" style="width: 80%">Approximately how many kegs of this beer do think you will sell in a week</label>
                      <input type="text" class="form-control" required="" value="{{ $beer->kegs_sell_week }}" name="kegs_sell_week" autofocus="" style=" width: 20%;height: 50px;">
                    </div>
					
					<div class="form-group hide" style="display: none;width:100%">
                      <input type="text" class="form-control number_only" name="qty" autofocus="" placeholder="Quantity" value="{{ $beer->qty }}" >
                    </div>
					
                    <div class="form-group" style="display: inline-flex;width: 100%;">
                      <select class="form-control" placeholder="KEG Size"  name="keg_size_id" required autofocus style="margin-right: 10px;">
                      <option>KEG Size</option>
                      @foreach($keg_sizes as $dt)
                        <option value="{{$dt['id']}}" {{ $dt['id'] == $beer->keg_size_id ? 'selected' : '' }} >{{$dt['name']}}</option>
                      @endforeach
                    </select>
                    </div>

                    

                    <div class="form-group hide" style="display: none;width:100%;">
                      <label class="label-control" for="" style="width: 80%">Approximately, how many KEGS do you in a week <br> (currently)?</label>
                      <input type="text" class="form-control" value="{{ $beer->kegs_sell_week_in_covid }}" name="kegs_sell_week_in_covid" autofocus="" style=" width: 20%;height: 50px;">
                    </div>

                    <div class="form-group hide" style="display: none;width: 100%;">
                      <label class="label-control" for="" style="width: 70%">Are you interested in canning this beer, too?</label>
                      <div class="some-class" style="width: 30%">
                        <input type="radio" class="radio" name="interested_canning_beer" value="1" id="y" />
                        <label for="y">Yes</label>
                        <input type="radio" class="radio" name="interested_canning_beer" value="0" id="z" />
                        <label for="z">No</label>
                      </div>
                    </div>
					
                    <div class="form-group" style="display: inline-flex;width: 100%;">
						<textarea type="text" class="form-control" style="width:100%" rows="1" cols="1" required="" placeholder="Is there a beer that you would like to be the motivation for this beer?" name="like_beer" autofocus="" >{{ $beer->like_beer }}</textarea>
                    </div>
					
					<div class="form-group" style="display: inline-flex;width: 100%;">
						<textarea type="text" class="form-control" style="width:100%" rows="5" cols="1" placeholder="Anything else you would like to tell us about this beer?" name="notes" autofocus="" >{{ $beer->notes }}</textarea>
					</div>
					
                    <div class="btn-inline">
                        <button class="btn btn-lg btn-bbb-notActive btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Update</button>
                    </div>
                    </form>
            </div>
      </div>
    </div>
  </div>

  </div>
@endsection
