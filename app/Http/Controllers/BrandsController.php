<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brands;
use App\Beers;
use App\User;
use Auth;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
    public function index(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			$user_id = Auth::user()->id;
			$data = Brands::where( 'brands.user_id' , $user_id )->get();
			//dd($data);
			return view('beer_builder/my-brand',['data' => $data]);
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			return view('beer_builder/build_brand_beer');
		}		
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			//dd(Auth::user()->id);die;
			$data = $request->all();
			
			$image_original_name  = '';
			$image_unique_name = '';
			if($request->hasFile('image'))
			{
				$fileobj				= $request->file('image');
				$image_original_name 	= $fileobj->getClientOriginalName('image');
				$file_extension_name 	= $fileobj->getClientOriginalExtension('image');
				$image_unique_name 		= time().rand(1000,9999).'.'.$file_extension_name;
				$destinationPath		= public_path('/uploads/');
				$fileobj->move($destinationPath,$image_unique_name);
			}

			$data['img_actual_name'] 	= $image_original_name;
			$data['img_unique_name'] 	= $image_unique_name;
			
			$data['user_id'] = Auth::user()->id;
			Brands::create($data);
			return redirect()->route('brand.index');
		}
    }

	public function deleteBrand($id)
    {
		if(!empty($id))
		{
			$user_id = Auth::user()->id;
			Brands::where(array('id' => $id, 'user_id' => $user_id))->delete();
			Beers::where(array('brand_id' => $id, 'user_id' => $user_id))->delete();
		}

		return redirect()->route('brand.index');
    }
	
	public function Orders(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			$data = array();
			$data = Beers::where('status','!=',0)
							->join('beer_style','beer_style.id','=','beers.beer_style_id')
							->join('keg_sizes','keg_sizes.id','=','beers.keg_size_id')
							->latest('beers.created_at')
							->get(['beers.qty','beers.order_date','beers.beer_name','beers.unit_cost','beers.total_cost','beer_style.name as beer_style_name','keg_sizes.name as keg_sizes_name']);
			return view('beer_builder/order',['data' => $data]);
		}
    }
	
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	public function BrandDelete(Request $request)
    {
		$para = $request->all();
		$id = (isset($para['id']) && !empty($para['id'])) ? $para['id'] : false;
		if(!empty($id))
		{
			$user_id = Auth::user()->id;
			Brands::where(array('id' => $id, 'user_id' => $user_id))->delete();
			//Beers::where(array('brand_id' => $id, 'user_id' => $user_id))->delete();
			return response()->json(array('status'=> true , 'msg' => "Brand Successfully Deleted."), 200);
		}
		else
		{
			return response()->json(array('status'=> false , 'msg' => "somethig went wrong."), 200);
		}
    }
}
