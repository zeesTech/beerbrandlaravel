@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <!-- Page Heading -->
 <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Tank</h1>
  </div>

  <!-- Content Row -->

  <div class="row">

    <!-- Area Chart -->
    <div class="col-xl-8 col-lg-7">
      <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 class="m-0 font-weight-bold text-primary">Edit Tank</h6>
        </div>
        <!-- Card Body -->
        <div class="card-body">
          <form action="{{ route('update_tank') }}" method="post">
                  @csrf
					<input type="hidden" name="id" value="{{ $data->id }}"/>
                    <div class="form-group">
						<input type="number" id="tank_size" name="tank_size" class="form-control number_only" min="0" value="{{ $data->tank_size }}" placeholder="Tank Size" required autofocus>
                    </div>
					<!--					
                    <div class="form-group">
						<input type="number" id="brewery_consumption" name="brewery_consumption" class="form-control number_only" min="0" max="{{ $data->tank_size }}" value="{{ $data->brewery_consumption }}" placeholder="Brewery Consumption (in BBLs)" required autofocus>
                    </div>

                    <div class="form-group">
						<input type="number" id="customer_consumption" name="customer_consumption" class="form-control number_only" min="0" max="{{ $data->tank_size }}" value="{{ $data->customer_consumption }}" placeholder="Customer Consumption (in BBLs)" required autofocus>
                    </div>
					
					<div class="form-group">
						<input type="number" id="available_capacity " name="available_capacity" min="0" class="form-control number_only" value="{{ $data->available_capacity }}" placeholder="Personal Use (in BBLs)" required autofocus>
                    </div>
					
					<div class="form-group">
						<input type="number" id="tank_cost" name="tank_cost" class="form-control" placeholder="Tank Cost" required autofocus>
                    </div>
                    -->
                    <div class="form-group">
						<select class="form-control" placeholder="tank_freq" name="tank_freq" required>
							<option value="">Select Tank Freqency</option>
							<option value="1" {{ $data->tank_freq == 1 ? 'selected' : '' }} >Single Use</option>
							<option value="2" {{ $data->tank_freq == 2 ? 'selected' : '' }} >Continuous Use</option>
						</select>
                    </div>
                    
                    <div class="form-group">
						<select class="form-control" placeholder="tank_status" name="tank_status" required>
							<option value="">Select Status</option>
							<option value="1" {{ $data->tank_status == 1 ? 'selected' : '' }} >Available</option>
							<option value="0" {{ $data->tank_status == 2 ? 'selected' : '' }} >Unavailable</option>
						</select>
                    </div>

					<div class="form-group">
						<input type="date" id="tank_date " name="tank_date" class="form-control" value="{{ $data->tank_date }}" placeholder="Date Available" required autofocus>
                    </div>
					
                    <button class="btn btn-lg btn-bbb-notActive btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">SUBMIT</button>
                  </form>
            </div>
      </div>
    </div>
  </div>

  </div>
  
<script>
$('#tank_size').keyup(function () { 
	var tankSize = this.value;
	$("#brewery_consumption").val('');
	$("#customer_consumption").val('');
	
	$("#brewery_consumption").attr('max',this.value);
	$("#customer_consumption").attr('max',this.value);
	
});

$('#brewery_consumption').keyup(function () { 
	var tankSize = $('#tank_size').val();
	var BreweryCon = this.value;
	var CustomerCon = tankSize - BreweryCon;

	if(CustomerCon < 0)
		CustomerCon = 0;
		
	$("#customer_consumption").val(CustomerCon);	
	$("#customer_consumption").attr('max',CustomerCon);
	$("#brewery_consumption").attr('max',tankSize);
	
});

$('#customer_consumption').keyup(function () { 
	var tankSize = $('#tank_size').val();
	var CustomerCon = this.value;
	var BreweryCon = tankSize - CustomerCon;

	if(BreweryCon < 0)
		BreweryCon = 0;
		
	$("#brewery_consumption").val(BreweryCon);	
	$("#brewery_consumption").attr('max',BreweryCon);
	$("#customer_consumption").attr('max',tankSize);
	
});

</script>
@endsection
