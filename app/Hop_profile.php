<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hop_profile extends Model
{
    protected $table = "hop_profile";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
