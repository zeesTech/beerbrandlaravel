@extends('layouts.app')

@section('content')

<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    #table_id thead {
        background-color: #FF6000;
        color: white;
    }

    #table_id thead tr {
        height: 60px;
    }
    #table_id .even { background-color: white }
    #table_id .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    #table_id tbody tr {
        height: 60px;
    }
	
	.come-from-modal.left .modal-dialog,
	.come-from-modal.right .modal-dialog {
		position: fixed;
		margin: auto;
		width: 420px;
		height: 100%;
		-webkit-transform: translate3d(0%, 0, 0);
		-ms-transform: translate3d(0%, 0, 0);
		-o-transform: translate3d(0%, 0, 0);
		transform: translate3d(0%, 0, 0);
	}

	.come-from-modal.left .modal-content,
	.come-from-modal.right .modal-content {
		height: 100%;
		overflow-y: auto;
		border-radius: 0px;
	}

	.come-from-modal.left .modal-body,
	.come-from-modal.right .modal-body {
		padding: 15px 15px 80px;
	}
	.come-from-modal.right.fade .modal-dialog {
		right: 0px;
		-webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
		-moz-transition: opacity 0.3s linear, right 0.3s ease-out;
		-o-transition: opacity 0.3s linear, right 0.3s ease-out;
		transition: opacity 0.3s linear, right 0.3s ease-out;
	}

	.come-from-modal.right.fade.in .modal-dialog {
		right: 0;
	}
	.color{
		background-color: #FF6000;
		color:#FFF;
	}
	.bold{
		font-weight : bold;
	}
	.tink{
		font-weight : 300;
	}
	
	.outerImg
    {
      position: relative;
      top: 0;
      left: 0;
    }
    .innerImg
    {
      position: absolute;
      top: 70px;
	  left: 140px;
    }
	
</style>



<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Beers</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
	<div class="row">
		<div class="col-md-6">
			<h6 class=" m-0 font-weight-bold text-primary">My Beers</h6>
		</div>
		<div class="col-md-3">
			<a href="javascript:void(0)" onclick="updateStatus()" class="customAdd-Btn text-uppercase float-right">Update Status</a>
		</div>
		<div class="col-md-3">
			<a href="{{ route('buildbeer') }}" class="customAdd-Btn text-uppercase float-right">Build a Beer</a>
		</div>
	</div>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="table_id" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Sr #</th>
            <th>Beer Name</th>
            <th>Beer Style</th>
            <th>Keg Size</th>
            <th>Unit Cost</th>
            <th>Beer Status</th>
			<th>Brew Beer</th>
            <th>Actions</th>
          </tr>
        </thead>
       
        <tbody>
		<?php
			$i = 0;
			foreach($data as $dt):
			$i++;
			$imgPath 	= (isset($dt->img_unique_name) && !empty($dt->img_unique_name)) ? asset('uploads/'.$dt->img_unique_name) : false;
			$notes 		= (isset($dt->notes) && !empty($dt->notes)) ? $dt->notes : false;
			$status 	= (isset($dt->status) && !empty($dt->status)) ? $dt->status : false;
			$beerStatusArray = array( "1" => "Brew this Beer" , "2" => "Brewery Matching" , "3" => "Brewing in Progress" , "4" => "Ready for Order" );
			$BeerStatus = (!empty($beerStatusArray) && isset($beerStatusArray[$dt->beer_status]) && !empty($beerStatusArray[$dt->beer_status])) ? $beerStatusArray[$dt->beer_status] : false;
			$style = "";
			if(!empty($status))
				$style = "background-color:#e3e6f0";
		?>
                    <tr style="{{$style}}">
						<input type="hidden" id="imgPath_<?php echo $i;?>" value="{{$imgPath}}"/>
						<input type="hidden" id="notes_<?php echo $i;?>" value="{{$notes}}"/>
                        <td>{{$i}}</td>
                        <td id="beer_name_<?php echo $i;?>">{{$dt->beer_name}}</td>
                        <td id="beer_style_name_<?php echo $i;?>">{{$dt->beer_style_name}}</td>
                        <td id="keg_size_<?php echo $i;?>">{{$dt->keg_sizes_name}}</td>
                        <td id="unit_cost_<?php echo $i;?>">{{ (!empty($dt->unit_cost)) ? $dt->unit_cost : "-" }}</td>
                        <td id="unit_status_<?php echo $i;?>">
							<?php
								if(!empty($dt->beer_status) && $dt->beer_status < 2)
								{
							?>
							<input type="hidden" name="beerIds[]" value="<?php echo $dt->beer_id;?>"/>
							<select class="form-control" placeholder="Select Status" name="status_id[]" id="status_id_<?php echo $i;?>" >
								<?php
									if(!empty($beerStatusArray))
									{
										for($k=1;$k<=count($beerStatusArray)-2;$k++)
										{		
											echo  '<option value="'.$k.'">'.$beerStatusArray[$k].'</option>';
										}
									}
								}
								else
								{
									echo $BeerStatus;
								}
								?>
							</select>
						</td>
						<td id="unit_cost_<?php echo $i;?>">
							<?php if(empty($status)){?>
								<a class="customBrew-Btn" onclick="BrewBeer(this)" data-beerID="{{$dt->beer_id}}" href="javascript:void(0)">Brew This Beer</a>
							<?php }else{ ?>
								<a class="customBrew-Btn" href="javascript:void(0)">Brewing</a>
							<?php } ?>
						</td>
                        <td>
							<div class="dropdown no-arrow">
								<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink<?php echo $i;?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink<?php echo $i;?>">
								<?php if(empty($status)){?>
									<a class="dropdown-item" href="{{ route('editbeer',['id' => $dt->beer_id]) }}">Edit This Beer</a>
									<a class="dropdown-item" onclick="deleteRow(this)" data-beerID="{{$dt->beer_id}}" href="javascript:void(0)">Delete This Beer</a>
								<?php }else if($status == 1 ){ ?>
									<a class="dropdown-item" onclick="StopBrewBeer(this)" data-beerID="{{$dt->beer_id}}" href="javascript:void(0)">Stop Brewing This Beer </a>
								<?php } ?>
									<a class="dropdown-item" href="javascript:void(0)" data-id="<?php echo $i;?>" data-toggle="modal" data-target="#myModal" onclick="loadBeer(this)">View This Beer</a>
								</div>
							</div>
						</td>
                    </tr>
            <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal fade  come-from-modal right" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header color">
				<h4 class="modal-title" id="myModalLabel">Meet Your Beer</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
		
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
						<div class="MeetBeer-Large" style="position: relative; left: 0; top: 0;">
							<img class="outerImg" src="{{URL::asset('assets/Image 1.png')}}" width="100%" height="300" alt="">
							<img class="innerImg" id="ImgPath" width="100px" height="100px"/>
						</div>
									
						<div class="MeetBeer-List">
							<h5>Name: <span class="bold" id="name"></span></h5>
							<h5>Beer Style: <span class="bold" id="beer_style"></span></h5>
							<!--<h5>IBU Range: <span class="bold" id="ibu"></span></h5>
							<h5>ABV Range: <span class="bold" id="abv"></span></h5>-->
							<h5>KEG Size: <span class="bold" id="keg_size"></span></h5>
							<h5>Beer Notes: <span class="tink" id="notes"></span></h5>
						</div>
						
					</div>
		
				</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default color" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

</div>

	<div class="modal fade in" id="addOrderPopup" data-backdrop="static">
	  <div class="modal-dialog modal-md">
		<div class="modal-content">
			<form action="javascript:void(0)" id="addOrderform" method="POST" enctype="multipart/form-data">
			  <input type="hidden" required id="BeerId" name="BeerId" required >
			  <div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Create Order</h4>
				<button type="button" class="close" onclick="resetOrderform()" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <div class="modal-body">
				<div class="row">
					<div class="col-md-12" >
						<div class="form-group" >
							<input type="text" class="form-control number_only" required id="qty" name="qty" autofocus="" placeholder="Quantity" >
						</div>
                    </div>
				</div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="customAdd-Btn" id="saveBtn" onclick="SaveOrder()">Create</button>
				<button type="reset" class="customClose-Btn" onclick="resetOrderform()">Close</button>
			  </div>
			</div>
		</form>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>

<script>
    $(document).ready( function () {
        $('#table_id').DataTable({
            searching: false, info: false,
            aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ -1 ]
                            }
            ]
        });
    } );
	
	function loadBeer(e)
	{
		var id = $(e).attr('data-id');
		if(id == '')
			return false;
			
		var beer_name = $("#beer_name_"+id).text();
		var beer_style_name = $("#beer_style_name_"+id).text();
		var keg_size = $("#keg_size_"+id).text();
		var abv_name = $("#abv_name_"+id).text();
		var ibu_name = $("#ibu_name_"+id).text();
		var unit_cost = $("#unit_cost_"+id).text();
		var imgPath = $("#imgPath_"+id).val();
		var notes = $("#notes_"+id).val();
		
		$("#name").text(beer_name);
		$("#beer_style").text(beer_style_name);
		$("#ibu").text(ibu_name);
		$("#abv").text(abv_name);
		$("#keg_size").text(keg_size);
		$("#notes").text(notes);
		
		$("#ImgPath").attr("src","");
		if(imgPath != '')
			$("#ImgPath").attr("src",imgPath);
	}

	function deleteRow(e)
	{
		var id = $(e).attr('data-beerID');
		if(id == '')
			return false;
		
		swal({
			title: "",
			text: "Do you really want to delete this beer?",
			type: "warning",
			showCancelButton: true,
			dangerMode: true,
			cancelButtonClass: '#DD6B55',
			confirmButtonColor: '#FF6000',
			confirmButtonText: 'Yes',
			cancelButtonText: 'No',
		},function (result) {
			if (result) {
				
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': "{{ csrf_token() }}"
					}
				});
				
				var type = "POST";
				var ajaxurl = '/deleteBeer';
				$.ajax({
					type: type,
					url: ajaxurl,
					data : {"id" : id },
					dataType: 'json',
					success: function (data) {
						location.reload();
					},
					error: function (data) {
						console.log(data);
					}
				});
			}
		});
		/*
		if (confirm('Do you really want to delete this beer?')) {
			
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});
			
			var type = "POST";
			var ajaxurl = '/deleteBeer';
			$.ajax({
				type: type,
				url: ajaxurl,
				data : {"id" : id },
				dataType: 'json',
				success: function (data) {
					location.reload();
				},
				error: function (data) {
					console.log(data);
				}
			});	
		}
		*/
	}
	
	function BrewBeerOld(e)
	{
		var id = $(e).attr('data-beerID');
		if(id == '')
			return false;
		
		if (confirm('If you want us to brew this beer, the next step will be to match your recipe with a brewing partner.  This is exciting!  Similar to how you order beer currently, you will place your order through the site and we will deliver your beer.  Please allow 4 weeks for the initial order to be created, after that we will continue to make this beer for you! Do you want us to begin brewing this beer? ')) {
			window.location.href = "brew_beer/"+id;
		}
	}
	
	function StopBrewBeer(e)
	{
		var id = $(e).attr('data-beerID');
		if(id == '')
			return false;

		swal({
			title: "",
			text: "Do you really want to stop brewing this beer?",
			type: "warning",
			showCancelButton: true,
			dangerMode: true,
			cancelButtonClass: '#DD6B55',
			confirmButtonColor: '#FF6000',
			confirmButtonText: 'Yes',
			cancelButtonText: 'No',
		},function (result) {
			if (result) {
				window.location.href = "stopBeerBrew/"+id;
			}
		});
		/*
		if (confirm('Do you really want to stop brewing this beer?')) {		
			window.location.href = "stopBeerBrew/"+id;
		}*/
	}

	function updateStatus()
	{
        var BeerIds = $("input[name='beerIds[]']").map(function(){return $(this).val();}).get();
        var StatusIds = $("select[name='status_id[]']").map(function(){return $(this).val();}).get();
		
		if(BeerIds == '')
			return false;
		
		swal({
                title: "",
                text: "Do you really want to change the status?",
                type: "warning",
                showCancelButton: true,
                dangerMode: true,
                cancelButtonClass: '#DD6B55',
                confirmButtonColor: '#FF6000',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
            },function (result) {
                if (result) {
                    
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': "{{ csrf_token() }}"
						}
					});
					
					var type = "POST";
					var ajaxurl = '/UpdateBeerStatus';
					$.ajax({
						type: type,
						url: ajaxurl,
						data : {"BeerIds" : BeerIds , "StatusIds" : StatusIds},
						dataType: 'json',
						success: function (data) {
							location.reload();
						},
						error: function (data) {
							console.log(data);
						}
					});
                }
            });
		/*	
		if (confirm('Do you really want to change the status?')) {
			
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});
			
			var type = "POST";
			var ajaxurl = '/UpdateBeerStatus';
			$.ajax({
				type: type,
				url: ajaxurl,
				data : {"BeerIds" : BeerIds , "StatusIds" : StatusIds},
				dataType: 'json',
				success: function (data) {
					location.reload();
				},
				error: function (data) {
					console.log(data);
				}
			});	
		}*/		
	}
	
	function resetOrderform()
	{
		$("#BeerId").val('');
		$("#addOrderform")[0].reset();
		$("#addOrderPopup").modal('hide');
	}

	function BrewBeer(e)
	{
		var id = $(e).attr('data-beerID');
		if(id == '')
			return false;
			
		$("#BeerId").val(id);
		$("#addOrderPopup").modal('show');
	}
	
	function SaveOrder()
	{
		if($("#qty").val() == '')
		{
			alert('Please Enter Quantity');
			return false;
		}
		if($("#BeerId").val() == '')
		{
			alert('Beer ID is missing');
			return false;
		}
			
		swal({
                title: "",
                text: "We’re excited to start brewing this beer.  We will begin matching your beer with a brewery.  Your initial batch will take 4-6 weeks, but then we should be able to have your beer in stock any time you place an order.  You will not be charged anything until you place your first order and the beer is delivered",
                type: "warning",
                showCancelButton: true,
                dangerMode: true,
                cancelButtonClass: '#DD6B55',
                confirmButtonColor: '#FF6000',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
            },function (result) {
                if (result) {
                    $.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': "{{ csrf_token() }}"
						}
					});
					
					$("#saveBtn").prop("disabled",true);
					
					var formData = $("#addOrderform").serialize();		
					var type = "POST";
					var ajaxurl = '/BeerBrew';
					$.ajax({
						type: type,
						url: ajaxurl,
						data: formData,
						dataType: 'json',
						success: function (data) {
							if(data.status)
							{
								location.reload();
							}
						},
						error: function (data) {
							console.log(data);
						}
					});
                }else{
					resetOrderform();
				}
            });
		/*
		if (confirm('If you want us to brew this beer, the next step will be to match your recipe with a brewing partner.  This is exciting!  Similar to how you order beer currently, you will place your order through the site and we will deliver your beer.  Please allow 4 weeks for the initial order to be created, after that we will continue to make this beer for you! Do you want us to begin brewing this beer? ')) {
			
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});
			
			$("#saveBtn").prop("disabled",true);
			
			var formData = $("#addOrderform").serialize();		
			var type = "POST";
			var ajaxurl = '/BeerBrew';
			$.ajax({
				type: type,
				url: ajaxurl,
				data: formData,
				dataType: 'json',
				success: function (data) {
					if(data.status)
					{
						location.reload();
					}
				},
				error: function (data) {
					console.log(data);
				}
			});	
		}*/
	}

</script>
@endsection
