<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Beer_style;
use App\Abv;
use App\Ibu;
use App\Reciepe;
use App\Revenue;
use App\Batchorder;

class HomebrewerpartnersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
    public function index(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('home_brewer_partner'))
		{
			$user_id = Auth::user()->id;
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function AddReciepe(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('home_brewer_partner'))
		{
			$data = array();
			$data['beer_styles'] 	= Beer_style::all();
			$data['abv'] 			= Abv::all();
			$data['ibu'] 			= Ibu::all();
			return view('home_brewer_partner/add_reciepe',['data' => $data]);
		}		
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	 
	public function CreateReciepe(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('home_brewer_partner'))
		{
			$data = $request->all();
			
			$image_original_name  = '';
			$image_unique_name = '';
			if($request->hasFile('image'))
			{
				$fileobj				= $request->file('image');
				$image_original_name 	= $fileobj->getClientOriginalName('image');
				$file_extension_name 	= $fileobj->getClientOriginalExtension('image');
				$image_unique_name 		= time().rand(1000,9999).'.'.$file_extension_name;
				$destinationPath		= public_path('/uploads/');
				$fileobj->move($destinationPath,$image_unique_name);
			}

			$data['type'] 				= "Home Brewery";
			$data['img_actual_name'] 	= $image_original_name;
			$data['img_unique_name'] 	= $image_unique_name;
			
			$data['user_id'] = Auth::user()->id;
			$reciepe = Reciepe::create($data);
			return redirect()->route('list_recipe');
		}
    }
	
	public function ListReciepe(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('home_brewer_partner'))
		{
			$user_id = Auth::user()->id;
			$data = array();
			//$data['reciepes'] = Reciepe::all();
			$data['reciepes'] = Reciepe::join('beer_style','beer_style.id','=','reciepes.beer_style_id')->
			join('ibu','ibu.id','=','reciepes.ibu_id')->
			join('abv','abv.id','=','reciepes.abv_id')->
			where( 'reciepes.user_id',$user_id )->
			select('reciepes.*','beer_style.name as beer_style_name','ibu.name as ibu_name','abv.name as abv_name')->get();
			//dd($data);
			return view('home_brewer_partner/list_reciepe',['data' => $data]);
		}
    } 
	
	public function ViewReciepe($id, Request $request){
		
		$user = $request->user();
		if($user->hasRole('home_brewer_partner'))
		{
			$data['reciepe'] = Reciepe::findorfail($id);
			return view('home_brewer_partner/view_reciepe',['data' => $data]);
		}
    }
	
	public function MyRevenue(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('home_brewer_partner'))
		{
			$user_id = Auth::user()->id;
			
			$data = array();
			$data['revenue'] = Batchorder::where("order_status" , "!=" , 0)
								->where(array("home_brewer_id" => $user_id))
								->join('beer_style','beer_style.id','=','batchorders.beer_style_id')
								->join('reciepes','reciepes.id','=','batchorders.reciepe_id')
								->join('revenues','revenues.batch_id','=','batchorders.id')
								->select('batchorders.id as batchID','batchorders.order_status','batchorders.name as batchName','batchorders.batch_size','batchorders.order_status','beer_style.name as beer_style_name','reciepes.beer_name as reciepe_name','revenues.keg_1_2_size_counter','revenues.keg_1_2_size_revenue','revenues.keg_1_6_size_counter','revenues.keg_1_6_size_revenue')
								->get();
			return view('home_brewer_partner/my_revenue',['data' => $data]);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
