<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tank extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'tank_size', 'tank_cost','tank_date','tank_freq','tank_status','available_capacity','brewery_consumption','customer_consumption'
    ];
}
