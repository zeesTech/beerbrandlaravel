<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tank;
use App\Brewery;
use Auth;
use App\Revenue;
use App\Beer_style;
use App\Keg_sizes;
use App\Batchorder;
use App\Reciepe;
use App\User;
use App\Message;
use App\Offerlog;
use App\Beers;

class BrewerpartnersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
    public function index(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('brewer_partner'))
		{
			$user_id = Auth::user()->id;
			$data = array();
			//$data = Tank::all();
			//dd($data);
			return view('brewer_partner/manage-beer',['data' => $data]);
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function MyRevenue(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('brewer_partner'))
		{
			$user_id = Auth::user()->id;
			
			$data = array();
			$data['revenue'] = Revenue::where('revenues.brewer_partner_id',$user_id)
			->where('order_status', '!=',0)
			->join('batchorders','batchorders.id','=','revenues.batch_id')
			->join('beer_style','beer_style.id','=','batchorders.beer_style_id')
			->join('reciepes','reciepes.id','=','batchorders.reciepe_id')
			->select('revenues.*','beer_style.name as beer_style_name','reciepes.beer_name as reciepe_name','order_status')->get();
			return view('brewer_partner/my_revenue',['data' => $data]);
		}		
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	 
	public function AddTank(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('brewer_partner'))
		{
			$data = array();
			return view('brewer_partner/add_tank',['data' => $data]);
		}
    }
	 
	public function CreateTank(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('brewer_partner'))
		{
			$data = $request->all();
			$data['user_id'] = Auth::user()->id;
			Tank::create($data);
			return redirect()->route('list_tank');
		}
    }
	
    public function ListTank(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('brewer_partner'))
		{
			$data = array();
			$user_id = Auth::user()->id;
			$data = Tank::where('user_id',$user_id)->latest()->get();
			return view('brewer_partner/list_tank',['data' => $data]);
		}
    }

	public function EditTank($id, Request $request)
	{
		$user = $request->user();
		if($user->hasRole('brewer_partner'))
		{
			if(!empty($id))
			{
				$data = Tank::where('id',$id)->first();
				return view('brewer_partner/edit_tank',compact('data'));
			}
			else
			{
				return redirect('/list_tank');
			}	
		}		
	}
	
	public function UpdateTank(Request $request)
	{
		$user = $request->user();
		if($user->hasRole('brewer_partner'))
		{
			$input = $request->all();
			$id = (isset($input['id']) && !empty($input['id'])) ? $input['id'] : false;
			
			unset($input['_token']);
			unset($input['id']);
			
			Tank::where('id', $id)->update($input);			
		}	

		return redirect('/list_tank');
	}
	
	public function deleteTank(Request $request,$id)
	{
		$user = $request->user();
		if($user->hasRole('brewer_partner'))
		{
			if(!empty($id))
			{
				$user_id = Auth::user()->id;
				Tank::where(array('id' => $id, 'user_id' => $user_id ))->delete();
			}
		}	

		return redirect('/list_tank');
	}
	
	public function Order(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('brewer_partner'))
		{
			$user_id = Auth::user()->id;
			$data = array();
			$data = Batchorder::where(array("partner_id" => $user_id , "brewer_partner_id" => $user_id))
								->join('beer_style','beer_style.id','=','batchorders.beer_style_id')
								->join('reciepes','reciepes.id','=','batchorders.reciepe_id')
								->join('revenues','revenues.batch_id','=','batchorders.id')
								->select('batchorders.id as batchID','batchorders.name as batchName','batchorders.reciepe_id','batchorders.batch_size','batchorders.order_status','batchorders.brew_date','batchorders.pickup_date','beer_style.name as beer_style_name','reciepes.beer_name as reciepe_name','revenues.keg_1_2_size_counter','revenues.keg_1_2_size_revenue','revenues.keg_1_6_size_counter','revenues.keg_1_6_size_revenue')
								->get();
			return view('brewer_partner/order',['data' => $data]);
		}
    }
	
	public function UpdateBreweryProfile(Request $request)
    {
		$id = auth()->user()->id;
		if(!empty($id))
		{
			$input = $request->all();

			if($request->hasFile('image'))
			{
				$fileobj				= $request->file('image');
				$image_original_name 	= $fileobj->getClientOriginalName('image');
				$file_extension_name 	= $fileobj->getClientOriginalExtension('image');
				$image_unique_name 		= time().rand(1000,9999).'.'.$file_extension_name;
				$destinationPath		= public_path('/uploads/');
				$fileobj->move($destinationPath,$image_unique_name);
				
				$input['img_actual_name'] 	= $image_original_name;
				$input['img_unique_name'] 	= $image_unique_name;
			}

			unset($input['_token']);
			unset($input['image']);
			unset($input['_method']);
		
			$check = Brewery::where('user_id', $id)->get();
			if(!empty($check))
				Brewery::where('user_id', $id)->update($input);
			else
				Brewery::create($input);
		}
		
		return redirect('/profile');
    }
	
	public function changeStatusOfBatch(Request $request)
	{
		$user = $request->user();
		if($user->hasRole('brewer_partner'))
		{
			$data = $request->all();
			$id 		= (!empty($data) && isset($data['batch_id']) && !empty($data['batch_id'])) ? $data['batch_id'] : false;
			$status_id 	= (!empty($data) && isset($data['status_id']) && !empty($data['status_id'])) ? $data['status_id'] : false;
			$brew_date 	= (!empty($data) && isset($data['brew_date']) && !empty($data['brew_date'])) ? $data['brew_date'] : false;
			$pickup_date 	= (!empty($data) && isset($data['pickup_date']) && !empty($data['pickup_date'])) ? $data['pickup_date'] : false;
			if(!empty($id) && !empty($status_id))
			{
				$updateArray['order_status'] = $status_id;									
				$updateArray['brew_date'] = $brew_date;
				$updateArray['pickup_date'] = $pickup_date;
					
				Batchorder::where('id', $id)->update($updateArray);
				
				return response()->json(array('status'=> true, "msg" => "successfully."), 200);
			}	
			else
			{
				return response()->json(array('status'=> false, "msg" => "something went wrong."), 200);
			}
		}		
	}

	public function ViewReciepe($id, Request $request){
		
		$user = $request->user();
		if($user->hasRole('brewer_partner'))
		{
			$data['reciepe'] = Reciepe::findorfail($id);
			return view('brewer_partner/view_reciepe',['data' => $data]);
		}
    }
	
	public function Messages(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('brewer_partner'))
		{
			$data['user'] = User::join('users_roles','users_roles.user_id','=','users.id')
						->join('roles','roles.id','=','users_roles.role_id')
						->where('roles.slug',"=","administrator")
						->select('users.id','users.name','users.email','users.phone','users.address','users.notes','users.biography','users.img_unique_name','roles.name as role_name')->get();
			
			$data['contactList'] = Message::join('users','messages.sender_id','=','users.id')
									->where("messages.revicer_id",Auth::user()->id)
									->select('users.id','users.name','users.email','users.phone','users.address','users.notes','users.biography','users.img_unique_name','messages.message')
									->orderBy('messages.created_at','DESC')
									->get();
			
			return view('brewer_partner/messages',['data' => $data]);
		}		
    }

	public function saveMessage(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('brewer_partner'))
		{
			$array['sender_id'] = Auth::user()->id;
			$array['revicer_id'] = $request['revicerID']; 
			$array['message'] = $request['message-input'];  
			$array['flag'] = 'brewer_partner';  
			Message::create($array);
			
			return response()->json(array('status'=> true, "msg" => "successfully"), 200);
		}		
    }
	
	public function loadChat(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('brewer_partner'))
		{
			$id 	= Auth::user()->id;
			$html 	= "";
			$Messages = Message::where("revicer_id",$id)->orWhere("sender_id",$id)->get();
			Message::where("flag","admin")->Where(array( "revicer_id" => $id , "is_read" => 0 ))->update( array("is_read" => 1 ));
			
			if(!empty($Messages))
			{
				foreach($Messages as $key => $message)
				{
					if($message['flag'] == "brewer_partner")
					{
						$html .= '<div class="UserChat-Left">
									<img src="asset/image/default-image.png" width="70" height="70" alt=""/>
									<div class="InnerChat-Head">
										<h1>'.$message['message'].'</h1>
										<h2>'.date_format($message['created_at'],"h:i A").'</h2>
									</div>
								</div>';
					}
					else
					{
						$html .= '<div class="UserChat-Right">
									<img src="assets/logo.png" width="70" height="70" alt=""/>
									<div class="InnerReply-Head">
										<h1>'.$message['message'].'</h1>
										<h2>'.date_format($message['created_at'],"h:i A").'</h2>
									</div>
								</div>';
					}
				}
			}
			else
			{
				$html .= '<div class="UserChat-Left emptyMessage">
							<div class="InnerChat-Head">
								<h1>No Messages</h1>
							</div>
						</div>';
			}
			return response()->json(array('status'=> true, "html" => $html ), 200);
		}		
    }
	
	public function offers(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('brewer_partner'))
		{
			$data = array();
			$user_id = Auth::user()->id;
			$data = Offerlog::join('batchorders','batchorders.id','=','offerlogs.batch_id')
					->join('beer_style','beer_style.id','=','batchorders.beer_style_id')
					->join('brewers_inspiration','brewers_inspiration.id','=','batchorders.brewers_inspiration_id')
					->where(array('offerlogs.partner_id' => $user_id , "batchorders.offer_status" => 1))
					->latest()
					->get(['batchorders.*','beer_style.name as beer_style_name','brewers_inspiration.name as brewers_inspiration_name']);
			return view('brewer_partner/offers',['data' => $data]);
		}
    }
	
	public function AcceptOffer(Request $request)
	{
		$user = $request->user();
		if($user->hasRole('brewer_partner'))
		{
			$data = $request->all();
			$id 		= (!empty($data) && isset($data['batch_id']) && !empty($data['batch_id'])) ? $data['batch_id'] : false;
			$PartnerID 	= Auth::user()->id;
			if(!empty($id) && !empty($PartnerID))
			{
				$batchData = Batchorder::where('id', $id)->first(['beer_ids','beer_style_id','reciepe_id','user_id']);
				if(!empty($batchData) && isset($batchData['beer_ids']) && !empty($batchData['beer_ids']))
				{
					$beer_ids = json_decode($batchData['beer_ids']);
					if(!empty($beer_ids))
					{
						Beers::whereIn('id', $beer_ids)->update(array( "status" => "3" ));
					}
					
					$HomeBrewerID = 0;
					$BrewerPartnerID = 0;
					if(!empty($batchData) && isset($batchData->reciepe_id) && !empty($batchData->reciepe_id))
					{
						$RecipeData = Reciepe::where('id', $batchData->reciepe_id)->first();
						if(!empty($RecipeData) && isset($RecipeData->user_id) && !empty($RecipeData->user_id))
						{
							$HomeBrewerID 					= $RecipeData->user_id;
							
							$para['notification_type_id'] 	= 3; // for 3 Attach Recipe
							$para['sender_id'] 				= $batchData->user_id;
							$para['receiver_id'] 			= $RecipeData->user_id;
							$para['action'] 				= "Attach Recipe";
							$para['action_id'] 				= $id;
							
							app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
						}
					}
					
					if(!empty($PartnerID) && isset($PartnerID) && !empty($PartnerID))
					{
						$BrewerPartnerID 				= $PartnerID;
						
						$para['notification_type_id'] 	= 2; // for 2 Brew This Batch
						$para['sender_id'] 				= $batchData->user_id;
						$para['receiver_id'] 			= $PartnerID;
						$para['action'] 				= "Brew this Batch";
						$para['action_id'] 				= $id;
						
						app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
					}
				
					$keg_Array = array();
					$key_1_2_cost = 0;
					$key_1_6_cost = 0;
					$total_cost = 0;
					if(!empty($batchData) && isset($batchData->beer_ids) && !empty($batchData->beer_ids))
					{
						$BeerData = Beers::whereIn('id', json_decode($batchData->beer_ids))->get();
						if(!empty($BeerData))
						{
							foreach($BeerData as $index => $beer)
							{
								$para['notification_type_id'] 	= 4; // for 4 Beer in process
								$para['sender_id'] 				= $batchData->user_id;
								$para['receiver_id'] 			= $beer->user_id;
								$para['action'] 				= "Beer in process";
								$para['action_id'] 				= $id;
								
								app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
								
								if(!empty($beer->keg_size_id))
								{
									if($beer->keg_size_id == 1)
										$key_1_2_cost = $key_1_2_cost + $beer->total_cost;
									else if($beer->keg_size_id == 2)
										$key_1_6_cost = $key_1_6_cost + $beer->total_cost;
										
									$keg_Array[$beer->keg_size_id][] = $beer->id;
								}
								
								$total_cost = $key_1_2_cost + $key_1_6_cost;
							}
						}
					}
				
					if(!empty($PartnerID) && isset($PartnerID) && !empty($PartnerID))
					{						
						$para['notification_type_id'] 	= 7; // for 7 Offer Acceptance
						$para['sender_id'] 				= $PartnerID;
						$para['receiver_id'] 			= "-1";
						$para['action'] 				= "Offer Acceptance";
						$para['action_id'] 				= $id;
						
						app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
					}

					Batchorder::where(array('id' => $id))->update(array( "offer_status" => "2" , "order_status" => "1" , "partner_id" => $PartnerID ,"keg_1_2_size_beer_cost" => $key_1_2_cost ,"keg_1_6_size_beer_cost" => $key_1_6_cost ,"total_cost" => $total_cost , 'order_date' => date('Y-m-d') ));
					
					$RevenuePara['batch_id']			= $id;
					$RevenuePara['kegs']				= $keg_Array;
					$RevenuePara['beer_style_id']		= (!empty($batchData) && isset($batchData->beer_style_id)) ? $batchData->beer_style_id : false;
					$RevenuePara['reciepe_id']			= (!empty($batchData) && isset($batchData->reciepe_id)) ? $batchData->reciepe_id : false;
					$RevenuePara['home_brewer_id']		= $HomeBrewerID;
					$RevenuePara['brewer_partner_id']	= $BrewerPartnerID;
					$RevenuePara['createdBy']			= $batchData->user_id;
					
					//echo'<pre>'; print_r($RevenuePara); die;
					app('App\Http\Controllers\RevenueController')->CreateHomeBrewerRevenueByOrder($RevenuePara);
					app('App\Http\Controllers\RevenueController')->CreateBreweryPartnerRevenueByOrder($RevenuePara);
				
				}
				
				return response()->json(array('status'=> true, "msg" => "successfully."), 200);
			}	
			else
			{
				return response()->json(array('status'=> true, "msg" => "something went wrong."), 200);
			}
		}		
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
