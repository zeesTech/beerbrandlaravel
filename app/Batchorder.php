<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batchorder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name','brewers_inspiration_id','beer_style_id', 'beer_ids','batch_size','partner_id','reciepe_id','order_status','updated_by','batch_notes','notes','offer_status'
    ];
}
