@extends('layouts.app')
@section('content')
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .profile-form {
        margin-top: 30px;
    }
    .profile-form input {
        width: 90%;
    }
    </style>
   <div class="container">
        
        <div class="row" style="margin-top: 0px;">
            
            <?php
				$heading = 'Basic Information';
				$Biography = 'Biography';
				$RoleSlug = (isset(Auth::user()->roles->first()->slug)) ? Auth::user()->roles->first()->slug : false;
				if(!empty($RoleSlug) && $RoleSlug == "brewer_partner")
				{
					$heading = 'Brewer’s Information';
					$Biography = 'About the Brewer';
					
				}
			?>
            <div class="col-md-6">
            <form class="profile-form" action="{{ route('update_profile') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <h3 class="login-heading mb-4">{{$heading}}</h3>
                
				<div class="form-group">
                    <input type="text" id="" class="form-control" placeholder="First Name" name="fname" value="{{ Auth::user()->fname }}" required autofocus>
                </div>

				<div class="form-group">
                    <input type="text" id="" class="form-control" placeholder="Last Name" name="lname" value="{{ Auth::user()->lname }}" required autofocus>
                </div>
				
                <div class="form-group">
                    <input type="text" class="form-control" required="" placeholder="Email" name="" value="{{ Auth::user()->email }}" autofocus="" readonly >
                </div>

				<div class="form-group">
					<input type="text" class="form-control number_only" required="" placeholder="Phone Number" name="phone" value="{{ Auth::user()->phone }}" autofocus="">
				</div>
				
				<!--<div class="form-group">
					<input type="file" id="image" name="image" class="form-control" placeholder="image" onchange="allowonlyImg(this)" >
				</div>-->
				
				<div class="form-group">
                    <textarea type="text" class="form-control" style="width:90%" rows="5" cols="1" placeholder="{{$Biography}}" name="biography" autofocus="" >{{ Auth::user()->biography }}</textarea>
                </div>
				<?php
					if(!empty($RoleSlug) && $RoleSlug != "brewer_partner")
					{
				?>
						<div class="form-group">
							<textarea type="text" class="form-control" style="width:90%" rows="5" cols="1" placeholder="Address" required name="address" autofocus="" >{{ Auth::user()->address }}</textarea>
						</div>
				<?php
					}
				?>
                <!--<div class="form-group">
                    <input type="text" class="form-control" required="" placeholder="Phone Number" name="phone" autofocus="">
                </div>
				
                <h3 class="login-heading mb-4">Change Password</h3>
                <div class="form-group">
                    <input type="text" class="form-control" required="" placeholder="Password" name="" autofocus="">
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" required="" placeholder="Confirm Password" name="" autofocus="">
                </div>-->
                
                <div class="btn-inline">
                    <button class="btn btn-lg btn-bbb-notActive btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Update</button>
                </div>
            </form>
            </div>
			
			<?php
				$RoleSlug = (isset(Auth::user()->roles->first()->slug)) ? Auth::user()->roles->first()->slug : false;
				if(!empty($RoleSlug) && $RoleSlug == "brewer_partner")
				{
					$id = auth()->user()->id;
					$breweryData =  \App\Brewery::where(['user_id' => $id])->first();
			?>
					<div class="col-md-6">
					<form class="profile-form" action="{{ route('update_brewery_profile') }}" method="POST" enctype="multipart/form-data">
						@csrf
						<h3 class="login-heading mb-4">Brewery Information</h3>
						<div class="form-group">
							<input type="text" id="" class="form-control" placeholder="Brewery Name" name="name" value="{{ $breweryData['name'] }}" required autofocus>
						</div>

						<div class="form-group hide">
							<input type="text" class="form-control" placeholder="Email" name="" value="{{ $breweryData['email'] }}" autofocus="" readonly >
						</div>
						
						<div class="form-group hide">
							<input type="text" class="form-control number_only" placeholder="Phone Number" name="phone" value="{{ $breweryData['phone'] }}" autofocus="">
						</div>
						
						<div class="form-group hide">
							<input type="file" id="image" name="image" class="form-control" placeholder="image" onchange="allowonlyImg(this)" >
						</div>
						
						<div class="form-group">
							<textarea type="text" class="form-control" style="width:90%" rows="5" cols="1" required="" placeholder="Tell us about the Brewery" name="description" autofocus="" >{{ $breweryData['description'] }}</textarea>
						</div>
						
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Address" name="location" value="{{ $breweryData['location'] }}" autofocus="" >
						</div>
						
						<div class="btn-inline">
							<button class="btn btn-lg btn-bbb-notActive btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Update Brewery</button>
						</div>
					</form>
					</div>
			<?php } ?>
        </div>
</div> 

@endsection