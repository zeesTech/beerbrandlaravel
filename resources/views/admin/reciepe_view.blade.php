@extends('layouts.app')

@section('content')
<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    #table_id thead {
        background-color: #FF6000;
        color: white;
    }

    #table_id thead tr {
        height: 60px;
    }
    #table_id .even { background-color: white }
    #table_id .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    #table_id tbody tr {
        height: 60px;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">

		<div class="row" style="margin-top: 30px;">
		
			<div class="col-md-4"></div>
			<div class="col-md-4">
			 <div class="logo">
				<p style="color: #FF6000 !important;
				text-align: center;
				font-size: 33px;
				font-style: bold;
				font-weight: bold;
				font-style: normal;
				margin: 0px 0px 20px 0;">Reciepe Detail Page</p>
			</div>
			</div>
			<div class="col-md-4"></div>	
		</div>


<!-- Page Heading -->

<!-- DataTales Example -->
<div class="card shadow mb-4">

  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="table_id" width="100%">
        <tbody>
			@if(isset($data['reciepe']) && !empty($data['reciepe']))
				<?php 
					if(!empty($data['reciepe']->img_unique_name))
						$src = asset('uploads/'.$data['reciepe']->img_unique_name);
					else 
						$src = asset('assets/Buildabrand/Group 249.png')
				?>
				<tr>
					<td width="10%"><img src="{{ $src }}" width="100%"></td>
				</tr>
				<tr>
					<td width="90%">
						<b>{{$data['reciepe']->beer_name}}</b> 
						<br> 
						{{$data['reciepe']->notes}}
					</td>
				</tr>
			@endif
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>

@endsection
