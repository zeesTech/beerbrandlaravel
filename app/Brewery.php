<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brewery extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user_id', 'email','img_actual_name','img_unique_name','phone','description','status','location'
    ];
}
