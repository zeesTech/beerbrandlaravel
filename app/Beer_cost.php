<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beer_cost extends Model
{
    protected $table = "beer_costs";
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','beer_style_id','beer_keg_size_id','cost','rate','created_by','updated_by'
    ];
}
