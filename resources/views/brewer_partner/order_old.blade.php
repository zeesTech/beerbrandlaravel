@extends('layouts.app')

@section('content')
<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    #table_id thead {
        background-color: #FF6000;
        color: white;
    }

    #table_id thead tr {
        height: 60px;
    }
    #table_id .even { background-color: white }
    #table_id .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    #table_id tbody tr {
        height: 60px;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Orders</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Orders</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="table_id" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Batch ID</th>
            <th>Brew Date</th>
            <th>Pickup Date</th>
            <th>Keg Quantity (1/2 BBL)</th>
            <th>½ BBL Revenue</th>
            <th>Keg Quantity (1/6 BBL)</th>
            <th>1/6 BBL Revenue</th>
            <th>Total Beer Revenue</th>
            <th>Beer Style & Recipe</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
       
        <tbody>
			<?php 
				$StatusArray = array( 1 => "Ready to brew", 2 => "Brewing", 3 => "Ready for Pickup", 4 => "Completed");
				//echo '<pre>'; print_r($data->toArray()); die;
				if(!empty($data)):
				$i = 0;
				foreach($data as $dt):
					$i++;
					$order_status = (isset($dt->order_status) && !empty($dt->order_status)) ? $dt->order_status : false;
					$status = (!empty($StatusArray) && isset($StatusArray[$order_status]) && !empty($StatusArray[$order_status])) ? $StatusArray[$order_status] : "-";

					$brew_date 		= (isset($dt->brew_date) && !empty($dt->brew_date)) ? $dt->brew_date : "-";
					$pickup_date 	= (isset($dt->pickup_date) && !empty($dt->pickup_date)) ? $dt->pickup_date : "-";
					
					$action = '-';
					if($order_status != 4)
					{
						$action = '
							<div class="dropdown no-arrow">
								<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink'.$i.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink'.$i.'">
									<a class="dropdown-item" href="javascript:void(0)" onclick="editDetail(this)" data-id="'.$dt->batchID.'" data-order_status="'.$order_status.'" data-brew_date="'.$brew_date.'" data-pickup_date="'.$pickup_date.'">Edit</a>
								</div>
							</div>
						';
					}
					

			?>
                    <tr>
                        <td>{{$dt->batchName}}</td>
                        <td>{{$brew_date}}</td>
                        <td>{{$pickup_date}}</td>
                        <td>{{number_format($dt->keg_1_2_size_counter)}}</td>
                        <td>${{number_format($dt->keg_1_2_size_revenue)}}</td>
                        <td>{{number_format($dt->keg_1_6_size_counter)}}</td>
                        <td>${{number_format($dt->keg_1_6_size_revenue)}}</td>
                        <td>${{number_format( $dt->keg_1_2_size_revenue + $dt->keg_1_6_size_revenue )}}</td>
                        <td>{{$dt->beer_style_name}} & {{$dt->reciepe_name}}</td>
                        <td>{{$status}}</td>
                        <td><?php echo $action?></td>
                    </tr>
			<?php
                endforeach;
			endif;
			?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>

	<div class="modal fade in" id="EditPopup" data-backdrop="static">
	  <div class="modal-dialog modal-md">
		<div class="modal-content">
			<form action="javascript:void(0)" id="Editform" method="POST" enctype="multipart/form-data">
			  <input type="hidden" name="batch_id" id="batch_id" />
			  <div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Edit</h4>
				<button type="button" class="close" onclick="resetEditform()" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <div class="modal-body">
				<div class="row">
				
					<div class="col-md-12" >
						<div class="form-group" >
							<input type="date" placeholder="Brew Date" class="form-control" id="brew_date" name="brew_date" />
						</div>
                    </div>
					
					<div class="col-md-12" >
						<div class="form-group" >
							<input type="date" placeholder="Pickup Date" class="form-control" id="pickup_date" name="pickup_date" />
						</div>
                    </div>
					
					<div class="col-md-12" >
						<div class="form-group" >
							<select class="form-control" placeholder="Select Status" name="status_id" id="status_id" >
								<?php
									if(!empty($StatusArray))
									{
										for($k=1;$k<=count($StatusArray)-1;$k++)
										{		
											echo  '<option value="'.$k.'">'.$StatusArray[$k].'</option>';
										}
									}
								?>
							</select>
						</div>
                    </div>
				</div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="customAdd-Btn" id="saveBtn" onclick="SaveStatus()">Save</button>
				<button type="reset" class="customClose-Btn" onclick="resetEditform()">Close</button>
			  </div>
			</div>
		</form>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>

<script>
    $(document).ready( function () {
        $('#table_id').DataTable({
            searching: false, info: false,
            aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ -1 ]
                            }
            ]
        });
    } );
	
function editDetail(e)
{
	var batchID 	= $(e).attr('data-id');
	var brew_date 	= $(e).attr('data-brew_date');
	var pickup_date = $(e).attr('data-pickup_date'); 
	var status 		= $(e).attr('data-order_status');
	if(batchID == '')
		return false;
		
	$("#batch_id").val(batchID);
	$("#brew_date").val(brew_date);
	$("#pickup_date").val(pickup_date);
	$("#status_id").val(status);
	$("#EditPopup").modal('show');
}

function SaveStatus()
{
	var StatusID = $("#status_id").val();
	if(StatusID == '')
	{
		alert("Please Select Status");
		return false;
	}
		
	var BatchID = $("#batch_id").val();
	if(BatchID == '')
	{
		alert("Something went wrong.!");
		return false;
	}
			
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var formData = $("#Editform").serialize();
	var type = "POST";
	var ajaxurl= "/changeStatusOfBatch";
	
	$.ajax({
		type: type,
		url: ajaxurl,
		data: formData,
		dataType: 'json',
		success: function (data) {
			if(data.status)
				location.reload();
			else
			{
				alert(data.msg);
			}
			
		},
		error: function (data) {
			console.log(data);
		}
	});
}

function resetEditform()
{
	$("#Editform")[0].reset();
	$("#batch_id").val('');
	$("#EditPopup").modal('hide');
}
</script>
@endsection
