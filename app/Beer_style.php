<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beer_style extends Model
{
    protected $table = "beer_style";
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
