<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reciepe extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','beer_name','beer_style_id','abv_id','ibu_id','similar_beer','awards','notes','img_actual_name','img_unique_name','type'
    ];
}
