<?php

use Illuminate\Database\Seeder;

class kegSizesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('keg_sizes')->insert([
            ['name' => 	'½ BBL (15.5 GALLONS)'],
            ['name' => 	'1/6 BBL (5.13 GALLONS)']
        ]);
    }
}
