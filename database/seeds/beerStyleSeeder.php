<?php

use Illuminate\Database\Seeder;

class beerStyleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('beer_style')->insert([
            ['name' => 	'LIGHT LAGER'],
            ['name' => 	'PILSNER'],
            ['name' => 	'AMBER'],
            ['name' => 	'DARK LAGER'],
            ['name' => 	'PALE ALE'],
            ['name' => 	'BROWN ALE'],
            ['name' => 	'PORTER'],
            ['name' => 	'STOUT'],
            ['name' => 	'PASTRY STOUT'],
            ['name' => 	'IPA'],
            ['name' => 	'HAZY IPA'],
            ['name' => 	'HEFEWEISEN'],
            ['name' => 	'BELGIAN ALE – YELLOW'],
            ['name' => 	'BELGIAN ALE - DARK'],
            ['name' => 	'SOUR ALE'],
            ['name' => 	'STRONG ALE'],
            ['name' => 	'FRUIT BEER'],
            ['name' => 	'CIDER']
        ]);
    }
}
