@extends('layouts.app')

@section('content')
<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    #table_id thead {
        background-color: #FF6000;
        color: white;
    }

    #table_id thead tr {
        height: 60px;
    }
    #table_id .even { background-color: white }
    #table_id .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    #table_id tbody tr {
        height: 60px;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Brands</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
	<div class="row">
		<div class="col-md-8">
			<h6 class="m-0 font-weight-bold text-primary">My Brands</h6>
		</div>
		<div class="col-md-4">
			<a href="{{ route('buildbrand') }}" class="customAdd-Btn text-uppercase float-right">Add Another Brand</a>
		</div>
	</div>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="table_id" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Brand Name</th>
            <th>email</th>
            <th>Address</th>
            <th>Contact Name</th>
            <th>Phone</th>
            <th>Action</th>
          </tr>
        </thead>
       
        <tbody>
        <?php $i=0;
			foreach($data as $dt): $i++; ?>
		
                    <tr>
                        <td>{{$dt->brand_name}}</td>
                        <td>{{$dt->email}}</td>
                        <td>{{$dt->address}}</td>
                        <td>{{$dt->contact_name}}</td>
                        <td>{{$dt->number}}</td>
                        <td>
							<div class="dropdown no-arrow">
								<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink<?php echo $i;?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink<?php echo $i;?>">
								  <!--<a class="dropdown-item" style="color:#FF6000;" href="{{ route('brand.delete_brand',['id' => $dt->id]) }}"><i class="fas fa-fw fa-trash"></i> Delete</a>-->
								  <a class="dropdown-item" style="color:#FF6000;" onclick="deleteRow(this)" data-id="{{$dt->id}}" href="javascript:void(0)"><i class="fas fa-fw fa-trash"></i> Delete</a>
								</div>
							</div>
						</td>
                    </tr>
        <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>

<script>
    $(document).ready( function () {
        $('#table_id').DataTable({
            searching: false, info: false,
            aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ -1 ]
                            }
            ]
        });
    } );
	
	function deleteRow(e)
	{
		var id = $(e).attr('data-id');
		if(id == '')
			return false;
			
		swal({
			title: "",
			text: "Do you really want to delete this brand?",
			type: "warning",
			showCancelButton: true,
			dangerMode: true,
			cancelButtonClass: '#DD6B55',
			confirmButtonColor: '#FF6000',
			confirmButtonText: 'Yes',
			cancelButtonText: 'No',
		},function (result) {
			if (result) {
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': "{{ csrf_token() }}"
					}
				});
				
				var type = "POST";
				var ajaxurl = '/DeleteBrand';
				$.ajax({
					type: type,
					url: ajaxurl,
					data : {"id" : id },
					dataType: 'json',
					success: function (data) {
						location.reload();
					},
					error: function (data) {
						console.log(data);
					}
				});	
			}
		});
		
		/*
		if (confirm('Do you really want to delete this brand?')) {
			
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});
			
			var type = "POST";
			var ajaxurl = '/BrandDelete';
			$.ajax({
				type: type,
				url: ajaxurl,
				data : {"id" : id },
				dataType: 'json',
				success: function (data) {
					location.reload();
				},
				error: function (data) {
					console.log(data);
				}
			});	
		}
		*/
	}
</script>
@endsection
