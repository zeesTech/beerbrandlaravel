<?php

use Illuminate\Database\Seeder;

class hopProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hop_profile')->insert([
            ['name' => 	'TRADITIONAL'],
            ['name' => 	'UNIQUE'],
            ['name' => 	'EXPERIMENTAL']
            ]);
    }	
    
}
