@extends('layouts.app')

@section('content')

 <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Optional JavaScript -->

<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    .table thead {
        background-color: #FF6000;
        color: white;
    }

    .table thead tr {
        height: 60px;
    }
    .table .even { background-color: white }
    .table .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    .table tbody tr {
        height: 60px;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
	<div class="customContainer">
                
		<div class="row" >
			<h2>Unassigned Orders</h2>
		</div>
		
		
		<form action="javascript:void(0)" method="post" id="OrderFilter">
			<div class="row" >
				<div class="col-md-2" >
					<div class="form-group" >
						<select class="form-control formdata" placeholder="Beer Style" name="beer_style_id" id="beer_style_id">
							<option value="" >Beer Style</option>
							@foreach($filter_data['beer_styles'] as $dt)
								<option value="{{$dt['id']}}">{{$dt['name']}}</option>
							@endforeach
						</select>
                    </div>
				</div>
				<div class="col-md-2" >
					<div class="form-group" >
						<select class="form-control formdata" placeholder="Beer Style" name="keg_size_id" >
							<option value="" >Keg Size</option>
							@foreach($filter_data['keg_sizes'] as $dt)
								<option value="{{$dt['id']}}">{{$dt['name']}}</option>
							@endforeach
						</select>
                    </div>
				</div>
				<div class="col-md-2" >
					<div class="form-group" >
						<button class="customAdd-Btn" type="submit" onclick="filter()">Filter</button>
                    </div>
				</div>
				<div class="col-md-2" >
					<div class="form-group" >
						<button class="customAdd-Btn" style="display:none;" id="createBatch-Btn" type="button" onclick="assignBatch()">Assign Batch</button>
                    </div>
				</div>
			</div>
		</form>
		
			
		<div class="row tabledesign">
			<table class="table table-bordered" id="table_id" class="display" style="width:100%">
				<thead>
					<tr>
						<th>Sr# <input type="checkbox" class="all-checkbox"></th>
						<th>Customer Name</th>
						<th>Brand</th>
						<th>Order Size</th>
						<!--<th>Address</th>
						<th>Phone Number</th>
						<th>Email</th>-->
						<th>Beer Type</th>
						<th>Brewers Motivation</th>
						<!--<th>Keg Size</th>
						<th>Weekly Usage</th>-->
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="OrderTbody">

				</tbody>
			</table>
		</div>
    </div>

</div>

	<div class="modal fade in" id="DetailPopup" data-backdrop="static">
	  <div class="modal-dialog modal-xl mw-100 w-75">
		<form action="javascript:void(0)" id="DetailPopupform" method="POST" enctype="multipart/form-data">
			<div class="modal-content">
			  <input type="hidden" name="beer_id" id="beer_id" />
			  <div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Beer Detail</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <div class="modal-body">
				<div class="row">
					<div class="col-md-12" >
						<div class="table-responsive">
						  <table class="table table-bordered" id="Detailtable" width="100%" cellspacing="0">
							<thead>
							  <tr>
								<!--
									<th>Brand Name</th>
									<th>Beer Name</th>
									<th>Beer Style</th>
									<th>Customer Name</th>
									<th>Phone Number</th>
									<th>Email</th>
									<th>Address</th>
									<th>Weekly keg usage </th>
									<th>Brewer's Inspiration </th>
									<th>Order Date </th>
								-->
								<th>heading</th>
								<th>Value</th>
								<th>heading</th>
								<th>Value</th>
							  </tr>
							</thead>
							<tbody id="Detailtbody">
							
							</tbody>
						  </table>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" >
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Keg Size</label>
							<select class="form-control col-sm-9" placeholder="Select Keg Size" name="keg_size_id" id="keg_size_id">
								<option value="" >Keg Size</option>
								@if(isset($filter_data['keg_sizes']) && !empty($filter_data['keg_sizes']))
									@foreach($filter_data['keg_sizes'] as $dt)
										<option value="{{$dt['id']}}">{{$dt['name']}}</option>
									@endforeach
								@endif;
							</select>
						</div>
                    </div>
					<div class="col-md-12" >
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Notes</label>
							<textarea class="form-control col-sm-9" name="order_notes" rows="5" id="order_notes"></textarea>
						</div>
                    </div>
				</div>
				
			  </div>
			  <div class="modal-footer">
				<button type="button" class="customAdd-Btn" id="saveBtn" onclick="SaveOrder()">Save</button>
				<button type="reset" class="customClose-Btn" data-dismiss="modal" >Close</button>
			  </div>
			</div>
		</form>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>

	<div class="modal fade in" id="BatchDetailPopup" data-backdrop="static">
	  <div class="modal-dialog modal-xl">
		<form action="javascript:void(0)" id="BatchDetailPopupform" method="POST" enctype="multipart/form-data">
			<div class="modal-content">
			  <div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Batch Detail</h4>
				<input type="hidden" id="singleBeerID" />
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <div class="modal-body">
				<div class="row">
					<div class="col-md-12" >
						<div class="table-responsive">
						  <table class="table table-bordered" id="BatchDetailtable" width="100%" cellspacing="0">
							<thead>
							  <tr>
								<th>Sr# </th>
								<th>Batch Name</th>
								<th>Beer Type</th>
								<th>Brewers Motivation</th>
								<th>Recipe</th>
								<th>Batch Size</th>
								<th>Action</th>
							  </tr>
							</thead>
							<tbody id="BatchDetailtbody">
							
							</tbody>
						  </table>
						</div>
					</div>
				</div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="customAdd-Btn" id="saveBtn1" onclick="attchBatch()">Assign Batch</button>
				<button type="button" class="customAdd-Btn" id="saveBtn2" onclick="CreateNewBatch()">Create Batch</button>
				<button type="reset" class="customClose-Btn" data-dismiss="modal" >Close</button>
			  </div>
			</div>
		</form>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>

	<div class="modal fade in" id="NewBatchPopup" data-backdrop="static">
	  <div class="modal-dialog modal-md">
		<form action="javascript:void(0)" id="NewBatchPopupform" method="POST" enctype="multipart/form-data">
			<div class="modal-content">
			  <div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Create Batch</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <div class="modal-body">
				<div class="row">
					@csrf
					<input type="hidden" id="beerstyleID" name="beer_style_id"/>
					<div class="form-group col-md-12">
						<input type="text" id="name" class="form-control" placeholder="Batch Name" name="name" required autofocus autocomplete="off">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<select class="form-control" placeholder="Brewer's Inspiration" id="brewers_inspiration_id" name="brewers_inspiration_id" required autofocus>
							<option value="">Brewer's Inspiration</option>
							@foreach($filter_data['brewers_inspiration'] as $dt)
								<option value="{{$dt['id']}}" >{{$dt['name']}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="row">	
					<div class="form-group col-md-12">
						<select class="form-control" placeholder="Recipe" id="reciepe_id" name="reciepe_id" required autofocus>
							<option value="">Recipe</option>
							@foreach($filter_data['reciepes'] as $dt)
								<option value="{{$dt['id']}}" >{{$dt['beer_name']}}</option>
							@endforeach
						</select>
					</div>
				</div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="customAdd-Btn" id="saveBtn3" onclick="SaveNewBatch()">Save Batch</button>
				<button type="reset" class="customClose-Btn" data-dismiss="modal" >Close</button>
			  </div>
			</div>
		</form>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>

	<div class="modal fade in" id="EditAmountPopup" data-backdrop="static">
	  <div class="modal-dialog modal-md">
		<div class="modal-content">
			<form action="javascript:void(0)" id="EditAmountform" method="POST" enctype="multipart/form-data">
			  <input type="hidden" name="beer_id" id="beer_id" />
			  <div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Edit</h4>
				<button type="button" class="close" onclick="resetAmountEditform()" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <div class="modal-body">
				<div class="row">
					<div class="col-md-12" >
						<div class="form-group" >
							<input class="form-control number_only" type="text" name="amount" id="amount" placeholder="Amount"/>
						</div>
                    </div>
				</div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="customAdd-Btn" id="saveBtn" onclick="SaveAmount()">Save</button>
				<button type="reset" class="customClose-Btn" onclick="resetAmountEditform()">Close</button>
			  </div>
			</div>
		</form>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>

<script>

var OrderTable_obj = "";
function OrderTable_Init(html = '')
{
	if(OrderTable_obj != '' && OrderTable_obj != null)
	{
		$('#table_id').dataTable().fnDestroy();
		$('#table_id tbody').empty();
		OrderTable_obj = '';
	}
	
	if(html !='')
		$('#table_id tbody').html(html);
	
	OrderTable_obj = $('#table_id').DataTable({
		searching: false, 
		info: false,
		aoColumnDefs: [
			{
				bSortable: false,
				aTargets: [ 0 ]
			}
		],
	});
}

$(document).ready( function () {
        
	$('.all-checkbox').click(function(){
		if($(this).prop("checked") == true){
			$(".single-checkbox").prop("checked",true);
		}
		else if($(this).prop("checked") == false){
			$(".single-checkbox").prop("checked",false);
		}
	});
	
	orderFilter();
});
	
function orderFilter()
{	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var type = "POST";
	var ajaxurl = '/unassignedOrderFilter';
	$.ajax({
		type: type,
		url: ajaxurl,
		dataType: 'json',
		success: function (data) {
			
			$("#beer_style_id").val('');
			$("#singleBeerID").val('');
			$(".all-checkbox").prop("checked",false);
			$(".single-checkbox").prop("checked",false);
	
			OrderTable_Init(data.html);
		},
		error: function (data) {
			console.log(data);
		}
	});		
}

function filter()
{
	if($("#beer_style_id").val() == '')
	{
		alert("Please Select Beer Style dropdown");
		return false;
	}
	
	var check = false;
	$(".formdata").each(function (item){
		if($(this).val() != "")
		{
			if(!check)
				check = true;
		}
	});
	
	if(!check)
	{
		alert("Please Select atleast one dropdown");
		return false;
	}
			
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var formData = $("#OrderFilter").serialize();		
	var type = "POST";
	var ajaxurl = '/unassignedOrderFilter';
	$.ajax({
		type: type,
		url: ajaxurl,
		data: formData,
		dataType: 'json',
		success: function (data) {
			OrderTable_Init(data.html);
			$("#createBatch-Btn").show();
		},
		error: function (data) {
			console.log(data);
			$("#createBatch-Btn").hide();
		}
	});	
}

function assignBatch()
{
	$("#singleBeerID").val('');
	
	var checkedVals = $('.single-checkbox:checkbox:checked').map(function() {
		return this.value;
	}).get();
	
	if($("#beer_style_id").val() == '')
	{
		alert("Please Select Beer Style dropdown");
		return false;
	}
	
	if(checkedVals =="")
	{
		alert("Please Select atleast one row");
		return false;
	}
		
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	

	var type = "POST";
	var ajaxurl = '/loadBatchList';
	$.ajax({
		type: type,
		url: ajaxurl,
		data: { "beerIDs" : checkedVals , "beer_style_id" : $("#beer_style_id").val() },
		dataType: 'json',
		success: function (data) {
			$("#BatchDetailtbody").html('');
			$("#BatchDetailtbody").html(data.html);
			$("#BatchDetailPopup").modal('show');
		},
		error: function (data) {
			
		}
	});	
}

function attchBatch()
{
	var singleBeerID = $("#singleBeerID").val(); 
	
	var checkedVals = $('.single-checkbox:checkbox:checked').map(function() {
		return this.value;
	}).get();
	
	var selectBatcdID = $('input[name="selectBatcdID"]:checked').val();
	if(selectBatcdID ==  undefined )
	{
		alert("Please Select Batch");
		return false;
	}
	
	if(singleBeerID != "")
		checkedVals = [singleBeerID];
		
	if(checkedVals =="")
	{
		alert("Please Select atleast one row");
		return false;
	}
		
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	

	var type = "POST";
	var ajaxurl = '/attchBatch';
	$.ajax({
		type: type,
		url: ajaxurl,
		data: { "beerIDs" : checkedVals , "BatchID" : selectBatcdID , "beer_style_id" : $("#beer_style_id").val() },
		dataType: 'json',
		success: function (data) {
			$("#BatchDetailtbody").html('');
			$("#BatchDetailPopup").modal('hide');
			orderFilter();
		},
		error: function (data) {
			
		}
	});	
}

function orderBatchFilter()
{	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var type = "POST";
	var ajaxurl = '/orderBatchFilter';
	$.ajax({
		type: type,
		url: ajaxurl,
		dataType: 'json',
		success: function (data) {
			ProcessOrderTable_Init(data.html);
		},
		error: function (data) {
			console.log(data);
		}
	});	
}

function resetFilter()
{
	$(".all-checkbox").prop("checked",false);
	$("#OrderFilter")[0].reset();
	$("#createBatch-Btn").hide();
}

function BrewThisBatch(e)
{
	var batchID = $(e).attr('data-id');
	if(batchID == '')
		return false;
		
	$("#batch_id").val(batchID);
	$("#BatchProgressPopup").modal('show');
}

function SaveOrder()
{
	if($("#beer_id").val() == '' || $("#beer_id").val() == ' ')
	{
		alert("Beer ID is missing");
		return false;
	}

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	$("#saveBtn").prop("disabled",true);
	
	var formData = $("#DetailPopupform").serialize();		
	var type = "POST";
	var ajaxurl = '/saveOrder';
	$.ajax({
		type: type,
		url: ajaxurl,
		data: formData,
		dataType: 'json',
		success: function (data) {
			if(data.status)
			{
				orderFilter();
				$("#saveBtn").attr("disabled",false);
				$("#beer_id").val('');
				$("#DetailPopup").modal('hide');
			}
		},
		error: function (data) {
			console.log(data);
		}
	});	
}

function resetBatchProgressform()
{
	TankTable_Init();
	RecipeTable_Init();
	$("#saveBtn").prop("disabled",false);
	$("#BatchProgressform")[0].reset();
	$("#batch_id").val('');
	$("#BatchProgressPopup").modal('hide');
}

function adminBrewBeer(e)
{
	return false;
	var BeerID = $(e).attr("data-id");
	if(BeerID == '')
		return false;

	var styleID = $(e).attr("data-styleID");
	if(styleID == '')
		return false;
		
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	$("#saveBtn").prop("disabled",true);
	
	var type = "POST";
	var ajaxurl = '/adminBrewBeer';
	$.ajax({
		type: type,
		url: ajaxurl,
		data: { 'BeerID' : BeerID , "styleID" : styleID },
		dataType: 'json',
		success: function (data) {		
			orderFilter();
			orderBatchFilter();
		},
		error: function (data) {
			console.log(data);
		}
	});	
}

function viewBeerDetail(e)
{
	var BeerID = $(e).attr("data-id");
	if(BeerID == '')
		return false;

	var styleID = $(e).attr("data-styleID");
	if(styleID == '')
		return false;
		
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var type = "POST";
	var ajaxurl = '/beer_details';
	$.ajax({
		type: type,
		url: ajaxurl,
		data: { 'BeerID' : BeerID , "styleID" : styleID },
		dataType: 'json',
		success: function (response) {	
			//console.log(response.data.beer_name);
			
			$("#beer_id").val('');
			$("#Detailtbody").html('');
			
			var order_date = "-";
			if(response.data.order_date != null )
				order_date = response.data.order_date
			
			//var TrHtml = '<tr><td>'+response.data.brand_name+'</td><td>'+response.data.beer_name+'</td><td>'+response.data.beer_style_name+'</td><td>'+response.data.customer_name+'</td><td>'+response.data.phone+'</td><td>'+response.data.email+'</td><td>'+response.data.address+'</td><td>'+response.data.kegs_sell_week+'</td><td>'+response.data.brewers_inspiration_name+'</td><td>'+order_date+'</td></tr>';
			
			var TrHtml = '<tr><th>Brand Name</th><td>'+response.data.brand_name+'</td><th>Beer Name</th><td>'+response.data.beer_name+'</td></tr>'+
						 '<tr><th>Beer Style</th><td>'+response.data.beer_style_name+'</td><th>Customer Name</th><td>'+response.data.customer_name+'</td></tr>'+
						 '<tr><th>Phone Number</th><td>'+response.data.phone+'</td><th>Email</th><td>'+response.data.email+'</td></tr>'+
						 "<tr><th>Address</hd><td>"+response.data.address+"</td><th>Weekly keg usage</th><td>"+response.data.kegs_sell_week+"</td></tr>"+
						 "<tr><th>Brewer's Inspiration</th><td>"+response.data.brewers_inspiration_name+"</td><th>Order Date</th><td>"+response.data.order_date+"</td></tr>";
			
			$("#Detailtbody").html(TrHtml);
			
			if(response.data.order_keg_size_id != '' && response.data.order_keg_size_id != null )
				$("#keg_size_id").val(response.data.order_keg_size_id);
			else
				$("#keg_size_id").val(response.data.keg_size_id);
				
			$("#order_notes").val(response.data.order_notes);
				
			$("#beer_id").val(response.data.beer_id)
			$("#DetailPopup").modal('show');
			
		},
		error: function (data) {
			console.log(data);
		}
	});	
}

function CreateNewBatch()
{
	$("#BatchDetailPopup").modal('hide');
	$("#NewBatchPopupform")[0].reset();
	$("#NewBatchPopup").modal('show');
}

function SaveNewBatch()
{
	var singleBeerID = $("#singleBeerID").val(); 
	
	$("#beerstyleID").val($("#beer_style_id").val());
	
	if($("#name").val() == '')
	{
		alert("Please Enter Batch Name");
		return false;
	}
	if($("#brewers_inspiration_id").val() == '')
	{
		alert("Please Select Brewer's Inspiration");
		return false;
	}
	if($("#reciepe_id").val() == '')
	{
		alert("Please Select Reciepe");
		return false;
	}
	if($("#beerstyleID").val() == '')
	{
		alert("Please Select Beer Style");
		return false;
	}
	
	var checkedVals = $('.single-checkbox:checkbox:checked').map(function() {
		return this.value;
	}).get();
	
	if(singleBeerID != "")
		checkedVals = [singleBeerID];

	if(checkedVals =="")
	{
		alert("Something went wrong.!");
		return false;
	}
	
	$("#saveBtn3").attr("disabled",true);
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});

	var type = "POST";
	var ajaxurl = '/saveNewBatch';
	$.ajax({
		type: type,
		url: ajaxurl,
		data: { "beerIDs" : checkedVals , "name" : $("#name").val(), "brewers_inspiration_id" : $("#brewers_inspiration_id").val(), "reciepe_id" : $("#reciepe_id").val(), "beer_style_id" : $("#beer_style_id").val() },
		dataType: 'json',
		success: function (data) { 
			$("#saveBtn3").attr("disabled",false);
			$("#NewBatchPopupform")[0].reset();
			$("#NewBatchPopup").modal('hide');
			orderFilter();
		},
		error: function (data) {
			
		}
	});	
	
}

function singleAssignBatch(e)
{
	$("#singleBeerID").val('');
	$(".all-checkbox").prop("checked",false);
	$(".single-checkbox").prop("checked",false);
	
	var BeerID = $(e).attr("data-id");
	if(BeerID == '')
	{
		alert("Beer ID is missing");
		return false;
	}
		
	var styleID = $(e).attr("data-styleID");
	if(styleID == '')
	{
		alert("Beer Style is missing");
		return false;
	}
		
	var checkedVals = [BeerID];
		
	$("#beer_style_id").val(styleID);
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	$("#singleBeerID").val(BeerID);

	var type = "POST";
	var ajaxurl = '/loadBatchList';
	$.ajax({
		type: type,
		url: ajaxurl,
		data: { "beerIDs" : checkedVals , "beer_style_id" : styleID },
		dataType: 'json',
		success: function (data) {
			$("#BatchDetailtbody").html('');
			$("#BatchDetailtbody").html(data.html);
			$("#BatchDetailPopup").modal('show');
		},
		error: function (data) {
			
		}
	});	
}

function editAmount(e)
{
	var beerID 	= $(e).attr('data-id');
	var amount 	= $(e).attr('data-amount');
	if(beerID == '')
		return false;
		
	$("#beer_id").val(beerID);
	$("#amount").val(amount);
	$("#EditAmountPopup").modal('show');
}

function SaveAmount()
{
	var amount = $("#amount").val();
	if(amount == '')
	{
		alert("Please Enter Amount");
		return false;
	}
		
	var beerID = $("#beer_id").val();
	if(beerID == '')
	{
		alert("Something went wrong.!");
		return false;
	}
			
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var formData = $("#EditAmountform").serialize();
	var type = "POST";
	var ajaxurl= "/SaveAmount";
	
	$.ajax({
		type: type,
		url: ajaxurl,
		data: { "beerID" : beerID , "amount" : amount },
		dataType: 'json',
		success: function (data) {
			alert("Data Successfully Updated.!");
			$("#EditAmountPopup").modal('hide');
			orderFilter();				
		},
		error: function (data) {
			console.log(data);
		}
	});
	
}

function resetAmountEditform()
{
	$("#EditAmountform")[0].reset();
	$("#beer_id").val('');
	$("#EditAmountPopup").modal('hide');
}

</script>
@endsection
