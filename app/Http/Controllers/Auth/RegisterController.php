<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Role;
use App\Message;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DB;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'roleName' => ['required'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
		if(isset($data['roleName']) && !empty($data['roleName']))
		{
			$RoleData = Role::where('slug',$data['roleName'])->select('id')->first();
			if(!empty($RoleData))
			{				
				$image_original_name  = '';
				$image_unique_name = '';
				if(isset($data['image']) && !empty($data['image']))
				{
					$fileobj				= $data['image'];
					$image_original_name 	= $fileobj->getClientOriginalName('image');
					$file_extension_name 	= $fileobj->getClientOriginalExtension('image');
					$image_unique_name 		= time().rand(1000,9999).'.'.$file_extension_name;
					$destinationPath		= public_path('/uploads/');
					$fileobj->move($destinationPath,$image_unique_name);
				}
				
				$RoleID = $RoleData->id;
				$UserData =  User::create([
					'name' => $data['fname']." ".$data['lname'],
					'fname' => $data['fname'],
					'lname' => $data['lname'],
					'email' => $data['email'],
					'password' => Hash::make($data['password']),
					'img_actual_name' => $image_original_name,
					'img_unique_name' => $image_unique_name,
				]);
				
				$UserID = $UserData->id;
				
				if(!empty($UserID) && $RoleID)
				{
					DB::table('users_roles')->insert(
						array(
								'role_id'   =>   $RoleID, 
								'user_id'   =>   $UserID
						 )
					);
				}
				
				//if($data['roleName'] == "beer_builder")
				//{
					$adminData 	= DB::table('users_roles')->where('role_id',4)->first('user_id');
					$admin_id 	= (!empty($adminData) && isset($adminData->user_id) && !empty($adminData->user_id)) ? $adminData->user_id : false;
					Message::create(
						array(
								'sender_id' => $admin_id, 
								'revicer_id' =>  $UserID,
								'message'  => "Welcome to our Beer Brand Brew",
								'flag' => 'admin'
						 )
					);
				//}
				
				return $UserData;
			}
		}
        
    }
}
