@extends('layouts.app')

@section('content')

<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    #table_id thead {
        background-color: #FF6000;
        color: white;
    }

    #table_id thead tr {
        height: 60px;
    }
    #table_id .even { background-color: white }
    #table_id .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    #table_id tbody tr {
        height: 60px;
    }
	
	.come-from-modal.left .modal-dialog,
	.come-from-modal.right .modal-dialog {
		position: fixed;
		margin: auto;
		width: 420px;
		height: 100%;
		-webkit-transform: translate3d(0%, 0, 0);
		-ms-transform: translate3d(0%, 0, 0);
		-o-transform: translate3d(0%, 0, 0);
		transform: translate3d(0%, 0, 0);
	}

	.come-from-modal.left .modal-content,
	.come-from-modal.right .modal-content {
		height: 100%;
		overflow-y: auto;
		border-radius: 0px;
	}

	.come-from-modal.left .modal-body,
	.come-from-modal.right .modal-body {
		padding: 15px 15px 80px;
	}
	.come-from-modal.right.fade .modal-dialog {
		right: -320px;
		-webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
		-moz-transition: opacity 0.3s linear, right 0.3s ease-out;
		-o-transition: opacity 0.3s linear, right 0.3s ease-out;
		transition: opacity 0.3s linear, right 0.3s ease-out;
	}

	.come-from-modal.right.fade.in .modal-dialog {
		right: 0;
	}
	.color{
		background-color: #FF6000;
		color:#FFF;
	}
	.bold{
		font-weight : bold;
	}
	.tink{
		font-weight : 300;
	}
	
	.outerImg
    {
      position: relative;
      top: 0;
      left: 0;
    }
    .innerImg
    {
      position: absolute;
      top: 70px;
	  left: 140px;
    }
	
</style>



<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Order my Beer</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
	<div class="row">
		<div class="col-md-12">
			<h6 class=" m-0 font-weight-bold text-primary">Order my Beer</h6>
		</div>
	</div>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="table_id" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Sr #</th>
            <th>Customer Name</th>
            <th>Beer Name</th>
            <th>Beer Style</th>
            <th>Quantity</th>
            <th>Keg Size</th>
            <th>Price</th>
          </tr>
        </thead>
       
        <tbody>
		<?php
			$i = 0;
			foreach($data as $dt):
			$i++;
			$imgPath 	= (isset($dt->img_unique_name) && !empty($dt->img_unique_name)) ? asset('uploads/'.$dt->img_unique_name) : false;
			$notes 		= (isset($dt->notes) && !empty($dt->notes)) ? $dt->notes : false;
			$status 	= (isset($dt->status) && !empty($dt->status)) ? $dt->status : false;
			
			$style = "";
			if(!empty($status))
				$style = "background-color:#e3e6f0";
		?>
                    <tr style="{{$style}}">
						<input type="hidden" id="imgPath_<?php echo $i;?>" value="{{$imgPath}}"/>
						<input type="hidden" id="notes_<?php echo $i;?>" value="{{$notes}}"/>
                        <td>{{$i}}</td>
                        <td id="customer_name_<?php echo $i;?>">{{$dt->customer_name}}</td>
                        <td id="beer_name_<?php echo $i;?>">{{$dt->beer_name}}</td>
                        <td id="beer_style_name_<?php echo $i;?>">{{$dt->beer_style_name}}</td>
                        <td id="keg_size_<?php echo $i;?>">{{$dt->qty}}</td>
                        <td id="keg_size_<?php echo $i;?>">{{$dt->keg_sizes_name}}</td>
                        <td id="unit_cost_<?php echo $i;?>">{{$dt->unit_cost}}</td>
                    </tr>
            <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
	
</div>

	<div class="modal fade in" id="addOrderPopup" data-backdrop="static">
	  <div class="modal-dialog modal-md">
		<div class="modal-content">
			<form action="javascript:void(0)" id="addOrderform" method="POST" enctype="multipart/form-data">
			  <input type="hidden" required id="BeerId" name="BeerId" required >
			  <div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Create Order</h4>
				<button type="button" class="close" onclick="resetOrderform()" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <div class="modal-body">
				<div class="row">
					<div class="col-md-12" >
						<div class="form-group" >
							<input type="text" class="form-control number_only" required id="qty" name="qty" autofocus="" placeholder="Quantity" >
						</div>
                    </div>
				</div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="customAdd-Btn" id="saveBtn" onclick="SaveOrder()">Create</button>
				<button type="reset" class="customClose-Btn" onclick="resetOrderform()">Close</button>
			  </div>
			</div>
		</form>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>
	
	
<script>
    $(document).ready( function () {
        $('#table_id').DataTable({
            searching: false, info: false,
            aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ -1 ]
                            }
            ]
        });
    } );
	
	function resetOrderform()
	{
		$("#BeerId").val('');
		$("#addOrderform")[0].reset();
		$("#addOrderPopup").modal('hide');
	}

	function BrewBeer(e)
	{
		var id = $(e).attr('data-beerID');
		if(id == '')
			return false;
			
		$("#BeerId").val(id);
		$("#addOrderPopup").modal('show');
	}
	
	function SaveOrder()
	{
		if($("#qty").val() == '')
		{
			alert('Please Enter Quantity');
			return false;
		}
		if($("#BeerId").val() == '')
		{
			alert('Beer ID is missing');
			return false;
		}
			
		if (confirm('If you want us to brew this beer, the next step will be to match your recipe with a brewing partner.  This is exciting!  Similar to how you order beer currently, you will place your order through the site and we will deliver your beer.  Please allow 4 weeks for the initial order to be created, after that we will continue to make this beer for you! Do you want us to begin brewing this beer? ')) {
			
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});
			
			$("#saveBtn").prop("disabled",true);
			
			var formData = $("#addOrderform").serialize();		
			var type = "POST";
			var ajaxurl = '/BrewBeer';
			$.ajax({
				type: type,
				url: ajaxurl,
				data: formData,
				dataType: 'json',
				success: function (data) {
					if(data.status)
					{
						resetOrderform();
						location.reload();
					}
				},
				error: function (data) {
					console.log(data);
				}
			});	
		}
	}

</script>
@endsection
