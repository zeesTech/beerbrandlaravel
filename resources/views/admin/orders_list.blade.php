@extends('layouts.app')

@section('content')

 <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Optional JavaScript -->

<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    .table thead {
        background-color: #FF6000;
        color: white;
    }

    .table thead tr {
        height: 60px;
    }
    .table .even { background-color: white }
    .table .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    .table tbody tr {
        height: 60px;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
	<div class="customContainer">
                
		<div class="row" >
			<h2>Orders</h2>
		</div>
		
		
		<form action="javascript:void(0)" method="post" id="OrderFilter">
			<div class="row" >
				<div class="col-md-2" >
					<div class="form-group" >
						<select class="form-control formdata" placeholder="Beer Style" name="beer_style_id" id="beer_style_id">
							<option value="" >Beer Style</option>
							@foreach($filter_data['beer_styles'] as $dt)
								<option value="{{$dt['id']}}">{{$dt['name']}}</option>
							@endforeach
						</select>
                    </div>
				</div>
				<div class="col-md-2" >
					<div class="form-group" >
						<select class="form-control formdata" placeholder="Beer Style" name="abv_id" >
							<option value="" >ABV %</option>
							@foreach($filter_data['abv'] as $dt)
								<option value="{{$dt['id']}}">{{$dt['name']}}</option>
							@endforeach
						</select>
                    </div>
				</div>
				<div class="col-md-2" >
					<div class="form-group" >
						<select class="form-control formdata" placeholder="Beer Style" name="ibu_id" >
							<option value="" >IBU %</option>
							@foreach($filter_data['ibu'] as $dt)
								<option value="{{$dt['id']}}">{{$dt['name']}}</option>
							@endforeach
						</select>
                    </div>
				</div>
				<div class="col-md-2" >
					<div class="form-group" >
						<select class="form-control formdata" placeholder="Beer Style" name="keg_size_id" >
							<option value="" >Keg Size</option>
							@foreach($filter_data['keg_sizes'] as $dt)
								<option value="{{$dt['id']}}">{{$dt['name']}}</option>
							@endforeach
						</select>
                    </div>
				</div>
				<div class="col-md-2" >
					<div class="form-group" >
						<button class="customAdd-Btn" type="submit" onclick="filter()">Filter</button>
                    </div>
				</div>
				<div class="col-md-2" >
					<div class="form-group" >
						<button class="customAdd-Btn" style="display:none;" id="createBatch-Btn" type="button" onclick="createBatch()">Create Batch</button>
                    </div>
				</div>
			</div>
		</form>
		
			
		<div class="row tabledesign">
			<table class="table table-bordered" id="table_id" class="display" style="width:100%">
				<thead>
					<tr>
						<th>Sr# <input type="checkbox" class="all-checkbox"></th>
						<th>Name</th>
						<th>Beer Type</th>
						<th>Brewers Motivation</th>
						<th>ABV</th>
						<th>IBU</th>
						<th>Keg Size</th>
						<th>Weekly Usage</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="OrderTbody">
					<?php
			$i = 0;
			if(isset($data))
			{
				foreach($data as $dt):
				$i++;
				
		?>
                    <tr>
						<td>{{$i}}</td>
                        <td>{{$dt->beer_name}}</td>
                        <td>{{$dt->beer_style_name}}</td>
                        <td>{{$dt->brewers_inspiration_name}}</td>
						<td>{{$dt->abv_name}}</td>
                        <td>{{$dt->ibu_name}}</td>
						<td>{{$dt->keg_sizes_name}}</td>
                        <td>{{$dt->kegs_sell_week}}</td>
                        <td>
							<div class="dropdown no-arrow">
								<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink<?php echo $i;?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink<?php echo $i;?>">
								  <a class="dropdown-item" href="javascript:void(0)" >Brew This Beer</a> <!-- href="{{ route('admin_brew_beer',['id' => $dt->beer_id]) }}" -->
								  <a class="dropdown-item" href="javascript:void(0)">Send Back To Order</a>
								  <a class="dropdown-item" href="javascript:void(0)">Delete This Order</a>
								</div>
							</div>
						</td>
                    </tr>
            <?php endforeach; 
			}
		?>
				</tbody>
			</table>
		</div>
    </div>

	<div class="customContainer">
                
		<div class="row" >
			<h2>Batch Progress</h2>
		</div>

		<div class="row tabledesign">
			<table class="table table-bordered" id="processOrderTbl" class="display" style="width:100%">
				<thead>
					<tr>
						<th>Batch ID</th>
						<th>Beer Type</th>
						<!--<th>ABV</th>
						<th>IBU</th>
						<th>Description</th>-->
						<th>Batch Size</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				<?php
					if(isset($progressOrder)){
					foreach($progressOrder as $dt):
					$i++;
						
				?>
						<tr>
							<td>{{$dt->beer_name}}</td>
							<td>{{$dt->beer_style_name}}</td>
							<!--<td>{{$dt->abv_name}}</td>
							<td>{{$dt->ibu_name}}</td>
							<td>{{$dt->notes}}</td>-->
							<td></td>
							<td>
								<div class="dropdown no-arrow">
									<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink<?php echo $i;?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink<?php echo $i;?>">
									  <a class="dropdown-item" href="javascript:void(0)">Brew This Beer</a>
									  <a class="dropdown-item" href="javascript:void(0)">Send Back To Order</a>
									  <a class="dropdown-item" href="javascript:void(0)">Delete This Order</a>
									</div>
								</div>
							</td>
						</tr>
			<?php endforeach; } ?>
				</tbody>
			</table>
		</div>
    </div>

</div>

	<div class="modal fade in" id="BatchProgressPopup" data-backdrop="static">
	  <div class="modal-dialog modal-xl">
		<div class="modal-content">
			<form action="javascript:void(0)" id="BatchProgressform" method="POST" enctype="multipart/form-data">
			  <input type="hidden" name="batch_id" id="batch_id" />
			  <div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Batch Progress Request</h4>
				<button type="button" class="close" onclick="resetBatchProgressform()" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <div class="modal-body">
				<div class="row">
					<div class="col-md-12" >
						<div class="form-group" >
							<select class="form-control" placeholder="Select Brewer Partner" name="brewer_partner_id" id="brewer_partner_id" onchange="ChoosePartner(this)">
								<option value="" >Select Brewer Partner</option>
								@if(isset($partnerData) && !empty($partnerData))
									@foreach($partnerData as $dt)
										<option value="{{$dt['id']}}">{{$dt['name']}}</option>
									@endforeach
								@endif;
							</select>
						</div>
                    </div>
				</div>
				<div class="row">
					<div class="col-md-12" >
						<div class="table-responsive">
						  <table class="table table-bordered" id="Tanktable" width="100%" cellspacing="0">
							<thead>
							  <tr>
								<th>Brewery Name</th>
								<th>Brewer Name</th>
								<th>Tank Size (BBL)</th>
								<th>Personal Use (BBL) </th>
								<th>Customer Use (BBL)</th>
								<th>Usage</th>
								<th>Available Date</th>
							  </tr>
							</thead>
							<tbody id="Tanktbody">
							
							</tbody>
						  </table>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-12" >
						<div class="form-group" >
							<select class="form-control" placeholder="Select Recipe" name="recipe_id" id="recipe_id" onchange="ChooseRecipe(this)">
								<option value="" >Select Recipe</option>
								@if(isset($reciepeData) && !empty($reciepeData))
									@foreach($reciepeData as $dt)
										<option data-type="{{$dt['type']}}" data-abv="{{$dt['abv_name']}}" data-ibu="{{$dt['ibu_name']}}" value="{{$dt['id']}}">{{$dt['beer_name']}}</option>
									@endforeach
								@endif
							</select>
						</div>
                    </div>
				</div>
				<div class="row">
					<div class="col-md-12" >
						<div class="table-responsive">
						  <table class="table table-bordered" id="RecipeTable" width="100%" cellspacing="0">
							<thead>
							  <tr>
								<th>Name</th>
								<th>Type</th>
								<th>ABV</th>
								<th>IBU</th>
							  </tr>
							</thead>
							<tbody id="Tanktbody">
							
							</tbody>
						  </table>
						</div>
					</div>
				</div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="customAdd-Btn" id="saveBtn" onclick="CreateOrder()">Save</button>
				<button type="reset" class="customClose-Btn" onclick="resetBatchProgressform()">Close</button>
			  </div>
			</div>
		</form>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>

<script>

var OrderTable_obj = "";
function OrderTable_Init(html = '')
{
	if(OrderTable_obj != '' && OrderTable_obj != null)
	{
		$('#table_id').dataTable().fnDestroy();
		$('#table_id tbody').empty();
		OrderTable_obj = '';
	}
	
	if(html !='')
		$('#table_id tbody').html(html);
	
	OrderTable_obj = $('#table_id').DataTable({
		searching: false, 
		info: false,
		aoColumnDefs: [
			{
				bSortable: false,
				aTargets: [ 0 ]
			}
		],
	});
}

var ProcessOrderTable_obj = "";
function ProcessOrderTable_Init(html = '')
{
	if(ProcessOrderTable_obj != '' && ProcessOrderTable_obj != null)
	{
		$('#processOrderTbl').dataTable().fnDestroy();
		$('#processOrderTbl tbody').empty();
		ProcessOrderTable_obj = '';
	}
	
	if(html !='')
		$('#processOrderTbl tbody').html(html);
	
	ProcessOrderTable_obj = $('#processOrderTbl').DataTable({
		searching: false, 
		info: false,
		aoColumnDefs: [
			{
				bSortable: false,
				aTargets: [ 0 ]
			}
		],
	});
}
 
var TankTable_obj = "";
function TankTable_Init(html = '')
{
	if(TankTable_obj != '' && TankTable_obj != null)
	{
		$('#Tanktable').dataTable().fnDestroy();
		$('#Tanktable tbody').empty();
		TankTable_obj = '';
	}
	
	if(html !='')
		$('#Tanktable tbody').html(html);
	
	TankTable_obj = $('#Tanktable').DataTable({
		searching: false, 
		info: false,
		aoColumnDefs: [
			{
				bSortable: false,
				aTargets: [ 0 ]
			}
		],
	});
}

var RecipeTable_obj = "";
function RecipeTable_Init(html = '')
{
	if(RecipeTable_obj != '' && RecipeTable_obj != null)
	{
		$('#RecipeTable').dataTable().fnDestroy();
		$('#RecipeTable tbody').empty();
		RecipeTable_obj = '';
	}
	
	if(html !='')
		$('#RecipeTable tbody').html(html);
	
	RecipeTable_obj = $('#RecipeTable').DataTable({
		searching: false, 
		info: false,
		aoColumnDefs: [
			{
				bSortable: false,
				aTargets: [ 0 ]
			}
		],
	});
}

$(document).ready( function () {
        
	$('.all-checkbox').click(function(){
		if($(this).prop("checked") == true){
			$(".single-checkbox").prop("checked",true);
		}
		else if($(this).prop("checked") == false){
			$(".single-checkbox").prop("checked",false);
		}
	});
	
	orderFilter();
	orderBatchFilter();
	TankTable_Init();
	RecipeTable_Init();
});
	
function orderFilter()
{	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var type = "POST";
	var ajaxurl = '/orderFilter';
	$.ajax({
		type: type,
		url: ajaxurl,
		dataType: 'json',
		success: function (data) {
			OrderTable_Init(data.html);
		},
		error: function (data) {
			console.log(data);
		}
	});		
}

function filter()
{
	if($("#beer_style_id").val() == '')
	{
		alert("Please Select Beer Style dropdown");
		return false;
	}
	
	var check = false;
	$(".formdata").each(function (item){
		if($(this).val() != "")
		{
			if(!check)
				check = true;
		}
	});
	
	if(!check)
	{
		alert("Please Select atleast one dropdown");
		return false;
	}
			
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var formData = $("#OrderFilter").serialize();		
	var type = "POST";
	var ajaxurl = '/orderFilter';
	$.ajax({
		type: type,
		url: ajaxurl,
		data: formData,
		dataType: 'json',
		success: function (data) {
			OrderTable_Init(data.html);
			$("#createBatch-Btn").show();
		},
		error: function (data) {
			console.log(data);
			$("#createBatch-Btn").hide();
		}
	});	
}

function createBatch()
{
	var checkedVals = $('.single-checkbox:checkbox:checked').map(function() {
		return this.value;
	}).get();
	
	if(checkedVals =="")
	{
		alert("Please Select atleast one row");
		return false;
	}
		
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var type = "POST";
	var ajaxurl = '/createBatch';
	$.ajax({
		type: type,
		url: ajaxurl,
		data: { "beerIDs" : checkedVals , "beer_style_id" : $("#beer_style_id").val() },
		dataType: 'json',
		success: function (data) {
			resetFilter();
			orderFilter();
			orderBatchFilter();
		},
		error: function (data) {
			
		}
	});	
}

function orderBatchFilter()
{	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var type = "POST";
	var ajaxurl = '/orderBatchFilter';
	$.ajax({
		type: type,
		url: ajaxurl,
		dataType: 'json',
		success: function (data) {
			ProcessOrderTable_Init(data.html);
		},
		error: function (data) {
			console.log(data);
		}
	});	
}

function resetFilter()
{
	$(".all-checkbox").prop("checked",false);
	$("#OrderFilter")[0].reset();
	$("#createBatch-Btn").hide();
}

function BrewThisBatch(e)
{
	var batchID = $(e).attr('data-id');
	if(batchID == '')
		return false;
		
	$("#batch_id").val(batchID);
	$("#BatchProgressPopup").modal('show');
}

function ChoosePartner(e)
{
	var PartnerID = $(e).val();
	if(PartnerID == '')
		return false;
		
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var type = "POST";
	var ajaxurl = '/TankListByPartnerID';
	$.ajax({
		type: type,
		url: ajaxurl,
		data : { "PartnerID" : PartnerID },
		dataType: 'json',
		success: function (data) {
			TankTable_Init(data.html);
		},
		error: function (data) {
			console.log(data);
		}
	});
}

function ChooseRecipe(e)
{
	var RecipeID = $(e).val();
	if(RecipeID == '')
		return false;
		
	var name 	= $('option:selected',e).text();
	var type 	= $('option:selected',e).attr('data-type');
	var abv 	= $('option:selected',e).attr('data-abv');
	var ibu 	= $('option:selected',e).attr('data-ibu');
	var html 	= '<tr><td>'+name+'</td><td>'+type+'</td><td>'+abv+'</td><td>'+ibu+'</td></tr>';
	RecipeTable_Init(html);

}

function CreateOrder()
{
	if($("#batch_id").val() == '' || $("#batch_id").val() == ' ')
	{
		alert("Batch ID is missing");
		return false;
	}
	if($("#brewer_partner_id").val() == '' || $("#brewer_partner_id").val() == ' ')
	{
		alert("Please Select Partner");
		return false;
	}
	if($("#recipe_id").val() == '')
	{
		alert("Please Select Recipe");
		return false;
	}
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	$("#saveBtn").prop("disabled",true);
	
	var formData = $("#BatchProgressform").serialize();		
	var type = "POST";
	var ajaxurl = '/createOrder';
	$.ajax({
		type: type,
		url: ajaxurl,
		data: formData,
		dataType: 'json',
		success: function (data) {
			
			orderBatchFilter();
			resetBatchProgressform();
		},
		error: function (data) {
			console.log(data);
		}
	});	
}

function resetBatchProgressform()
{
	TankTable_Init();
	RecipeTable_Init();
	$("#saveBtn").prop("disabled",false);
	$("#BatchProgressform")[0].reset();
	$("#batch_id").val('');
	$("#BatchProgressPopup").modal('hide');
}

function adminBrewBeer(e)
{
	var BeerID = $(e).attr("data-id");
	if(BeerID == '')
		return false;

	var styleID = $(e).attr("data-styleID");
	if(styleID == '')
		return false;
		
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	$("#saveBtn").prop("disabled",true);
	
	var type = "POST";
	var ajaxurl = '/adminBrewBeer';
	$.ajax({
		type: type,
		url: ajaxurl,
		data: { 'BeerID' : BeerID , "styleID" : styleID },
		dataType: 'json',
		success: function (data) {		
			orderFilter();
			orderBatchFilter();
		},
		error: function (data) {
			console.log(data);
		}
	});	
}

</script>
@endsection
