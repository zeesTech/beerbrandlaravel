<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification_type extends Model
{
	 protected $table = "notification_type";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'flag', 'default_msg','isStatus'
    ];
}
