<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReciepesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reciepes', function (Blueprint $table) {
            $table->id();
			$table->unsignedInteger('user_id');
			$table->string('beer_name',50)->nullable();
            $table->Integer('beer_style_id');
            $table->Integer('abv_id');
            $table->Integer('ibu_id');
			$table->string('similar_beer',100)->nullable();
			$table->string('awards',100)->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reciepes');
    }
}
