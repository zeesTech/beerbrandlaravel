<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('BrewingPartners', function () {
    return view('brewing_partners');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => 'role:beer_builder'], function() {

	Route::get('/build_brand','BrandsController@create')->name('brand.create');
	Route::post('/store_brand','BrandsController@store')->name('brand.store');
	Route::get('/my_brands','BrandsController@index')->name('brand.index');
	Route::get('/delete_brand/{id}', 'BrandsController@deleteBrand')->name('brand.delete_brand');
	Route::get('/orders','BrandsController@Orders')->name('orders');
	Route::post('/BrandDelete', 'BrandsController@BrandDelete')->name('BrandDelete');
	
	Route::get('/beers', 'BeersController@index')->name('beer.index');
	Route::get('/build_beer','BeersController@create')->name('beer.create');
	Route::post('/store_beer','BeersController@store')->name('beer.store');
	Route::post('/show_beer','BeersController@show')->name('beer.show');
	Route::get('/meet_beer/{id}', 'BeersController@meetBeer')->name('meet_beer');
	Route::get('/delete_beer/{id}', 'BeersController@deleteBeer')->name('beer.delete_beer');
	Route::get('/brew_beer/{id}', 'BeersController@Brew_Beer')->name('beer.brew_beer');
	Route::get('/edit_beer/{id}','BeersController@EditBeer')->name('edit_beer');
	Route::post('/update_beer','BeersController@UpdateBeer')->name('update_beer');
	Route::post('/BeerDelete','BeersController@BeerDelete')->name('BeerDelete');
	Route::get('/stop_brew_beer/{id}', 'BeersController@stopBrewBeer')->name('beer.stop_brew_beer');
	Route::get('/beerOrder','BeersController@beerOrder')->name('beerOrder');
	Route::post('/BeerUpdateStatus','BeersController@BeerUpdateStatus')->name('BeerUpdateStatus');
	Route::post('/BrewBeer', 'BeersController@BrewBeer')->name('beer.BrewBeer');
	
	Route::get('/message','BeersController@Messages')->name('message');
	Route::post('/saveNewMessage','BeersController@saveMessage')->name('saveNewMessage');
	Route::post('/loadChat','BeersController@loadChat')->name('loadChat');
	Route::post('/loadChatContact','BeersController@loadContact')->name('loadChatContact');
	
});

Route::group(['middleware' => 'role:brewer_partner'], function() {

	//Route::get('/beer_manage','BrewerpartnersController@index')->name('beer_manage');
	Route::get('/my_revenue','BrewerpartnersController@MyRevenue')->name('my_revenue');
	Route::get('/add_tank','BrewerpartnersController@AddTank')->name('add_tank');
	Route::post('/create_tank','BrewerpartnersController@CreateTank')->name('create_tank');
	Route::get('/list_tank','BrewerpartnersController@ListTank')->name('list_tank');
	Route::get('/edit_tank/{id}','BrewerpartnersController@EditTank')->name('edit_tank');
	Route::post('/update_tank','BrewerpartnersController@UpdateTank')->name('update_tank');
	Route::get('/delete_tank/{id}', 'BrewerpartnersController@deleteTank')->name('delete_tank');
	Route::get('/order','BrewerpartnersController@Order')->name('order');
	Route::post('/changeStatusOfBatch','BrewerpartnersController@changeStatusOfBatch')->name('changeStatusOfBatch');
	Route::post('/update_brewery_profile','BrewerpartnersController@UpdateBreweryProfile')->name('update_brewery_profile');
	Route::get('/viewrecipe/{id}', 'BrewerpartnersController@ViewReciepe')->name('viewrecipe');
	
	Route::get('/chat','BrewerpartnersController@Messages')->name('chat');
	Route::post('/saveMessageNew','BrewerpartnersController@saveMessage')->name('saveMessageNew');
	Route::post('/loadChats','BrewerpartnersController@loadChat')->name('loadChats');
	
	Route::get('/offers','BrewerpartnersController@offers')->name('offers');
	Route::post('/AcceptOffer','BrewerpartnersController@AcceptOffer')->name('AcceptOffer');	
});

Route::group(['middleware' => 'role:home_brewer_partner'], function() {
	Route::get('/add_recipe','HomebrewerpartnersController@AddReciepe')->name('add_recipe');
	Route::post('/create_recipe','HomebrewerpartnersController@CreateReciepe')->name('create_recipe');
	Route::get('/list_recipe','HomebrewerpartnersController@ListReciepe')->name('list_recipe');
	Route::get('/revenue','HomebrewerpartnersController@MyRevenue')->name('revenue');
	Route::get('/view_recipe/{id}', 'HomebrewerpartnersController@ViewReciepe')->name('view_recipe');
});

Route::group(['middleware' => 'role:administrator'], function() {
	
	Route::get('/orders_list','AdminController@OrdersList')->name('orders_list');
	
	Route::get('/customer_list','AdminController@CustomerList')->name('customer_list');
	Route::get('/total_revenue','AdminController@TotalRevenue')->name('total_revenue');
	//Route::get('/brewery_partners','AdminController@BreweryPartners')->name('brewery_partners');
	Route::get('/partner_list','AdminController@PartnersList')->name('partner_list');
	
	Route::get('/recipe_add','AdminController@AddReciepe')->name('recipe_add');
	Route::post('/recipe_create','AdminController@CreateReciepe')->name('recipe_create');
	Route::get('/recipes_list','AdminController@ReciepesList')->name('recipes_list');
	Route::get('/recipe_view/{id}', 'AdminController@ReciepeView')->name('recipe_view');
	
	Route::get('/beer_matching','AdminController@BeerMatching')->name('beer_matching');
	Route::get('/home_brewer_submission','AdminController@HomeBrewerSubmission')->name('home_brewer_submission');
	Route::get('/admin_brew_beer/{id}', 'AdminController@BrewBeer')->name('admin_brew_beer');
	Route::post('/adminBrewBeer/', 'AdminController@adminBrewBeer')->name('adminBrewBeer');
	Route::post('/beer_details/', 'AdminController@beer_details')->name('beer_details');
	
	Route::post('/orderFilter','AdminController@orderFilter')->name('orderFilter');
	Route::post('/createBatch','AdminController@createBatch')->name('createBatch');
	Route::post('/orderBatchFilter','AdminController@orderBatchFilter')->name('orderBatchFilter');
	Route::post('/TankListByPartnerID','AdminController@TankListByPartnerID')->name('TankListByPartnerID');
	Route::post('/TankListByPartnerIDs','AdminController@TankListByPartnerIDs')->name('TankListByPartnerIDs');
	Route::post('/createOrder','AdminController@createOrder')->name('createOrder');
	
	Route::get('/orders_unassigned','AdminController@OrdersUnassigned')->name('orders_unassigned');
	Route::post('/unassignedOrderFilter','AdminController@unassignedOrderFilter')->name('unassignedOrderFilter');
	Route::post('/saveOrder','AdminController@saveOrder')->name('saveOrder');
	Route::post('/loadBatchList','AdminController@loadBatchList')->name('loadBatchList');
	Route::post('/attchBatch','AdminController@attchBatch')->name('attchBatch');
	Route::post('/saveNewBatch','AdminController@saveNewBatch')->name('saveNewBatch');
	
	Route::get('/order_being_built','AdminController@OrderBeingBuilt')->name('order_being_built');
	Route::post('/orderBatchesBeingBuiltFilter','AdminController@orderBatchesBeingBuiltFilter')->name('orderBatchesBeingBuiltFilter');
	Route::get('/CreateOrderBatch','AdminController@CreateOrderBatch')->name('CreateOrderBatch');
	Route::get('/EditOrderBatch','AdminController@CreateOrderBatch')->name('EditOrderBatch');
	Route::post('/SaveOrderBatch','AdminController@SaveOrderBatch')->name('SaveOrderBatch');
	Route::get('/DeleteSaveOrderBatch','AdminController@DeleteSaveOrderBatch')->name('DeleteSaveOrderBatch');
	Route::post('/AssignToBrewer','AdminController@AssignToBrewer')->name('AssignToBrewer');
	Route::post('/SendOffer','AdminController@SendOffer')->name('SendOffer');
	
	Route::get('/order_batches_brewing','AdminController@OrderBatchesBrewing')->name('order_batches_brewing');
	Route::post('/orderBatchesBrewingFilter','AdminController@orderBatchesBrewingFilter')->name('orderBatchesBrewingFilter');
	Route::post('/loadOrderBeerList','AdminController@loadOrderBeerList')->name('loadOrderBeerList');
	Route::post('/setStatusOfBatch','AdminController@setStatusOfBatch')->name('setStatusOfBatch');
	
	Route::get('/order_completed_batches','AdminController@OrderCompletedBatches')->name('order_completed_batches');
	Route::post('/orderCompletedBatchesFilter','AdminController@orderCompletedBatchesFilter')->name('orderCompletedBatchesFilter');
	Route::post('/batchBackToBrewing','AdminController@batchBackToBrewing')->name('batchBackToBrewing');
	
	Route::get('/SaveBeerCost','AdminController@SaveBeerCost')->name('SaveBeerCost');
	Route::get('/MapBeerCost','AdminController@MapBeerCost')->name('MapBeerCost');

	Route::post('/saveNotes','AdminController@saveNotes')->name('saveNotes');
	Route::post('/SaveAmount','AdminController@SaveAmount')->name('SaveAmount');
	
	Route::get('/messages','AdminController@Messages')->name('messages');
	Route::post('/saveMessage','AdminController@saveMessage')->name('saveMessage');
	Route::post('/loadMessage','AdminController@loadMessage')->name('loadMessage');
	Route::post('/loadContact','AdminController@loadContact')->name('loadContact');
	
	Route::get('/beersList','AdminController@beersList')->name('beersList');
	Route::post('/BeerStatusUpdate','AdminController@BeerStatusUpdate')->name('BeerStatusUpdate');
	
	
	Route::get('/ListofBeers', 'AdminController@ListofBeers')->name('ListofBeers');
	Route::get('/buildbeer','AdminController@buildbeer')->name('buildbeer');
	Route::post('/beerStore','AdminController@beerStore')->name('beerStore');
	Route::get('/meetBeer/{id}', 'AdminController@meetBeer')->name('meetBeer');
	Route::get('/editbeer/{id}','AdminController@EditBeer')->name('editbeer');
	Route::post('/updateBeer','AdminController@UpdateBeer')->name('updateBeer');
	Route::post('/deleteBeer', 'AdminController@BeerDelete')->name('deleteBeer');
	Route::post('/UpdateBeerStatus','AdminController@UpdateBeerStatus')->name('UpdateBeerStatus');
	Route::get('/Orderbeer','AdminController@Orderbeer')->name('Orderbeer');
	Route::post('/BeerBrew', 'AdminController@BeerBrew')->name('BeerBrew');
	Route::get('/stopBeerBrew/{id}', 'AdminController@stopBeerBrew')->name('stopBeerBrew');
	
	Route::get('/listbrand','AdminController@listbrand')->name('listbrand');
	Route::get('/buildbrand','AdminController@buildbrand')->name('buildbrand');
	Route::post('/storebrand','AdminController@storebrand')->name('storebrand');
	Route::post('/DeleteBrand', 'AdminController@DeleteBrand')->name('DeleteBrand');
	
	
});

Route::post('/getNotification','NotificationController@getNotification')->name('getNotification');
Route::post('/markRead','NotificationController@markRead')->name('markRead');

Route::get('/profile', function () {
    return view('beer_builder/profile');
});

Route::post('/update_profile','HomeController@updateProfile')->name('update_profile');

Route::get('/dashboard','HomeController@Dashboard')->name('dashboard');