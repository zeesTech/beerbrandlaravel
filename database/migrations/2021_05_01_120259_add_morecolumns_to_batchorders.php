<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMorecolumnsToBatchorders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('batchorders', function (Blueprint $table) {
            $table->float('keg_1_2_size_beer_cost', 11, 2)->nullable()->after('reciepe_id');
            $table->float('keg_1_6_size_beer_cost', 11, 2)->nullable()->after('keg_1_2_size_beer_cost');
            $table->float('total_cost', 11, 2)->nullable()->after('keg_1_6_size_beer_cost');
			$table->date('brew_date')->nullable()->after('total_cost');
			$table->date('pickup_date')->nullable()->after('brew_date');
			$table->date('completed_date')->nullable()->after('pickup_date');
			$table->date('order_date')->nullable()->after('completed_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('batchorders', function (Blueprint $table) {
            //
        });
    }
}
