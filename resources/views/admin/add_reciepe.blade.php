@extends('layouts.app')

@section('content')
<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    #table_id thead {
        background-color: #FF6000;
        color: white;
    }

    #table_id thead tr {
        height: 60px;
    }
    #table_id .even { background-color: white }
    #table_id .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    #table_id tbody tr {
        height: 60px;
    }
	#buttonStyle{
	  padding: 10px;
	  color: white;
	  background-color: #FF6000;
	  border: 1px solid #FF6000;
	  border-radius: 5px;
	  cursor: pointer;
	}

	#buttonStyle:hover {
	  background-color: #FF6000;
	  border: 1px solid #FF6000;
	}

	#errormsg {
	  margin-left: 10px;
	  font-family: sans-serif;
	  color: #aaa;
	}
</style>
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Beer Uploader</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Beer Uploader</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive" >
		<form action="{{ route('recipe_create') }}" method="post" enctype="multipart/form-data">
		  @csrf
		  
			<div class="form-group">
				<input type="text" id="beer_name" name="beer_name" class="form-control" placeholder="Beer Name" required autofocus>
			</div>

			<div class="form-group" style="border: 1px solid #d1d3e2;border-radius: 0.35rem;">
				<input type="file" id="image"  onchange="selectFile();" name = "image" accept="image/*" hidden="hidden" onchange="allowonlyImg(this)" />
				<button type="button" id="buttonStyle" onclick="FileUpload();">Upload Brewer Photo</button>
				<span id="errormsg">No file chosen, yet.</span>
			</div>
					
			<div class="form-group" style="display: inline-flex;width: 100%;">
				<select class="form-control col-md-4" placeholder="Beer Style" name="beer_style_id" required autofocus >
					<option>Beer Style</option>
					@if(isset($data['beer_styles']) && !empty($data['beer_styles']))
						@foreach($data['beer_styles'] as $dt)
							<option value="{{$dt['id']}}">{{$dt['name']}}</option>
						@endforeach
					@endif
				</select>
            
				<select class="form-control col-md-4" placeholder="ABV %" required name="abv_id" autofocus >
					<option>ABV %</option>
					@if(isset($data['abv']) && !empty($data['abv']))
						@foreach($data['abv'] as $dt)
							<option value="{{$dt['id']}}">{{$dt['name']}}</option>
						@endforeach
					@endif
				</select>
				
				<select class="form-control col-md-4" placeholder="IBU’S" required name="ibu_id" autofocus >
                    <option>IBU'S</option>
					@if(isset($data['ibu']) && !empty($data['ibu']))
						@foreach($data['ibu'] as $dt)
							<option value="{{$dt['id']}}">{{$dt['name']}}</option>
						@endforeach
					@endif
                </select>
				
			</div>
			
			<!--<div class="form-group">
				<input type="text" id="similar_beer" name="similar_beer" class="form-control" placeholder="Similar Beer" required autofocus>
			</div>-->
			
			<div class="form-group">
				<textarea id="awards" name="awards" class="form-control" rows="5" placeholder="Tell Us About Your Beer" required autofocus></textarea>
			</div>
			
			<div class="form-group">
				<textarea id="notes" name="notes" class="form-control" rows="5" placeholder="Tasting Notes" required autofocus></textarea>
			</div>

			<button class="btn btn-lg btn-bbb-notActive btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">SUBMIT</button>
		  </form>
  </div>
</div>

</div>

<script>
    $(document).ready( function () {
        $('#table_id').DataTable({
            searching: false, info: false,
            aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ -1 ]
                            }
            ]
        });
    } );
</script>

<script>
function FileUpload()
{
	document.getElementById('image').click();	
}

function selectFile()
{
	 if (document.getElementById('image').value) {
		 document.getElementById('errormsg').innerHTML = document.getElementById('image').value.match(
		      /[\/\\]([\w\d\s\.\-\(\)]+)$/
		    )[1];
		  } else {
			  document.getElementById('errormsg').innerHTML = "No file chosen, yet.";
		  }
}
</script>

@endsection
