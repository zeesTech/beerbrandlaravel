<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beers extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','beer_name', 'beer_style_id', 'brewers_inspiration_id','abv_id','malt_style_id','ibu_id', 'hop_id' , 'keg_size_id', 'beer_color_id', 
		'beer_color','kegs_sell_week_in_covid','kegs_sell_week','interested_canning_beer','package_type','unit_cost','total_cost','like_beer','brand_id',
		'status','notes','order_keg_size_id','order_notes','order_date','qty','beer_size','beer_size_rate','beer_status'
    ];
}
