@extends('layouts.app')

@section('content')

 <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Optional JavaScript -->

<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    #table_id thead {
        background-color: #FF6000;
        color: white;
    }

    #table_id thead tr {
        height: 60px;
    }
    #table_id .even { background-color: white }
    #table_id .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    #table_id tbody tr {
        height: 60px;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
	<div class="customContainer">
                
		<div class="row" >
			
			<div class="col-md-12 pl-0 pr-0">
			 <div class="my-revenue-container">
			  <div class="row">
				 
			    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
				<div class="Revenue-Head">
				 <h1>$0</h1>
				 <p>Total Earnings</p>
				</div>
			   </div>
			   
			   <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
				<div class="Revenue-Head">
				 <h1>0</h1>
				 <p>Total Orders</p>
				</div>
			   </div>
				  
			   <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
				<div class="Revenue-Head">
				 <h1>$0</h1>
				 <p>Pending Amount</p>
				</div>
			   </div>
				  
			   <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
				<div class="Revenue-Head">
				 <h1>0</h1>
				 <p>Pending Orders</p>
				</div>
			   </div>
				  
			  </div>
			 </div>
			</div>
			
		</div>

		<div class="row tabledesign">
			<table class="table table-bordered" id="table_id" class="display" style="width:100%">
				<thead>
					<tr>
						<th>Batch ID</th>
						<th>Keg Quantity (1/2 BBL)</th>
						<th>½ BBL Revenue</th>
						<th>Keg Quantity (1/6 BBL)</th>
						<th>1/6 BBL Revenue</th>
						<th>Total Beer Revenue</th>
						<th>Beer Style & Recipe</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
    </div>
</div>

<script>
    $(document).ready( function () {
        $('#table_id').DataTable({
            searching: false, info: false,
            aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ -1 ]
                            }
            ]
        });
    } );
</script>
@endsection
