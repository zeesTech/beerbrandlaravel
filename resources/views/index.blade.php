<html>
    <head>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>  
     
    <style>
        .tiles {
            background-color: #FF6000;
            margin: 0px 5px;
            width: 30%;
            color: white;
            font-weight: 600;
            border-radius: 7px;
            padding: 35px 0px 20px 35px;
        }
        .tiles > img {
            width: 17%;
        }

        .tiles > p {
            font-weight: 700;
            font-size: 25px; 
        }
        .t1 {
            float: left;
        }

        .t2 {
            margin: 0 auto;
        }

        .t3 {
            float: right;
        }
        .verticle-center {
            width: 90%;
            top: 50%;
    left: 50%;position: absolute;transform: translate(-50%, -50%);
        }
    </style>
    </head>
    <body>
        <div class="container">
            <div class="verticle-center">
                <div class="row">
                    <img src="assets/Brewery Partners/Logo.png" style="
                        display: block;
                        margin-left: auto;
                        margin-right: auto;    width: 17%;
                    ">
                    <div class="col-md-12">
                        <p style="text-align: center;font-size: 50;font-style: bold;font-weight: 700;font-style: normal;margin: 50px 0px;">Welcome to Beer Brand Brew</p>
                    </div>
                </div>
                <div class="row">
                    <div class="tiles t1"><img src="assets/Main/Build A Beverage.png" alt=""><p>Beer Builder<br><span style="font-weight: 100;font-size: 18px;">( For Bars &amp; Restaurants )</span></p></div>
                    <div class="tiles t2"><img src="assets/Main/Brewery Partners'.png" alt=""><p>Brewery Partners<br><span style="font-weight: 100;font-size: 18px;">(Brewery Partners)</span></p></div>
                    <div class="tiles t3"><img src="assets/Main/Site Administrator.png" alt=""><p>Site Administrator</p></div>
                </div>
            </div>
        </div>
       <script>
           $(document).ready(function(){
               $('.t1').click(function(){
                    window.location.href= '{{route('login')}}';
               });

               $('.t2').click(function(){
					window.location.href= '{{route('login')}}';
                    //window.location.href= '{{url('BrewingPartners')}}';
               });

               $('.t3').click(function(){
                    window.location.href= '{{route('login')}}';
               });
           });
       </script> 
    </body>
</html>