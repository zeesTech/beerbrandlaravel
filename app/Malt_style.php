<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Malt_style extends Model
{
    protected $table = "malt_style";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
