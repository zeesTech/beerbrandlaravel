<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(beerStyleSeeder::class);
        $this->call(brewerInspirationSeeder::class);
        $this->call(abvSeeder::class);
        $this->call(maltStyleSeeder::class);
        $this->call(ibuSeeder::class);
        $this->call(hopProfileSeeder::class);
        $this->call(kegSizesSeeder::class);
    }
}
