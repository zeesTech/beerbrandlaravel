<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brands extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'brand_name', 'address','city','state','zipcode', 'location', 'contact_name' , 'email', 'number', 'license','img_actual_name','img_unique_name'
    ];
}
