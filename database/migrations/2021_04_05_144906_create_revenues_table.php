<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRevenuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revenues', function (Blueprint $table) {
            $table->id();
			$table->integer('home_brewer_id');
			$table->integer('brewer_partner_id');
			$table->integer('batch_id');
			$table->integer('beer_style_id');
			$table->integer('reciepe_id');
			$table->integer('tank_id');
			$table->integer('keg_1_2_size_counter')->nullable();
			$table->json('keg_1_2_size_beerIDs')->nullable();
			$table->integer('keg_1_2_size_revenue')->nullable();
			$table->integer('keg_1_6_size_counter')->nullable();
			$table->json('keg_1_6_size_beerIDs')->nullable();
			$table->integer('keg_1_6_size_revenue')->nullable();
			$table->tinyInteger('status')->default(0);
            $table->integer('createdBy');
            $table->integer('updatedBy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revenues');
    }
}
