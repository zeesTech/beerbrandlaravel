<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToBeersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('beers', function (Blueprint $table) {
            $table->string('package_type',50)->after('notes')->nullable();
            $table->decimal('unit_cost', 10, 2)->after('package_type')->nullable();
            $table->decimal('total_cost', 10, 2)->after('unit_cost')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('beers', function (Blueprint $table) {
            $table->dropColumn('package_type');
            $table->dropColumn('unit_cost');
            $table->dropColumn('total_cost');
        });
    }
}
