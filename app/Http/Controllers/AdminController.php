<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Reciepe;
use App\User;
use App\Beers;
use DB;
use App\Beer_style;
use App\Abv;
use App\Ibu;
use App\Keg_sizes;
use App\Batchorder;
use App\Tank;
use App\Brewery;
use App\Brewers_inspiration;
use App\Beer_cost;
use App\Message;
use App\Brands;
use App\Malt_style;
use App\Hop_profile;
use App\Offerlog; 

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	
	public function __construct()
    {
        $this->total_keg_gallons = 31.5;
    }

    public function index(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function OrdersList(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{			
			$filter_data['beer_styles'] = Beer_style::all();
			$filter_data['abv'] = Abv::all();
			$filter_data['ibu'] = Ibu::all();
			$filter_data['keg_sizes'] = Keg_sizes::all();
			/*
			$data = Beers::where('beers.status',"=","1")->
			join('beer_style','beer_style.id','=','beers.beer_style_id')->
			join('brands','brands.id','=','beers.brand_id')->
			join('ibu','ibu.id','=','beers.ibu_id')->
			join('abv','abv.id','=','beers.abv_id')->
			join('brewers_inspiration','brewers_inspiration.id','=','beers.brewers_inspiration_id')->
			join('keg_sizes','keg_sizes.id','=','beers.keg_size_id')->
			select('beers.id as beer_id','beers.beer_name','beers.package_type','beers.notes','beers.kegs_sell_week','brands.img_unique_name','beers.unit_cost','beers.status','beer_style.name as beer_style_name','ibu.name as ibu_name','abv.name as abv_name','brewers_inspiration.name as brewers_inspiration_name','keg_sizes.name as keg_sizes_name')->get();
			
			$progressOrder = Beers::where('beers.status',"=","2")->
			join('beer_style','beer_style.id','=','beers.beer_style_id')->
			join('brands','brands.id','=','beers.brand_id')->
			join('ibu','ibu.id','=','beers.ibu_id')->
			join('abv','abv.id','=','beers.abv_id')->
			join('brewers_inspiration','brewers_inspiration.id','=','beers.brewers_inspiration_id')->
			join('keg_sizes','keg_sizes.id','=','beers.keg_size_id')->
			select('beers.id as beer_id','beers.beer_name','beers.package_type','beers.notes','beers.kegs_sell_week','brands.img_unique_name','beers.unit_cost','beers.status','beer_style.name as beer_style_name','ibu.name as ibu_name','abv.name as abv_name','brewers_inspiration.name as brewers_inspiration_name','keg_sizes.name as keg_sizes_name')->get();
			*/
						
			$partnerData = User::join('users_roles','users_roles.user_id','=','users.id')
						->join('roles','roles.id','=','users_roles.role_id')
						->where('roles.slug',"=","brewer_partner")
						->select('users.id','users.name','users.phone','users.email','roles.name as role_name')->get();
						
			$reciepeData = Reciepe::join('ibu','ibu.id','=','reciepes.ibu_id')
			->join('abv','abv.id','=','reciepes.abv_id')
			->select('reciepes.*','ibu.name as ibu_name','abv.name as abv_name')->get();
			
			return view('admin/orders_list',['partnerData' => $partnerData, 'reciepeData' => $reciepeData, 'filter_data' => $filter_data ]);
		}		
    }

	public function OrdersUnassigned(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{			
			$filter_data['beer_styles'] = Beer_style::all();
			$filter_data['keg_sizes'] = Keg_sizes::all();
			$filter_data['brewers_inspiration'] = Brewers_inspiration::all();
			$filter_data['reciepes'] = Reciepe::all();
			
			return view('admin/orders_unassigned',['filter_data' => $filter_data ]);
		}		
    }

	public function OrderBeingBuilt(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{			
			$filter_data['beer_styles'] = Beer_style::all();
			$filter_data['abv'] = Abv::all();
			$filter_data['ibu'] = Ibu::all();
			$filter_data['keg_sizes'] = Keg_sizes::all();
						
			$partnerData = User::join('users_roles','users_roles.user_id','=','users.id')
						->join('roles','roles.id','=','users_roles.role_id')
						->where('roles.slug',"=","brewer_partner")
						->select('users.id','users.name','users.phone','users.email','roles.name as role_name')->get();
						
			$reciepeData = Reciepe::join('ibu','ibu.id','=','reciepes.ibu_id')
			->join('abv','abv.id','=','reciepes.abv_id')
			->select('reciepes.*','ibu.name as ibu_name','abv.name as abv_name')->get();
			
			return view('admin/order_being_built',['partnerData' => $partnerData, 'reciepeData' => $reciepeData, 'filter_data' => $filter_data ]);
		}		
    }

	public function OrderBatchesBrewing(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{			
			$filter_data['beer_styles'] = Beer_style::all();
			$filter_data['abv'] = Abv::all();
			$filter_data['ibu'] = Ibu::all();
			$filter_data['keg_sizes'] = Keg_sizes::all();
						
			$partnerData = User::join('users_roles','users_roles.user_id','=','users.id')
						->join('roles','roles.id','=','users_roles.role_id')
						->where('roles.slug',"=","brewer_partner")
						->select('users.id','users.name','users.phone','users.email','roles.name as role_name')->get();
						
			$reciepeData = Reciepe::join('ibu','ibu.id','=','reciepes.ibu_id')
			->join('abv','abv.id','=','reciepes.abv_id')
			->select('reciepes.*','ibu.name as ibu_name','abv.name as abv_name')->get();
			
			return view('admin/order_batches_brewing',['partnerData' => $partnerData, 'reciepeData' => $reciepeData, 'filter_data' => $filter_data ]);
		}		
    }

	public function OrderCompletedBatches(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{			
			$filter_data['beer_styles'] = Beer_style::all();
			$filter_data['abv'] = Abv::all();
			$filter_data['ibu'] = Ibu::all();
			$filter_data['keg_sizes'] = Keg_sizes::all();
						
			$partnerData = User::join('users_roles','users_roles.user_id','=','users.id')
						->join('roles','roles.id','=','users_roles.role_id')
						->where('roles.slug',"=","brewer_partner")
						->select('users.id','users.name','users.phone','users.email','roles.name as role_name')->get();
						
			$reciepeData = Reciepe::join('ibu','ibu.id','=','reciepes.ibu_id')
			->join('abv','abv.id','=','reciepes.abv_id')
			->select('reciepes.*','ibu.name as ibu_name','abv.name as abv_name')->get();
			
			return view('admin/order_completed_batches',['partnerData' => $partnerData, 'reciepeData' => $reciepeData, 'filter_data' => $filter_data ]);
		}		
    }

	public function orderFilter()
	{
		
		$beer_style_id 	= (isset($_POST['beer_style_id']) && !empty($_POST['beer_style_id'])) ? $_POST['beer_style_id'] : false;
		$abv_id 		= (isset($_POST['abv_id']) && !empty($_POST['abv_id'])) ? $_POST['abv_id'] : false;
		$ibu_id 		= (isset($_POST['ibu_id']) && !empty($_POST['ibu_id'])) ? $_POST['ibu_id'] : false;
		$keg_size_id 	= (isset($_POST['keg_size_id']) && !empty($_POST['keg_size_id'])) ? $_POST['keg_size_id'] : false;
		
		$where['beers.status'] = 1;
		if(!empty($beer_style_id))
			$where['beers.beer_style_id'] = $beer_style_id;
		if(!empty($abv_id))
			$where['beers.abv_id'] = $abv_id;
		if(!empty($ibu_id))
			$where['beers.ibu_id'] = $ibu_id;
		if(!empty($keg_size_id))
			$where['beers.keg_size_id'] = $keg_size_id;		
										
		$Filterdata = Beers::where($where)->
			join('beer_style','beer_style.id','=','beers.beer_style_id')->
			join('brands','brands.id','=','beers.brand_id')->
			join('ibu','ibu.id','=','beers.ibu_id')->
			join('abv','abv.id','=','beers.abv_id')->
			join('brewers_inspiration','brewers_inspiration.id','=','beers.brewers_inspiration_id')->
			join('keg_sizes','keg_sizes.id','=','beers.keg_size_id')->
			select('beers.id as beer_id','beers.beer_name','beers.beer_style_id','beers.package_type','beers.notes','beers.kegs_sell_week','brands.img_unique_name','beers.unit_cost','beers.status','beer_style.name as beer_style_name','ibu.name as ibu_name','abv.name as abv_name','brewers_inspiration.name as brewers_inspiration_name','keg_sizes.name as keg_sizes_name')->get();
			
		$html = '';
		if(!empty($Filterdata))
		{
			$i = 0;
			foreach($Filterdata as $index => $filter)
			{
				$i++;
				$admin_brew_beer_link = "adminBrewBeer(this)";//route('admin_brew_beer',['id' => $filter->beer_id]);
				$admin_view_beer_details = "viewBeerDetail(this)";//route('admin_brew_beer',['id' => $filter->beer_id]);
				
				$action = '
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink'.$i.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink'.$i.'">
						<a class="dropdown-item" href="javascript:void(0)" onclick="'.$admin_brew_beer_link.'" data-id="'.$filter->beer_id.'" data-styleID="'.$filter->beer_style_id.'">View details</a>
						<a class="dropdown-item" href="javascript:void(0)" onclick="'.$admin_brew_beer_link.'" data-id="'.$filter->beer_id.'" data-styleID="'.$filter->beer_style_id.'">Brew This Beer</a>
						  <a class="dropdown-item" href="javascript:void(0)">Send Back To Order</a>
						  <a class="dropdown-item" href="javascript:void(0)">Delete This Order</a>
						</div>
					</div>
				';


				$html .= '<tr>';
				if(!empty($beer_style_id))
					$html .= '<td>'.$i.' <input type="checkbox" class="single-checkbox" value="'.$filter->beer_id.'"></td>';
				else
					$html .= '<td>'.$i.'</td>';
					
				$html .= '<td>'.$filter['beer_name'].'</td>';
				$html .= '<td>'.$filter['beer_style_name'].'</td>';
				$html .= '<td>'.$filter['brewers_inspiration_name'].'</td>';
				//$html .= '<td>'.$filter['abv_name'].'</td>';
				//$html .= '<td>'.$filter['ibu_name'].'</td>';
				$html .= '<td>'.$filter['keg_sizes_name'].'</td>';
				$html .= '<td>'.$filter['kegs_sell_week'].'</td>';
				$html .= '<td>'.$action.'</td>';
				$html .= '</tr>';
			}
		}
		return response()->json(array('html'=> $html), 200);
	}
   
	public function createBatch()
	{
		$beerIDs 		= (isset($_POST['beerIDs']) && !empty($_POST['beerIDs'])) ? $_POST['beerIDs'] : false;
		$beer_style_id 	= (isset($_POST['beer_style_id']) && !empty($_POST['beer_style_id'])) ? $_POST['beer_style_id'] : false;
		if(isset($beerIDs) && !empty($beerIDs) && isset($beer_style_id) && !empty($beer_style_id))
		{			
			$data['beer_ids'] 		= json_encode($beerIDs);
			$data['batch_size'] 	= count($beerIDs);
			$data['beer_style_id'] 	= $beer_style_id;
			$data['user_id'] 		= Auth::user()->id;
			$data['updated_by'] 	= Auth::user()->id;
			Batchorder::create($data);
			Beers::whereIn('id', $beerIDs)->update(array( "status" => "2" ));	
		}
		return response()->json(array('status' => true, "msg" => "Create Batch Successfully"), 200);
	}
   
	public function orderBatchFilter()
	{								
		$Filterdata = Batchorder::join('beer_style','beer_style.id','=','batchorders.beer_style_id')->
			select('batchorders.id as batchID','batchorders.batch_size','batchorders.order_status','beer_style.name as beer_style_name')->get();
			
		$html = '';
		if(!empty($Filterdata))
		{
			$i = 0;
			foreach($Filterdata as $index => $filter)
			{
				$i++;
				$order_status = (isset($filter['order_status']) && !empty($filter['order_status'])) ? $filter['order_status'] : false;
				$style = "";
				if(!empty($order_status))
					$style 	= "background-color:#e3e6f0";
									
				$action = '
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="batchdropdownMenuLink'.$i.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>';
				
				if(empty($order_status))
				{			
					$action .= '
							<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="batchdropdownMenuLink'.$i.'">
							  <a class="dropdown-item" data-id="'.$filter['batchID'].'" onclick="BrewThisBatch(this)" href="javascript:void(0)">Brew This Batch</a>
							  <a class="dropdown-item" href="javascript:void(0)">Send Back To Order</a>
							  <a class="dropdown-item" href="javascript:void(0)">Delete This Order</a>
							</div>';
				}
				
				$action .= '</div>';
				
				$html .= '<tr style="'.$style.'">';
				$html .= '<td>'.$filter['batchID'].'</td>';
				$html .= '<td>'.$filter['beer_style_name'].'</td>';
				$html .= '<td>'.$filter['batch_size'].'</td>';
				$html .= '<td>'.$action.'</td>';
				$html .= '</tr>';
			}
		}
		return response()->json(array('html'=> $html), 200);
	}
	
	public function TankListByPartnerID()
	{
		$PartnerID 	= (isset($_POST['PartnerID']) && !empty($_POST['PartnerID'])) ? $_POST['PartnerID'] : false;
		$html		= '';
		if(!empty($PartnerID))
		{
			
			$userdata = User::where('id',$PartnerID)->first();
			$brewerydata = Brewery::where('user_id',$PartnerID)->first();
			$data = Tank::where('user_id',$PartnerID)->where('tank_status',1)->latest()->get();
			if(!empty($data))
			{
				$i = 0;
				foreach($data as $index => $tank)
				{
					$tankFreq = "";
					if($tank->tank_freq == 1)
						$tankFreq = "Single Use";
					else if($tank->tank_freq == 2)
						$tankFreq = "Continuous Use";
						
					$tankStaus = "Unavailable";
					$actionBtn = "-";
					if($tank->tank_status == 1)
					{
						$tankStaus = "Available";
					}
						
					$tankDate = '-';
					if(!empty($tank->tank_date))
						$tankDate = date("F d,Y", strtotime($tank->tank_date));
				
					$i++;

					$html .= '<tr>';
					$html .= '<td>'.$brewerydata['name'].'</td>';
					$html .= '<td>'.$userdata['name'].'</td>';
					$html .= '<td>'.$tank['tank_size'].'</td>';
					$html .= '<td>'.$tankDate.'</td>';
					$html .= '<td>'.$tankFreq.'</td>';
					$html .= '<td>'.$tankStaus.'</td>';
					$html .= '</tr>';
				}
			}
		}
		return response()->json(array('html'=> $html), 200);
	}

	public function TankListByPartnerIDs()
	{
		$PartnerIDs 	= (isset($_POST['PartnerIDs']) && !empty($_POST['PartnerIDs'])) ? $_POST['PartnerIDs'] : false;
		$html		= '';
		if(!empty($PartnerIDs))
		{
			$data = Tank::leftjoin('users','users.id','=','tanks.user_id')
						->leftjoin('breweries','breweries.user_id','=','tanks.user_id')
						->whereIn('tanks.user_id',$PartnerIDs)->where('tanks.tank_status',1)->latest()->get(['tanks.*','users.name as user_name','breweries.name as brewery_name']);
			
			
			if(!empty($data))
			{
				$i = 0;
				foreach($data as $index => $tank)
				{
					$tankFreq = "";
					if($tank->tank_freq == 1)
						$tankFreq = "Single Use";
					else if($tank->tank_freq == 2)
						$tankFreq = "Continuous Use";
						
					$tankStaus = "Unavailable";
					$actionBtn = "-";
					if($tank->tank_status == 1)
					{
						$tankStaus = "Available";
					}
						
					$tankDate = '-';
					if(!empty($tank->tank_date))
						$tankDate = date("F d,Y", strtotime($tank->tank_date));
				
					$tank_size = 0;
					$tank_size =  $tank->tank_size . " barrels / " . round(( $tank->tank_size * $this->total_keg_gallons ),2) ." Gallons";
							
					$i++;
					$html .= '<tr>';
					$html .= '<td>'.$tank['brewery_name'].'</td>';
					$html .= '<td>'.$tank['user_name'].'</td>';
					$html .= '<td>'.$tank_size.'</td>';
					$html .= '<td>'.$tankFreq.'</td>';
					$html .= '<td>'.$tankStaus.'</td>';
					$html .= '</tr>';
				}
			}
		}
		return response()->json(array('html'=> $html), 200);
	}
	
	public function createOrder()
	{
		$batch_id 		= (isset($_POST['batch_id']) && !empty($_POST['batch_id'])) ? $_POST['batch_id'] : false;
		$partner_id 	= (isset($_POST['brewer_partner_id']) && !empty($_POST['brewer_partner_id'])) ? $_POST['brewer_partner_id'] : false;
		$recipe_id 		= (isset($_POST['recipe_id']) && !empty($_POST['recipe_id'])) ? $_POST['recipe_id'] : false;
		
		$keg_Array = array();
		$RevenuePara = array();
		$HomeBrewerID = '';
		$BrewerPartnerID = '';
		if(!empty($batch_id) && !empty($partner_id) && !empty($recipe_id))
		{					
			$data['partner_id'] 	= $partner_id;
			$data['reciepe_id'] 	= $recipe_id;
			$data['order_status'] 	= 1;
			$data['updated_by'] 	= Auth::user()->id;
			$result = Batchorder::where('id', $batch_id)->update($data);
			if($result)
			{
				$OrderData = Batchorder::where('id', $batch_id)->first();
				
				if(!empty($OrderData) && isset($OrderData->reciepe_id) && !empty($OrderData->reciepe_id))
				{
					$RecipeData = Reciepe::where('id', $OrderData->reciepe_id)->first();
					if(!empty($RecipeData) && isset($RecipeData->user_id) && !empty($RecipeData->user_id))
					{
						$HomeBrewerID 					= $RecipeData->user_id;
						
						$para['notification_type_id'] 	= 3; // for 3 Attach Recipe
						$para['sender_id'] 				= Auth::user()->id;
						$para['receiver_id'] 			= $RecipeData->user_id;
						$para['action'] 				= "Attach Recipe";
						$para['action_id'] 				= $batch_id;
						
						app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
					}
				}
				
				if(!empty($OrderData) && isset($OrderData->partner_id) && !empty($OrderData->partner_id))
				{
					$BrewerPartnerID 				= $OrderData->partner_id;
					
					$para['notification_type_id'] 	= 2; // for 2 Brew This Batch
					$para['sender_id'] 				= Auth::user()->id;
					$para['receiver_id'] 			= $OrderData->partner_id;
					$para['action'] 				= "Brew this Batch";
					$para['action_id'] 				= $batch_id;
					
					app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
				}
				
				
				if(!empty($OrderData) && isset($OrderData->beer_ids) && !empty($OrderData->beer_ids))
				{
					$BeerData = Beers::whereIn('id', json_decode($OrderData->beer_ids))->get();
					if(!empty($BeerData))
					{
						foreach($BeerData as $index => $beer)
						{
							$para['notification_type_id'] 	= 4; // for 4 Beer in process
							$para['sender_id'] 				= Auth::user()->id;
							$para['receiver_id'] 			= $beer->user_id;
							$para['action'] 				= "Beer in process";
							$para['action_id'] 				= $batch_id;
							
							app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
							
							if(!empty($beer->keg_size_id))
							{
								$keg_Array[$beer->keg_size_id][] = $beer->id;
							}
							
						}
					}
				}
				
				$RevenuePara['batch_id']			= $batch_id;
				$RevenuePara['kegs']				= $keg_Array;
				$RevenuePara['beer_style_id']		= (!empty($OrderData) && isset($OrderData->beer_style_id)) ? $OrderData->beer_style_id : false;
				$RevenuePara['reciepe_id']			= (!empty($OrderData) && isset($OrderData->reciepe_id)) ? $OrderData->reciepe_id : false;
				$RevenuePara['home_brewer_id']		= (isset($HomeBrewerID) && !empty($HomeBrewerID)) ? $HomeBrewerID : false;
				$RevenuePara['brewer_partner_id']	= (isset($BrewerPartnerID) && !empty($BrewerPartnerID)) ? $BrewerPartnerID : false;
				$RevenuePara['createdBy']			= Auth::user()->id;
				app('App\Http\Controllers\RevenueController')->CreateRevenueByOrder($RevenuePara);
			}
		}
		return response()->json(array('status' => true, "msg" => "Order Successfully Created"), 200);
	}

	public function BrewBeer($id)
    {
		if(!empty($id))
		{
			Beers::where('id', $id)->update(array("status" => 2));
		}

		return redirect()->route('orders_list');
    }
	
	public function adminBrewBeer()
    {
		$BeerID 	= (isset($_POST['BeerID']) && !empty($_POST['BeerID'])) ? $_POST['BeerID'] : false;
		$styleID 	= (isset($_POST['styleID']) && !empty($_POST['styleID'])) ? $_POST['styleID'] : false;
		if(isset($BeerID) && !empty($BeerID) && isset($styleID) && !empty($styleID))
		{
			$BeerIDs[] 				= $BeerID;
			$data['beer_ids'] 		= json_encode($BeerIDs);
			$data['batch_size'] 	= count($BeerIDs);
			$data['beer_style_id'] 	= $styleID;
			$data['user_id'] 		= Auth::user()->id;
			$data['updated_by'] 	= Auth::user()->id;
			Batchorder::create($data);
			Beers::where('id', $BeerID)->update(array("status" => 2));
		}

		return response()->json(array('status' => true, "msg" => "Brew Beer Successfully"), 200);
    }
	
    public function CustomerList(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = User::join('users_roles','users_roles.user_id','=','users.id')
						->join('roles','roles.id','=','users_roles.role_id')
						->where('roles.slug',"=","beer_builder")
						->select('users.id','users.name','users.phone','users.email','roles.name as role_name')->get();
						
			return view('admin/customer_list',['data' => $data]);
		}		
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	 
	public function TotalRevenue(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$user_id = Auth::user()->id;
			
			$data = array();
			$data['revenue'] = Batchorder::where("order_status" , "!=" , 0)
								->where(array("home_brewer_id" => $user_id))
								->join('beer_style','beer_style.id','=','batchorders.beer_style_id')
								->join('reciepes','reciepes.id','=','batchorders.reciepe_id')
								->join('revenues','revenues.batch_id','=','batchorders.id')
								->select('batchorders.id as batchID','batchorders.order_status','batchorders.name as batchName','batchorders.batch_size','batchorders.order_status','beer_style.name as beer_style_name','reciepes.beer_name as reciepe_name','revenues.keg_1_2_size_counter','revenues.keg_1_2_size_revenue','revenues.keg_1_6_size_counter','revenues.keg_1_6_size_revenue')
								->get();
			return view('admin/my_revenue',['data' => $data]);
		}
    }
	
	public function BreweryPartners(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = array();
			return view('admin/brewery_partners',['data' => $data]);
		}
    }
	
	public function PartnersList(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = User::join('users_roles','users_roles.user_id','=','users.id')
						->join('roles','roles.id','=','users_roles.role_id')
						->where('roles.slug',"!=","beer_builder")
						->where('roles.slug',"!=","administrator")
						->select('users.id','users.name','users.email','users.phone','users.address','users.notes','users.biography','roles.name as role_name')->get();
			
			return view('admin/partners_list',['data' => $data]);
		}
    }
	
	public function AddReciepe(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = array();
			$data['beer_styles'] 	= Beer_style::all();
			$data['abv'] 			= Abv::all();
			$data['ibu'] 			= Ibu::all();
			return view('admin/add_reciepe',['data' => $data]);
		}		
    }
	
	public function CreateReciepe(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = $request->all();
			
			$image_original_name  = '';
			$image_unique_name = '';
			if($request->hasFile('image'))
			{
				$fileobj				= $request->file('image');
				$image_original_name 	= $fileobj->getClientOriginalName('image');
				$file_extension_name 	= $fileobj->getClientOriginalExtension('image');
				$image_unique_name 		= time().rand(1000,9999).'.'.$file_extension_name;
				$destinationPath		= public_path('/uploads/');
				$fileobj->move($destinationPath,$image_unique_name);
			}

			$data['type'] 				= "IBU";
			$data['img_actual_name'] 	= $image_original_name;
			$data['img_unique_name'] 	= $image_unique_name;
			
			$data['user_id'] = Auth::user()->id;
			$reciepe = Reciepe::create($data);
			return redirect()->route('recipes_list');
		}
    }
	
	public function ReciepesList(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = array();
			//$data['reciepes'] = Reciepe::all();
			$limit = 10;
			$data['reciepes'] = Reciepe::latest()
			->select('reciepes.*','beer_style.name as beer_style_name','ibu.name as ibu_name','abv.name as abv_name')
			->join('beer_style','beer_style.id','=','reciepes.beer_style_id')
			->join('ibu','ibu.id','=','reciepes.ibu_id')
			->join('abv','abv.id','=','reciepes.abv_id')			
			->paginate($limit);
			return view('admin/reciepes_list',['data' => $data]);
		}
    }
		
	public function ReciepeView($id, Request $request)
	{
		
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data['reciepe'] = Reciepe::findorfail($id);
			return view('admin/reciepe_view',['data' => $data]);
		}
    }
	
	public function BeerMatching(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = array();
			return view('admin/beer_matching',['data' => $data]);
		}
    }

	public function HomeBrewerSubmission(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = array();
			return view('admin/home_brewer_submission',['data' => $data]);
		}
    }

	public function beer_details()
    {
		$Filterdata = array();
		$BeerID 	= (isset($_POST['BeerID']) && !empty($_POST['BeerID'])) ? $_POST['BeerID'] : false;
		$styleID 	= (isset($_POST['styleID']) && !empty($_POST['styleID'])) ? $_POST['styleID'] : false;
		if(isset($BeerID) && !empty($BeerID) && isset($styleID) && !empty($styleID))
		{
			$Filterdata = Beers::where(array('beers.id' => $BeerID))->
			join('beer_style','beer_style.id','=','beers.beer_style_id')->
			join('brands','brands.id','=','beers.brand_id')->
			join('users','users.id','=','beers.user_id')->
			join('brewers_inspiration','brewers_inspiration.id','=','beers.brewers_inspiration_id')->
			select('beers.id as beer_id','beers.kegs_sell_week','beers.order_keg_size_id','beers.keg_size_id','beers.beer_name','beers.beer_style_id','beers.package_type','beers.notes','beers.kegs_sell_week','beers.order_notes','brands.brand_name','brands.img_unique_name','beers.unit_cost','beers.status','beer_style.name as beer_style_name','brewers_inspiration.name as brewers_inspiration_name','users.name as customer_name','users.address','users.phone','users.email',DB::raw("DATE_FORMAT(beers.order_date, '%M %d, %Y') as order_date"))->first();
			
		}
		return response()->json(array('data'=> $Filterdata), 200);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	public function unassignedOrderFilter()
	{
		$beer_style_id 	= (isset($_POST['beer_style_id']) && !empty($_POST['beer_style_id'])) ? $_POST['beer_style_id'] : false;
		$keg_size_id 	= (isset($_POST['keg_size_id']) && !empty($_POST['keg_size_id'])) ? $_POST['keg_size_id'] : false;
		
		$where['beers.status'] = 1;
		if(!empty($beer_style_id))
			$where['beers.beer_style_id'] = $beer_style_id;
		if(!empty($keg_size_id))
			$where['beers.order_keg_size_id'] = $keg_size_id;		
										
		$Filterdata = Beers::where($where)->
			join('beer_style','beer_style.id','=','beers.beer_style_id')->
			join('brands','brands.id','=','beers.brand_id')->
			join('users','users.id','=','beers.user_id')->
			leftjoin('brewers_inspiration','brewers_inspiration.id','=','beers.brewers_inspiration_id')->
			leftjoin('keg_sizes','keg_sizes.id','=','beers.keg_size_id')->
			leftjoin('keg_sizes as keg2','keg2.id','=','beers.order_keg_size_id')->
			select('beers.id as beer_id','beers.beer_size','beers.beer_name','beers.beer_style_id','beers.package_type','beers.notes','beers.kegs_sell_week','beers.kegs_sell_week','brands.brand_name','brands.img_unique_name','beers.unit_cost','beers.total_cost','beers.status','beer_style.name as beer_style_name','brewers_inspiration.name as brewers_inspiration_name','keg_sizes.name as keg_sizes_name','keg2.name as order_keg_sizes_name','users.name as customer_name','users.address','users.phone','users.email')->get();
			
		$html = '';
		if(!empty($Filterdata))
		{
			$i = 0;
			foreach($Filterdata as $index => $filter)
			{
				$i++;
				$admin_brew_beer_link = "singleAssignBatch(this)";//route('admin_brew_beer',['id' => $filter->beer_id]);
				$admin_view_beer_details = "viewBeerDetail(this)";//route('admin_brew_beer',['id' => $filter->beer_id]);
				
				$action = '
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink'.$i.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink'.$i.'">
							<a class="dropdown-item" href="javascript:void(0)" onclick="'.$admin_view_beer_details.'" data-id="'.$filter->beer_id.'" data-styleID="'.$filter->beer_style_id.'">View details</a>
							<a class="dropdown-item" href="javascript:void(0)" onclick="'.$admin_brew_beer_link.'" data-id="'.$filter->beer_id.'" data-styleID="'.$filter->beer_style_id.'">Assign to a Batch</a>
							<a class="dropdown-item" href="javascript:void(0)" onclick="editAmount(this)" data-id="'.$filter->beer_id.'" data-amount="'.$filter->total_cost.'">Edit Amount</a>
						</div>
					</div>
				';


				$html .= '<tr>';
				if(!empty($beer_style_id))
					$html .= '<td>'.$i.' <input type="checkbox" class="single-checkbox" value="'.$filter->beer_id.'"></td>';
				else
					$html .= '<td>'.$i.'</td>';
					
				$keg_sizes_name = (isset($filter['order_keg_sizes_name']) && !empty($filter['order_keg_sizes_name'])) ? $filter['order_keg_sizes_name'] : $filter['keg_sizes_name'];
				
				$beer_total_size_text = 0;
				$total_keg_gallons = $this->total_keg_gallons;
				$beer_total_size_text = round(( $filter['beer_size'] / $total_keg_gallons ),2) . " barrels / " . $filter['beer_size'] ." Gallons";
					
				$html .= '<td>'.$filter['customer_name'].'</td>';
				$html .= '<td>'.$filter['brand_name'].'</td>';
				$html .= '<td>'.$beer_total_size_text.' </td>';
				//$html .= '<td>'.$filter['address'].'</td>';
				//$html .= '<td>'.$filter['phone'].'</td>';
				//$html .= '<td>'.$filter['email'].'</td>';
				$html .= '<td>'.$filter['beer_style_name'].'</td>';
				$html .= '<td>'.$filter['brewers_inspiration_name'].'</td>';
				//$html .= '<td>'.$keg_sizes_name.'</td>';
				//$html .= '<td>'.$filter['kegs_sell_week'].'</td>';
				$html .= '<td>'.$action.'</td>';
				$html .= '</tr>';
			}
		}
		return response()->json(array('html'=> $html), 200);
	}
		
	public function saveOrder(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = $request->all();
			$data['updated_by'] = Auth::user()->id;
			$id = (!empty($data) && isset($data['beer_id']) && !empty($data['beer_id'])) ? $data['beer_id'] : false;
			if(!empty($id))
			{
				$dataArray['order_keg_size_id'] = $data['keg_size_id'];
				$dataArray['order_notes'] = $data['order_notes'];
				$reponse = Beers::where(array('id' => $id))->update($dataArray);
			}
					
			return response()->json(array('status'=> true , 'msg' => "Data updated successfully"), 200);
		}
		else
		{
			return response()->json(array('status'=> false , 'msg' => "Something went wrong"), 200);
		}
    }
	
	public function loadBatchList()
	{
		$beer_style_id 	= (isset($_POST['beer_style_id']) && !empty($_POST['beer_style_id'])) ? $_POST['beer_style_id'] : false;
		$where['batchorders.order_status'] = 0;
		if(!empty($beer_style_id))
			$where['batchorders.beer_style_id'] = $beer_style_id;	
										
		$Filterdata = Batchorder::where($where)
					->join('beer_style','beer_style.id','=','batchorders.beer_style_id')
					->join('brewers_inspiration','brewers_inspiration.id','=','batchorders.brewers_inspiration_id')
					->join('reciepes','reciepes.id','=','batchorders.reciepe_id')
					->select('batchorders.*','reciepes.beer_name','beer_style.name as beer_style_name','brewers_inspiration.name as brewers_inspiration_name')
					->get();

		$html = '';
		if(!empty($Filterdata->toArray()))
		{
			$i = 0;
			foreach($Filterdata as $index => $filter)
			{
				$i++;	
				$batch_size = (!empty($filter['batch_size'])) ? $filter['batch_size'] : 0;
				
				$html .= '<tr>';
				$html .= '<td>'.$i.'</td>';			
				$html .= '<td>'.$filter['name'].'</td>';
				$html .= '<td>'.$filter['beer_style_name'].'</td>';
				$html .= '<td>'.$filter['brewers_inspiration_name'].'</td>';
				$html .= '<td>'.$filter['beer_name'].'</td>';
				$html .= '<td>'.$batch_size.'</td>';
				$html .= '<td><input class="batchRatio" type="radio" value="'.$filter['id'].'" name="selectBatcdID" id="selectBatcdID" /></td>';
				$html .= '</tr>';
			}
		}
		else
		{
			$html .= '<tr><td colspan="7"> <center>Data Not Found.!</center></td></tr>';
		}
		return response()->json(array('html'=> $html), 200);
	}

	public function attchBatch()
	{
		$beerIDs 	= (isset($_POST['beerIDs']) && !empty($_POST['beerIDs'])) ? $_POST['beerIDs'] : false;
		$BatchID 	= (isset($_POST['BatchID']) && !empty($_POST['BatchID'])) ? $_POST['BatchID'] : false;
		if(!empty($BatchID) && !empty($beerIDs))
		{	
			$beerIDsArray 	= array();
			$beer_ids 		= array();
			
			$batchData = Batchorder::where('id', $BatchID)->first('beer_ids');
			if(!empty($batchData) && isset($batchData['beer_ids']) && !empty($batchData['beer_ids']))
			{
				$beer_ids = json_decode($batchData['beer_ids']);
			}
			
			$beerIDsArray = array_merge($beer_ids,$beerIDs);

			$data['beer_ids'] 		= json_encode($beerIDsArray);
			$data['batch_size'] 	= count($beerIDsArray);
			$data['updated_by'] 	= Auth::user()->id;
			$result = Batchorder::where('id', $BatchID)->update($data);
			if($result)
			{
				Beers::whereIn('id', $beerIDs)->update(array( "status" => "2" ));
			}
		}
		return response()->json(array('status' => true, "msg" => "Batch Successfully Assigned"), 200);
	}
	
	public function saveNewBatch(Request $request)
	{
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = $request->all();
			$beerIDs 				= (isset($_POST['beerIDs']) && !empty($_POST['beerIDs'])) ? $_POST['beerIDs'] : false;
			$name 					= (isset($_POST['name']) && !empty($_POST['name'])) ? $_POST['name'] : false;
			$beer_style_id 			= (isset($_POST['beer_style_id']) && !empty($_POST['beer_style_id'])) ? $_POST['beer_style_id'] : false;
			$reciepe_id 			= (isset($_POST['reciepe_id']) && !empty($_POST['reciepe_id'])) ? $_POST['reciepe_id'] : false;
			$brewers_inspiration_id = (isset($_POST['brewers_inspiration_id']) && !empty($_POST['brewers_inspiration_id'])) ? $_POST['brewers_inspiration_id'] : false;
			if(!empty($beerIDs) && !empty($beer_style_id))
			{
				$Array['name'] 					= $name;
				$Array['beer_style_id'] 		= $beer_style_id;
				$Array['reciepe_id'] 			= $reciepe_id;
				$Array['brewers_inspiration_id'] = $brewers_inspiration_id;
				$Array['beer_ids'] 				= json_encode($beerIDs);
				$Array['batch_size'] 			= count($beerIDs);
				$Array['updated_by'] 			= Auth::user()->id;
				$Array['user_id'] 				= Auth::user()->id;
				$reponse = Batchorder::create($Array);
				
				if($reponse)
				{
					Beers::whereIn('id', $beerIDs)->update(array( "status" => "2" ));
				}
			
				return response()->json(array('status' => true, "msg" => "Batch Successfully Assigned"), 200);
			}
			else
			{
				return response()->json(array('status' => false, "msg" => "Something went wrong."), 200);
			}
		}
	}

	public function CreateOrderBatch(Request $request)
    {
        $user = $request->user();
		if($user->hasRole('administrator'))
		{
			$para = $request->all();
			$orderData = array();
			if(isset($para) && isset($para['id']) && !empty($para['id']))
			{
				$orderData = Batchorder::findorfail($para['id']);
			}
			
			$data['orderData'] = $orderData;
			$data['beer_styles'] = Beer_style::all();
			$data['brewers_inspiration'] = Brewers_inspiration::all();
			$data['reciepes'] = Reciepe::all();
			return view('admin/saveOrderBatch',$data);
		}
    }
	
	public function SaveOrderBatch(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = $request->all();
			$data['updated_by'] = Auth::user()->id;
			$id = (!empty($data) && isset($data['id']) && !empty($data['id'])) ? $data['id'] : false;
			if(!empty($id))
			{
				unset($data['_token']);
				$reponse = Batchorder::where(array('id' => $data['id']))->update($data);
			}
			else
			{
				$data['user_id'] 	= Auth::user()->id;
				$reponse = Batchorder::create($data);
			}
			
			return redirect('/order_being_built');
		}
    }
	
	public function orderBatchesBeingBuiltFilter()
	{
		$beer_style_id 	= (isset($_POST['beer_style_id']) && !empty($_POST['beer_style_id'])) ? $_POST['beer_style_id'] : false;
		$where['batchorders.order_status'] = 0;
		if(!empty($beer_style_id))
			$where['batchorders.beer_style_id'] = $beer_style_id;	
										
		$Filterdata = Batchorder::where($where)
					->join('beer_style','beer_style.id','=','batchorders.beer_style_id')
					->join('brewers_inspiration','brewers_inspiration.id','=','batchorders.brewers_inspiration_id')
					->join('reciepes','reciepes.id','=','batchorders.reciepe_id')
					->select('batchorders.*','reciepes.beer_name','beer_style.name as beer_style_name','brewers_inspiration.name as brewers_inspiration_name')
					->get();

		$html = '';
		if(!empty($Filterdata))
		{
			$i = 0;
			foreach($Filterdata as $index => $filter)
			{
				$i++;
				
				$beer_total_size = 0;
				$beer_total_size_text = 0;
				$total_keg_gallons = $this->total_keg_gallons;
				$batch_size = (!empty($filter['batch_size'])) ? $filter['batch_size'] : 0;
				$beer_json = (!empty($filter['beer_ids'])) ? json_decode($filter['beer_ids']) : 0;
				if(!empty($beer_json))
				{
					$beer_total_size = Beers::whereIn('id',$beer_json)->sum('beer_size');
					$beer_total_size_text = round(( $beer_total_size / $total_keg_gallons ),2) . " barrels / " . $beer_total_size ." Gallons";
				}
				
				$beerIDs = '';
				if(!empty($filter['beer_ids']))
					$beerIDs = $filter['beer_ids'];
					
				$beer_link = "-";
				if(!empty($batch_size))
					$beer_link = "<a class='dropdown-item' style='color:#FF6000;' data-beerIDs='".$beerIDs."' href='javascript:void(0)' onclick='getBeerInfo(this)'>Detail</a>";

				$href = "javascript:void(0)";
				if(!empty($filter->reciepe_id))
					$href = route('recipe_view',['id' => $filter->reciepe_id]);
				
				$reciepe_link = '<a class="dropdown-item" style="color:#FF6000;" href="'.$href.'" target="_blank">'.$filter['beer_name'].'View</a>';
				
				$assign_to_brewer = "assignToBrewer(this)";
				//if(!empty($batch_size))
				//	$assign_to_brewer = route('AssignToBrewer',['id' => $filter->id]);
					
				$delete_batch = route('DeleteSaveOrderBatch',['id' => $filter->id]);
				$edit_batch = route('EditOrderBatch',['id' => $filter->id]);
				
				$action = '
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink'.$i.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink'.$i.'">';
						
				if(!empty($batch_size))
				{
					$getLogIDs = Offerlog::where('batch_id',$filter->id)->select(DB::raw("group_concat(partner_id SEPARATOR '|') as PartnerIds"))->first()->toArray();
					$PartnerIds = (!empty($getLogIDs) && isset($getLogIDs['PartnerIds'])) ? $getLogIDs['PartnerIds'] : false;
					
					$action .= '<a class="dropdown-item" href="javascript:void(0)" onclick="'.$assign_to_brewer.'" data-id="'.$filter->id.'" data-styleID="'.$filter->beer_style_id.'" data-partnerIds="'.$PartnerIds.'">Assign To Brewer</a>';

				}
				
				$action .= '<a class="dropdown-item" href="'.$edit_batch.'" data-id="'.$filter->id.'" data-styleID="'.$filter->beer_style_id.'">Edit This Batch</a>
						<a class="dropdown-item" href="'.$delete_batch.'" data-id="'.$filter->id.'" data-styleID="'.$filter->beer_style_id.'">Delete This Batch</a>
						</div>
					</div>
				';

				$style = "";
				if(!empty($filter->offer_status) && $filter->offer_status == 1)
					$style = "background-color:#e3e6f0";
				
				$html .= '<tr style="'.$style.'">';
				if(!empty($beer_style_id))
					$html .= '<td>'.$i.'</td>';
				else
					$html .= '<td>'.$i.'</td>';
					
				$html .= '<td>'.$filter['name'].'</td>';
				$html .= '<td>'.$filter['beer_style_name'].'</td>';
				$html .= '<td>'.$filter['brewers_inspiration_name'].'</td>';
				$html .= '<td>'.$beer_link.'</td>';
				$html .= '<td>'.$reciepe_link.'</td>';
				$html .= '<td>'.$beer_total_size_text.'</td>';
				$html .= '<td>'.$action.'</td>';
				$html .= '</tr>';
			}
		}
		return response()->json(array('html'=> $html), 200);
	}

	public function DeleteSaveOrderBatch(Request $request)
	{
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = $request->all();
			$id = (!empty($data) && isset($data['id']) && !empty($data['id'])) ? $data['id'] : false;
			if(!empty($id))
			{
				$batchData = Batchorder::where('id', $id)->first('beer_ids');
				Batchorder::where(array('id' => $id))->delete();
				if(!empty($batchData) && isset($batchData['beer_ids']) && !empty($batchData['beer_ids']))
				{
					$beer_ids = json_decode($batchData['beer_ids']);
					if(!empty($beer_ids))
					{
						Beers::whereIn('id', $beer_ids)->update(array( "status" => "1" ));
					}
				}
			}
			
			return redirect('/order_being_built');
		}
	}
		
	public function AssignToBrewer(Request $request)
	{
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = $request->all();
			$id 		= (!empty($data) && isset($data['batch_id']) && !empty($data['batch_id'])) ? $data['batch_id'] : false;
			$PartnerID 	= (!empty($data) && isset($data['brewer_partner_id']) && !empty($data['brewer_partner_id'])) ? $data['brewer_partner_id'] : false;
			if(!empty($id) && !empty($PartnerID))
			{
				$batchData = Batchorder::where('id', $id)->first(['beer_ids','beer_style_id','reciepe_id']);
				if(!empty($batchData) && isset($batchData['beer_ids']) && !empty($batchData['beer_ids']))
				{
					$beer_ids = json_decode($batchData['beer_ids']);
					if(!empty($beer_ids))
					{
						Beers::whereIn('id', $beer_ids)->update(array( "status" => "3" ));
					}
					
					$HomeBrewerID = 0;
					$BrewerPartnerID = 0;
					if(!empty($batchData) && isset($batchData->reciepe_id) && !empty($batchData->reciepe_id))
					{
						$RecipeData = Reciepe::where('id', $batchData->reciepe_id)->first();
						if(!empty($RecipeData) && isset($RecipeData->user_id) && !empty($RecipeData->user_id))
						{
							$HomeBrewerID 					= $RecipeData->user_id;
							
							$para['notification_type_id'] 	= 3; // for 3 Attach Recipe
							$para['sender_id'] 				= Auth::user()->id;
							$para['receiver_id'] 			= $RecipeData->user_id;
							$para['action'] 				= "Attach Recipe";
							$para['action_id'] 				= $id;
							
							app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
						}
					}
					
					if(!empty($PartnerID) && isset($PartnerID) && !empty($PartnerID))
					{
						$BrewerPartnerID 				= $PartnerID;
						
						$para['notification_type_id'] 	= 2; // for 2 Brew This Batch
						$para['sender_id'] 				= Auth::user()->id;
						$para['receiver_id'] 			= $PartnerID;
						$para['action'] 				= "Brew this Batch";
						$para['action_id'] 				= $id;
						
						app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
					}
				
					$keg_Array = array();
					$key_1_2_cost = 0;
					$key_1_6_cost = 0;
					$total_cost = 0;
					if(!empty($batchData) && isset($batchData->beer_ids) && !empty($batchData->beer_ids))
					{
						$BeerData = Beers::whereIn('id', json_decode($batchData->beer_ids))->get();
						if(!empty($BeerData))
						{
							foreach($BeerData as $index => $beer)
							{
								$para['notification_type_id'] 	= 4; // for 4 Beer in process
								$para['sender_id'] 				= Auth::user()->id;
								$para['receiver_id'] 			= $beer->user_id;
								$para['action'] 				= "Beer in process";
								$para['action_id'] 				= $id;
								
								app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
								
								if(!empty($beer->keg_size_id))
								{
									if($beer->keg_size_id == 1)
										$key_1_2_cost = $key_1_2_cost + $beer->total_cost;
									else if($beer->keg_size_id == 2)
										$key_1_6_cost = $key_1_6_cost + $beer->total_cost;
										
									$keg_Array[$beer->keg_size_id][] = $beer->id;
								}
								
								$total_cost = $key_1_2_cost + $key_1_6_cost;
							}
						}
					}
				
					Batchorder::where(array('id' => $id))->update(array( "order_status" => "1" , "partner_id" => $PartnerID ,"keg_1_2_size_beer_cost" => $key_1_2_cost ,"keg_1_6_size_beer_cost" => $key_1_6_cost ,"total_cost" => $total_cost , 'order_date' => date('Y-m-d') ));
					
					$RevenuePara['batch_id']			= $id;
					$RevenuePara['kegs']				= $keg_Array;
					$RevenuePara['beer_style_id']		= (!empty($batchData) && isset($batchData->beer_style_id)) ? $batchData->beer_style_id : false;
					$RevenuePara['reciepe_id']			= (!empty($batchData) && isset($batchData->reciepe_id)) ? $batchData->reciepe_id : false;
					$RevenuePara['home_brewer_id']		= $HomeBrewerID;
					$RevenuePara['brewer_partner_id']	= $BrewerPartnerID;
					$RevenuePara['createdBy']			= Auth::user()->id;
					
					//echo'<pre>'; print_r($RevenuePara); die;
					app('App\Http\Controllers\RevenueController')->CreateHomeBrewerRevenueByOrder($RevenuePara);
					app('App\Http\Controllers\RevenueController')->CreateBreweryPartnerRevenueByOrder($RevenuePara);
				
				}
				
				return response()->json(array('status'=> true, "msg" => "successfully."), 200);
			}	
			else
			{
				return response()->json(array('status'=> true, "msg" => "something went wrong."), 200);
			}
		}		
	}
	
	public function orderBatchesBrewingFilter()
	{
		$beer_style_id 	= (isset($_POST['beer_style_id']) && !empty($_POST['beer_style_id'])) ? $_POST['beer_style_id'] : false;
		if(!empty($beer_style_id))
		{
			$where['batchorders.beer_style_id'] = $beer_style_id;							
			$Filterdata = Batchorder::where($where)
						->whereBetween('batchorders.order_status', [1, 4])
						->join('beer_style','beer_style.id','=','batchorders.beer_style_id')
						->join('brewers_inspiration','brewers_inspiration.id','=','batchorders.brewers_inspiration_id')
						->join('reciepes','reciepes.id','=','batchorders.reciepe_id')
						->join('users','users.id','=','batchorders.partner_id')
						->select('batchorders.*','users.name as partner_name','users.email','users.phone','reciepes.beer_name','beer_style.name as beer_style_name','brewers_inspiration.name as brewers_inspiration_name')
						->get();
		}
		else
		{									
			$Filterdata = Batchorder::whereBetween('batchorders.order_status', [1, 4])
						->join('beer_style','beer_style.id','=','batchorders.beer_style_id')
						->join('brewers_inspiration','brewers_inspiration.id','=','batchorders.brewers_inspiration_id')
						->join('reciepes','reciepes.id','=','batchorders.reciepe_id')
						->join('users','users.id','=','batchorders.partner_id')
						->select('batchorders.*','users.name as partner_name','users.email','users.phone','reciepes.beer_name','beer_style.name as beer_style_name','brewers_inspiration.name as brewers_inspiration_name')
						->get();
		}
		

		$html = '';
		if(!empty($Filterdata))
		{
			$i = 0;
			foreach($Filterdata as $index => $filter)
			{
				$i++;
				
				$beer_total_size = 0;
				$beer_total_size_text = 0;
				$total_keg_gallons = $this->total_keg_gallons;
				$beer_json = (!empty($filter['beer_ids'])) ? json_decode($filter['beer_ids']) : 0;
				if(!empty($beer_json))
				{
					$beer_total_size = Beers::whereIn('id',$beer_json)->sum('beer_size');
					$beer_total_size_text = round(( $beer_total_size / $total_keg_gallons ),2) . " barrels / " . $beer_total_size ." Gallons";
				}
				
				$batch_size = (!empty($filter['batch_size'])) ? $filter['batch_size'] : 0;
				
				$href = "javascript:void(0)";
				if(!empty($filter->reciepe_id))
					$href = route('recipe_view',['id' => $filter->reciepe_id]);
				
				$beerIDs = '';
				if(!empty($filter['beer_ids']))
					$beerIDs = $filter['beer_ids'];
					
				$reciepe_link = '<a class="dropdown-item" style="color:#FF6000;" href="'.$href.'" target="_blank">'.$filter['beer_name'].' View</a>';
				$partner_link = '<a class="dropdown-item" style="color:#FF6000;" data-name="'.$filter['partner_name'].'" data-email="'.$filter['email'].'" data-phone="'.$filter['phone'].'" href="javascript:void(0)" onclick="getPartnerInfo(this)">'.$filter['partner_name'].'</a>';
				$beer_link = "<a class='dropdown-item' style='color:#FF6000;' data-beerIDs='".$beerIDs."' href='javascript:void(0)' onclick='getBeerInfo(this)'>Detail</a>";
				
				$assign_to_brewer = "javascript:void(0)";
				if(!empty($batch_size))
					$assign_to_brewer = route('AssignToBrewer',['id' => $filter->id]);
					
				$delete_batch = route('DeleteSaveOrderBatch',['id' => $filter->id]);
				$edit_batch = route('EditOrderBatch',['id' => $filter->id]);
				$order_status = (isset($filter->order_status) && !empty($filter->order_status)) ? $filter->order_status : false;
				
				$StatusArray = array( 1 => "Shipping Supplies", 2 => "Supplies Delivered", 3 => "Brewing Beer", 4 => "Ready for Pickup", 5 => "Completed" );
				
				/*
				if($order_status >= 3)
				{
					$status = '<select class="form-control" placeholder="Status" name="status_id" data-id="'.$filter->id.'" onchange="ChangeStatus(this)"> ';
						if(!empty($StatusArray))
						{
							for($k=1;$k<=count($StatusArray);$k++)
							{
								$selected = '';
								if($k == $order_status)
									$selected = 'selected';
									
								$status .= '<option value="'.$k.'" '.$selected.'>'.$StatusArray[$k].'</option>';
							}
						}						
					$status .= '</select>';
				}
				else
				{
					$status = (!empty($StatusArray) && isset($StatusArray[$order_status]) && !empty($StatusArray[$order_status])) ? $StatusArray[$order_status] : "-";
				}
				*/
				
				$status = (!empty($StatusArray) && isset($StatusArray[$order_status]) && !empty($StatusArray[$order_status])) ? $StatusArray[$order_status] : "-";
				
				$notes = (isset($filter->brewingnotes) && !empty($filter->brewingnotes)) ? $filter->brewingnotes : "-";
				$action = '
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink'.$i.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink'.$i.'">
						<a class="dropdown-item" href="javascript:void(0)" onclick="editBatch(this)" data-id="'.$filter->id.'" data-order_status="'.$order_status.'" data-notes="'.$filter->brewingnotes.'">Edit</a>
						</div>
					</div>
				';

				$html .= '<tr>';
				if(!empty($beer_style_id))
					$html .= '<td>'.$i.'</td>';
				else
					$html .= '<td>'.$i.'</td>';
					
				$html .= '<td>'.$filter['name'].'</td>';
				$html .= '<td>'.$filter['beer_style_name'].'</td>';
				$html .= '<td>'.$beer_total_size_text.' </td>';
				$html .= '<td>'.$beer_link.'</td>';
				$html .= '<td>'.$partner_link.'</td>';
				$html .= '<td>'.$reciepe_link.'</td>';
				$html .= '<td>'.$status.'</td>';
				$html .= '<td>'.$notes.'</td>';
				$html .= '<td>'.$action.'</td>';
				$html .= '</tr>';
			}
		}
		return response()->json(array('html'=> $html), 200);
	}

	public function setStatusOfBatch(Request $request)
	{
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = $request->all();
			
			$id 		= (!empty($data) && isset($data['batch_id']) && !empty($data['batch_id'])) ? $data['batch_id'] : false;
			$status_id 	= (!empty($data) && isset($data['status_id']) && !empty($data['status_id'])) ? $data['status_id'] : false;
			$brewingnotes 	= (!empty($data) && isset($data['brewingnotes']) && !empty($data['brewingnotes'])) ? $data['brewingnotes'] : false;
			if(!empty($id) && !empty($status_id))
			{
				$toDay = date("Y-m-d");
				$updateArray['order_status'] = $status_id;				
				if($status_id == 2)
					$updateArray['brew_date'] = $toDay;
				if($status_id == 3)
					$updateArray['pickup_date'] = $toDay;
				if($status_id == 4)
					$updateArray['completed_date'] = $toDay;
					
				$updateArray['brewingnotes'] = $brewingnotes;
				Batchorder::where('id', $id)->update($updateArray);
				
				$BatchData = Batchorder::where('id', $id)->first();
				if(!empty($BatchData) && isset($BatchData->beer_ids) && !empty($BatchData->beer_ids))
				{
					$beerArray['batch_status'] = $status_id;
					Beers::whereIn('id', json_decode($BatchData->beer_ids))->update($beerArray);
				}
				
				return response()->json(array('status'=> true, "msg" => "successfully."), 200);
			}	
			else
			{
				return response()->json(array('status'=> true, "msg" => "something went wrong."), 200);
			}
		}		
	}
	
	public function loadOrderBeerList()
	{
		$beerIDs 	= (isset($_POST['beerIDs']) && !empty($_POST['beerIDs'])) ? explode(",",preg_replace('~["]~','',str_replace("[","", str_replace("]","",$_POST['beerIDs'])))) : false;
		if(!empty($beerIDs))
		{
			$Filterdata = Beers::whereIn("beers.id",$beerIDs)
					->join('beer_style','beer_style.id','=','beers.beer_style_id')
					->join('keg_sizes','keg_sizes.id','=','beers.keg_size_id')
					->join('users','users.id','=','beers.user_id')
					->latest('beers.created_at')
					->get(['beers.qty','beers.order_date','beers.total_cost','beers.beer_name','beer_style.name as beer_style_name','keg_sizes.name as keg_sizes_name','users.name as customer_name']);
					
			$html = '';
			if(!empty($Filterdata->toArray()))
			{
				$i = 0;
				foreach($Filterdata as $index => $filter)
				{
					$i++;	
					
					$html .= '<tr>';
					$html .= '<td>'.$filter['customer_name'].'</td>';
					$html .= '<td>'.$filter['beer_name'].'</td>';
					$html .= '<td>'.$filter['beer_style_name'].'</td>';
					$html .= '<td>'.$filter['qty'].'</td>';
					$html .= '<td>'.$filter['keg_sizes_name'].'</td>';
					$html .= '<td>'.$filter['order_date'].'</td>';
					$html .= '<td>'.$filter['total_cost'].'</td>';
					$html .= '</tr>';
				}
			}
			else
			{
				$html .= '<tr><td colspan="7"> <center>Data Not Found.!</center></td></tr>';
			}
			return response()->json(array('html'=> $html), 200);
		}
	}

	public function orderCompletedBatchesFilter()
	{
		$beer_style_id 	= (isset($_POST['beer_style_id']) && !empty($_POST['beer_style_id'])) ? $_POST['beer_style_id'] : false;
		if(!empty($beer_style_id))
		{
			$where['batchorders.beer_style_id'] = $beer_style_id;							
			$Filterdata = Batchorder::where($where)
						->where('batchorders.order_status',5)
						->join('beer_style','beer_style.id','=','batchorders.beer_style_id')
						->join('brewers_inspiration','brewers_inspiration.id','=','batchorders.brewers_inspiration_id')
						->join('reciepes','reciepes.id','=','batchorders.reciepe_id')
						->join('users','users.id','=','batchorders.partner_id')
						->select('batchorders.*','users.name as partner_name','users.email','users.phone','reciepes.beer_name','beer_style.name as beer_style_name','brewers_inspiration.name as brewers_inspiration_name')
						->get();
		}
		else
		{									
			$Filterdata = Batchorder::where('batchorders.order_status',5)
						->join('beer_style','beer_style.id','=','batchorders.beer_style_id')
						->join('brewers_inspiration','brewers_inspiration.id','=','batchorders.brewers_inspiration_id')
						->join('reciepes','reciepes.id','=','batchorders.reciepe_id')
						->join('users','users.id','=','batchorders.partner_id')
						->select('batchorders.*','users.name as partner_name','users.email','users.phone','reciepes.beer_name','beer_style.name as beer_style_name','brewers_inspiration.name as brewers_inspiration_name')
						->get();
		}
		

		$html = '';
		if(!empty($Filterdata))
		{
			$i = 0;
			foreach($Filterdata as $index => $filter)
			{
				$i++;
								
				$href = "javascript:void(0)";
				if(!empty($filter->reciepe_id))
					$href = route('recipe_view',['id' => $filter->reciepe_id]);
				
				$beerIDs = '';
				if(!empty($filter['beer_ids']))
					$beerIDs = $filter['beer_ids'];
					
				$reciepe_link = '<a class="dropdown-item" style="color:#FF6000;" href="'.$href.'" target="_blank">'.$filter['beer_name'].' View</a>';
				//$partner_link = '<a class="dropdown-item" style="color:#FF6000;" data-name="'.$filter['partner_name'].'" data-email="'.$filter['email'].'" data-phone="'.$filter['phone'].'" href="javascript:void(0)" onclick="getPartnerInfo(this)">'.$filter['partner_name'].'</a>';
				//$beer_link = "<a class='dropdown-item' style='color:#FF6000;' data-beerIDs='".$beerIDs."' href='javascript:void(0)' onclick='getBeerInfo(this)'>Detail</a>";
				
				$partner_link = $filter['partner_name'];						
				$notes = (isset($filter->notes) && !empty($filter->notes)) ? $filter->notes : "-";

				$action = '
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink'.$i.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink'.$i.'">
						  <a class="dropdown-item" href="javascript:void(0)" data-id="'.$filter->id.'" onclick="backToBrewing(this)">Back to Brewing</a>
						</div>
					</div>
				';
				
				$html .= '<tr>';
				if(!empty($beer_style_id))
					$html .= '<td>'.$i.'</td>';
				else
					$html .= '<td>'.$i.'</td>';
					
				$html .= '<td>'.$filter['name'].'</td>';
				$html .= '<td>'.$filter['beer_style_name'].'</td>';
				$html .= '<td>'.$partner_link.'</td>';
				$html .= '<td>'.$filter->updated_at.'</td>';
				$html .= '<td>'.$reciepe_link.'</td>';
				$html .= '<td>'.$notes.'</td>';
				$html .= '<td>'.$action.'</td>';
				$html .= '</tr>';
			}
		}
		return response()->json(array('html'=> $html), 200);
	}

	public function SaveBeerCost()
	{
		$Beer_style_data = Beer_style::all();
		if(!empty($Beer_style_data))
		{
			foreach($Beer_style_data as $beer)
			{
				$array = array();
				
				$array['name'] = $beer->name . " ½ BBL (15.5 GALLONS)";
				$array['beer_style_id'] = $beer->id;
				$array['beer_keg_size_id'] = 1;
				$array['cost'] = 200;
				$array['rate'] = 75;
				$array['created_by'] = Auth::user()->id;
				$array['updated_by'] = Auth::user()->id;
				
				Beer_cost::create($array);
				
				$array['name'] = $beer->name . " 1/6 BBL (5.13 GALLONS)";
				$array['beer_keg_size_id'] = 2;
				$array['cost'] = 100;
				Beer_cost::create($array);
			}
			
			echo 'successfully '; die;
		}
	}

	public function MapBeerCost()
	{
		$Beer_data = Beers::all();
		if(!empty($Beer_data))
		{
			foreach($Beer_data as $beer)
			{
				$Beer_cost_data = Beer_cost::where(array('beer_style_id' => $beer->beer_style_id,'beer_keg_size_id' => $beer->keg_size_id))->get('cost')->first();
				
				$array['unit_cost']  = $Beer_cost_data->cost;
				$array['total_cost'] = $Beer_cost_data->cost * $beer->qty;
				Beers::where("id",$beer->id)->update($array);
			}
			
			echo 'successfully Done'; die;
		}
	}

	public function batchBackToBrewing(Request $request)
	{
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = $request->all();
			$id 		= (!empty($data) && isset($data['batch_id']) && !empty($data['batch_id'])) ? $data['batch_id'] : false;
			if(!empty($id))
			{
				$updateArray['order_status'] = 1;				
				$updateArray['back_date'] 	 = date("Y-m-d");				
				Batchorder::where('id', $id)->update($updateArray);
				
				return response()->json(array('status'=> true, "msg" => "successfully."), 200);
			}	
			else
			{
				return response()->json(array('status'=> true, "msg" => "something went wrong."), 200);
			}
		}		
	}
	
	public function saveNotes(Request $request)
	{
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = $request->all();
			
			$id 	= (!empty($data) && isset($data['user_id']) && !empty($data['user_id'])) ? $data['user_id'] : false;
			$notes 	= (!empty($data) && isset($data['notes']) && !empty($data['notes'])) ? $data['notes'] : false;
			if(!empty($id))
			{					
				$updateArray['notes'] = $notes;
				User::where('id', $id)->update($updateArray);
				
				return response()->json(array('status'=> true, "msg" => "successfully."), 200);
			}	
			else
			{
				return response()->json(array('status'=> true, "msg" => "something went wrong."), 200);
			}
		}		
	}
	
	public function SaveAmount(Request $request)
	{
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = $request->all();
			
			$id 	= (!empty($data) && isset($data['beerID']) && !empty($data['beerID'])) ? $data['beerID'] : false;
			$amount = (!empty($data) && isset($data['amount']) && !empty($data['amount'])) ? $data['amount'] : false;
			if(!empty($id))
			{					
				$updateArray['total_cost'] = $amount;
				Beers::where('id', $id)->update($updateArray);
				
				return response()->json(array('status'=> true, "msg" => "successfully."), 200);
			}	
			else
			{
				return response()->json(array('status'=> true, "msg" => "something went wrong."), 200);
			}
		}		
	}

	public function Messages(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data['user'] = User::join('users_roles','users_roles.user_id','=','users.id')
						->join('roles','roles.id','=','users_roles.role_id')
						->whereIn('roles.slug',array("beer_builder","brewer_partner"))
						->select('users.id','users.name','users.email','users.phone','users.address','users.notes','users.biography','users.img_unique_name','roles.name as role_name')->get();
			
			$data['contactList'] = Message::join('users','messages.revicer_id','=','users.id')
									->where("flag","=","admin")
									->select('users.id','users.name','users.email','users.phone','users.address','users.notes','users.biography','users.img_unique_name','messages.message','messages.is_read')
									->orderBy('messages.created_at','DESC')
									->get();
			
			return view('admin/messages',['data' => $data]);
		}		
    }
	
	public function saveMessage(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$array['sender_id'] = Auth::user()->id;
			$array['revicer_id'] = $request['revicerID']; 
			$array['message'] = $request['message-input'];  
			$array['flag'] = 'admin';  
			Message::create($array);
			
			return response()->json(array('status'=> true, "msg" => "successfully"), 200);
		}		
    }
	
	public function loadMessage(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$id 	= Auth::user()->id;
			$html 	= "";
			$Messages = Message::where("revicer_id",$request['userID'])->orWhere("sender_id",$request['userID'])->get();
			Message::where("flag","beer_builder")->Where(array( "sender_id" => $request['userID'] , "is_read" => 0 ))->update( array("is_read" => 1 ));
			
			if(!empty($Messages))
			{
				foreach($Messages as $key => $message)
				{
					if($message['flag'] == "admin")
					{
						$html .= '<div class="UserChat-Left">
									<img src="assets/logo.png" width="70" height="70" alt=""/>
									<div class="InnerChat-Head">
										<h1>'.$message['message'].'</h1>
										<h2>'.date_format($message['created_at'],"h:i A").'</h2>
									</div>
								</div>';
					}
					else
					{
						$html .= '<div class="UserChat-Right">
									<img src="asset/image/default-image.png" width="70" height="70" alt=""/>
									<div class="InnerReply-Head">
										<h1>'.$message['message'].'</h1>
										<h2>'.date_format($message['created_at'],"h:i A").'</h2>
									</div>
								</div>';
					}
				}
			}
			else
			{
				$html .= '<div class="UserChat-Left emptyMessage">
							<div class="InnerChat-Head">
								<h1>No Messages</h1>
							</div>
						</div>';
			}
			return response()->json(array('status'=> true, "html" => $html ), 200);
		}		
    }
	
	public function loadContact(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$id 	= Auth::user()->id;
			$html 	= "";
			$contactData =  Message::join('users','messages.revicer_id','=','users.id')
									->where("flag","=","admin")
									->select('users.id','users.name','users.email','users.phone','users.address','users.notes','users.biography','users.img_unique_name','messages.message','messages.is_read','messages.flag')
									->orderBy('messages.created_at','DESC')
									->get();
			
			if(!empty($contactData))
			{
				$contactListArray = array();
				foreach($contactData as $key => $contactList)
				{
					
					if(in_array($contactList['id'],$contactListArray))
						continue;
							
					$className = (empty($contactListArray)) ? "active" : "";
					
					$contactListArray[] = $contactList['id'];
					
					$messageData =  Message::where("revicer_id",$contactList['id'])
											->orWhere("sender_id",$contactList['id'])
											->orderBy('messages.id','DESC')
											->limit(1)
											->first();

					$highlight 		= (!empty($messageData) && isset($messageData['is_read']) && empty($messageData['is_read']) && $messageData['flag'] != "admin") ? "highlight" : "";
					$lastMessage 	= (!empty($messageData) && isset($messageData['message']) && !empty($messageData['message'])) ? $messageData['message'] : $contactList->message;
					
					$html 	.= '<li class="'.$className.'" data-id="'.$contactList->id.'" id="li'.$contactList->id.'">
								<a href="javascript:void(0)" onclick="loadMessage('.$contactList->id.')">
									<div class="ChatDropdown-Head">
										<img src="asset/image/default-image.png" width="36" height="36" alt=""/>
										<h1 class="'.$highlight.'">'.$contactList->name.'<br><span>'.substr($lastMessage,0,25).'</span></h1>
									</div>
								</a>
							</li>';
				}
			}
			else
			{
				$html .= '<li class="emptyContactList">
							<a href="#">
								<div class="ChatDropdown-Head">
									<h1><span>Empty Contact List</span></h1>
								</div>
							</a>
						</li>';
			}
			
			return response()->json(array('status'=> true, "html" => $html ), 200);
		}		
    }
	
	public function beersList(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$user_id = Auth::user()->id;
			
			$data = Beers::join('beer_style','beer_style.id','=','beers.beer_style_id')->
			join('keg_sizes','keg_sizes.id','=','beers.keg_size_id')->
			join('brands','brands.id','=','beers.brand_id')->
			leftjoin('ibu','ibu.id','=','beers.ibu_id')->
			leftjoin('abv','abv.id','=','beers.abv_id')->
			//where( 'beers.user_id' , $user_id )->
			select('beers.id as beer_id','beers.beer_name','beers.package_type','beers.notes','beers.beer_status','brands.img_unique_name','beers.unit_cost','beers.status','beer_style.name as beer_style_name','ibu.name as ibu_name','abv.name as abv_name','keg_sizes.name as keg_sizes_name')->orderby('beers.id','DESC')->get();
			//dd($data);
			return view('admin/beersList',['data' => $data]);
		}
    }
	
	public function BeerStatusUpdate(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$input = $request->all();
			if(!empty($input))
			{				
				$BeerIds 	= (isset($input['BeerIds']) && !empty($input['BeerIds'])) ? $input['BeerIds'] : false;
				$StatusIds 	= (isset($input['StatusIds']) && !empty($input['StatusIds'])) ? $input['StatusIds'] : false;
				
				if(!empty($BeerIds))
				{
					$beerStatusArray = array( "1" => "Brew this Beer" , "2" => "Brewery Matching" , "3" => "Brewing in Progress" , "4" => "Ready for Order" );

					for($i=0;$i<count($BeerIds);$i++)
					{
						$BeerData = Beers::where('id', $BeerIds[$i])->first(['user_id','beer_status']);
						$beerStatus = (!empty($BeerData) && isset($BeerData->beer_status)) ? $BeerData->beer_status : false;
						if(!empty($beerStatus) && $beerStatus != $StatusIds[$i])
						{
							Beers::where('id', $BeerIds[$i])->update(array("beer_status" => $StatusIds[$i]));
							
							$para['notification_type_id'] 	= 5; // for 5 Change Beer Status
							$para['sender_id'] 				= Auth::user()->id;
							$para['receiver_id'] 			= (!empty($BeerData) && isset($BeerData->user_id)) ? $BeerData->user_id : false;
							$para['action'] 				= "Admin Change Beer Status";
							$para['action_id'] 				= $BeerIds[$i];
							$para['msg'] 					= "Change Beer Status form ".$beerStatusArray[$beerStatus] ." to ".$beerStatusArray[$StatusIds[$i]];
							
							app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
						}	
					}
				}
			}
		}
		
		return response()->json(array('status'=> true), 200);
    }

	public function ListofBeers(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$user_id = Auth::user()->id;
			
			$data = Beers::join('beer_style','beer_style.id','=','beers.beer_style_id')->
			join('keg_sizes','keg_sizes.id','=','beers.keg_size_id')->
			leftjoin('brands','brands.id','=','beers.brand_id')->
			leftjoin('ibu','ibu.id','=','beers.ibu_id')->
			leftjoin('abv','abv.id','=','beers.abv_id')->
			where( 'beers.user_id' , $user_id )->
			select('beers.id as beer_id','beers.beer_name','beers.package_type','beers.notes','beers.beer_status','brands.img_unique_name','beers.unit_cost','beers.status','beer_style.name as beer_style_name','ibu.name as ibu_name','abv.name as abv_name','keg_sizes.name as keg_sizes_name')->orderby('beers.id','DESC')->get();
			return view('admin/beers',['data' => $data]);
		}
    }    
	
	public function buildbeer(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$user_id = Auth::user()->id;
			$data['brands'] = Brands::where('user_id', $user_id)->get();
			//$data['brands'] = Brands::all();
			$data['beer_styles'] = Beer_style::all();
			$data['brewers_inspiration'] = Brewers_inspiration::all();
			$data['abv'] = Abv::all();
			$data['malt_style'] = malt_style::all();
			$data['ibu'] = Ibu::all();
			$data['hop_profile'] = Hop_profile::all();
			$data['keg_sizes'] = Keg_sizes::all();
			//dd($data);
			return view('admin/build_beer',$data);
		}
    }

    public function beerStore(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = $request->all();
			$like_beer = (isset($data['like_beer']) && !empty($data['like_beer'])) ? $data['like_beer'] : 0;
			$data['like_beer'] 			= $like_beer;
			$data['user_id'] 			= Auth::user()->id;
			$data['order_keg_size_id'] 	= $data['keg_size_id'];
			$total_keg_gallons = $this->total_keg_gallons;
			if(isset($data['keg_size_id']) && !empty($data['keg_size_id']))
			{
				$Keg_data = Keg_sizes::where('id',$data['keg_size_id'])->first();
				if(!empty($Keg_data) && isset($Keg_data->size) && !empty($Keg_data->size))
				{
					$beer_size = $Keg_data->size * $data['qty'];
					if(!empty($beer_size))
					{
						$data['beer_size'] = round($beer_size,2);
						$data['beer_size_rate'] = round($beer_size / $total_keg_gallons,2);
					}
				}
			}
			
			$Beer_cost_data = Beer_cost::where(array('beer_style_id' => $data['beer_style_id'],'beer_keg_size_id' => $data['keg_size_id']))->get('cost')->first();
			$unit_cost  = (isset($Beer_cost_data->cost)) ? $Beer_cost_data->cost : 0;
			$total_cost = $unit_cost * $data['qty'];
			$data['unit_cost'] = $unit_cost;
			$data['total_cost'] = $total_cost;
			$data['beer_status'] = 1;
			
			$beer = Beers::create($data);
			return redirect('meetBeer/'.$beer->id);
			
			//return response()->json(['success' => 'true' , 'data' => $beer]);
		}
    }

    public function meetBeer($id, Request $request)
	{
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			
			$beer_data = Beers::findorfail($id);
			
			if(!empty($beer_data['brand_id']))
				$data['brands'] = Brands::findorfail($beer_data['brand_id']);
			
			$data['beer_style'] = Beer_style::findorfail($beer_data['beer_style_id']);
			$data['brewers_inspiration'] = Brewers_inspiration::findorfail($beer_data['brewers_inspiration_id']); 
			$data['keg_size'] = Keg_sizes::findorfail($beer_data['keg_size_id']);
			
			return view('admin/meet-beer',['beer_data' => $beer_data , 'data' => $data]);
		}
    }
	
	public function EditBeer($id, Request $request)
	{
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			if(!empty($id))
			{
				$user_id = Auth::user()->id;
				$data['brands'] = Brands::where('user_id', $user_id)->get();
				//$data['brands'] = Brands::all();
				$data['beer_styles'] = Beer_style::all();
				$data['brewers_inspiration'] = Brewers_inspiration::all();
				$data['abv'] = Abv::all();
				$data['malt_style'] = malt_style::all();
				$data['ibu'] = Ibu::all();
				$data['hop_profile'] = Hop_profile::all();
				$data['keg_sizes'] = Keg_sizes::all();
				$data['beer'] = Beers::where('id',$id)->first();
				
				return view('admin/edit_beer',$data);
			}
			else
			{
				return redirect('ListofBeers');
			}	
		}		
	}
	
	public function UpdateBeer(Request $request)
	{
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$input = $request->all();
			$id = (isset($input['id']) && !empty($input['id'])) ? $input['id'] : false;
			
			unset($input['_token']);
			unset($input['id']);
			$total_keg_gallons = $this->total_keg_gallons;
			$input['order_keg_size_id'] 	= $input['keg_size_id'];
			if(isset($input['keg_size_id']) && !empty($input['keg_size_id']))
			{
				$Keg_data = Keg_sizes::where('id',$input['keg_size_id'])->first();
				if(!empty($Keg_data) && isset($Keg_data->size) && !empty($Keg_data->size))
				{
					$beer_size = $Keg_data->size * $input['qty'];
					if(!empty($beer_size))
					{
						$input['beer_size'] = round($beer_size,2);
						$input['beer_size_rate'] = round($beer_size / $total_keg_gallons,2);
					}
				}
			}
			
			$Beer_cost_data = Beer_cost::where(array('beer_style_id' => $input['beer_style_id'],'beer_keg_size_id' => $input['keg_size_id']))->get('cost')->first();
			$unit_cost  = (isset($Beer_cost_data->cost)) ? $Beer_cost_data->cost : 0;
			$total_cost = $unit_cost * $input['qty'];
			$input['unit_cost'] = $unit_cost;
			$input['total_cost'] = $total_cost;
			
			Beers::where('id', $id)->update($input);			
		}	

		return redirect('ListofBeers');
	}

	public function BeerDelete(Request $request)
    {
		$para = $request->all();
		$id = (isset($para['id']) && !empty($para['id'])) ? $para['id'] : false;
		if(!empty($id))
		{
			$user_id = Auth::user()->id;
			Beers::where(array('id' => $id, 'user_id' => $user_id))->delete();
			return response()->json(array('status'=> true , 'msg' => "Beer Successfully Deleted."), 200);
		}
		else
		{
			return response()->json(array('status'=> false , 'msg' => "somethig went wrong."), 200);
		}
    }

	public function UpdateBeerStatus(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$input = $request->all();
			if(!empty($input))
			{				
				$BeerIds 	= (isset($input['BeerIds']) && !empty($input['BeerIds'])) ? $input['BeerIds'] : false;
				$StatusIds 	= (isset($input['StatusIds']) && !empty($input['StatusIds'])) ? $input['StatusIds'] : false;
				
				if(!empty($BeerIds))
				{
					$beerStatusArray = array( "1" => "Brew this Beer" , "2" => "Brewery Matching" , "3" => "Brewing in Progress" , "4" => "Ready for Order" );
					
					for($i=0;$i<count($BeerIds);$i++)
					{
						$BeerData = Beers::where('id', $BeerIds[$i])->first(['user_id','beer_status']);
						$beerStatus = (!empty($BeerData) && isset($BeerData->beer_status)) ? $BeerData->beer_status : false;
						if(!empty($beerStatus) && $beerStatus != $StatusIds[$i])
						{
							Beers::where('id', $BeerIds[$i])->update(array("beer_status" => $StatusIds[$i]));
							
							$para['notification_type_id'] 	= 5; // for 5 Change Beer Status
							$para['sender_id'] 				= Auth::user()->id;
							$para['receiver_id'] 			= "-1";
							$para['action'] 				= "Admin Beer Builder Change Beer Status";
							$para['action_id'] 				= $BeerIds[$i];
							$para['msg'] 					= "Change Beer Status form ".$beerStatusArray[$beerStatus] ." to ".$beerStatusArray[$StatusIds[$i]];
							
							app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
						}							
					}
				}
			}
		}
		
		return response()->json(array('status'=> true), 200);
    }

	public function Orderbeer(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$user_id = Auth::user()->id;
			
			$data = Beers::join('beer_style','beer_style.id','=','beers.beer_style_id')->
			join('keg_sizes','keg_sizes.id','=','beers.keg_size_id')->
			join('brands','brands.id','=','beers.brand_id')->
			leftjoin('ibu','ibu.id','=','beers.ibu_id')->
			leftjoin('abv','abv.id','=','beers.abv_id')->
			where( 'beers.user_id' , $user_id )->
			where( 'beers.batch_status' , ">=" ,4 )->
			select('beers.id as beer_id','beers.beer_name','beers.qty','beers.package_type','beers.notes','brands.img_unique_name','beers.unit_cost','beers.status','beer_style.name as beer_style_name','ibu.name as ibu_name','abv.name as abv_name','keg_sizes.name as keg_sizes_name')->orderby('beers.id','DESC')->get();
			//dd($data);
			return view('admin/beers_order',['data' => $data]);
		}
    }
	
	public function BeerBrew(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$input = $request->all();
			if(!empty($input))
			{				
				$BeerId = (isset($input['BeerId']) && !empty($input['BeerId'])) ? $input['BeerId'] : false;
				$Qty 	= (isset($input['qty']) && !empty($input['qty'])) ? $input['qty'] : false;
				
				if(!empty($BeerId))
				{
					$data = Beers::where('id',$BeerId)->first();
					$total_keg_gallons = $this->total_keg_gallons;
					if(isset($data['keg_size_id']) && !empty($data['keg_size_id']))
					{
						$Keg_data = Keg_sizes::where('id',$data['keg_size_id'])->first();
						if(!empty($Keg_data) && isset($Keg_data->size) && !empty($Keg_data->size))
						{
							$beer_size = $Keg_data->size * $Qty;
							if(!empty($beer_size))
							{
								$array['beer_size'] = round($beer_size,2);
								$array['beer_size_rate'] = round($beer_size / $total_keg_gallons,2);
							}
						}
					}

					$Beer_cost_data = Beer_cost::where(array('beer_style_id' => $data['beer_style_id'],'beer_keg_size_id' => $data['keg_size_id']))->get('cost')->first();
					$unit_cost  = (isset($Beer_cost_data->cost)) ? $Beer_cost_data->cost : 0;
					$total_cost = $unit_cost * $Qty;

					$array['qty'] 			= $Qty;
					$array['unit_cost'] 	= $unit_cost;
					$array['total_cost'] 	= $total_cost;
					$array['order_date'] 	= date("Y-m-d");
					$array['status'] 		= 1;
					Beers::where('id', $BeerId)->update($array);

					$para['notification_type_id'] 	= 1; // for 1 brew this beer
					$para['sender_id'] 				= Auth::user()->id;
					$para['receiver_id'] 			= "-1";
					$para['action'] 				= "Admin Brew this Beer";
					$para['action_id'] 				= $BeerId;
					
					app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
				}
			}
		}
		
		return response()->json(array('status'=> true), 200);
    }

	public function stopBeerBrew($id)
    {
		if(!empty($id))
		{
			Beers::where('id', $id)->update(array("order_date" => date("Y-m-d") , "status" => 0));

			$para['notification_type_id'] 	= 1; // for 1 brew this beer
			$para['sender_id'] 				= Auth::user()->id;
			$para['receiver_id'] 			= "-1";
			$para['action'] 				= "Stop Brewing this Beer";
			$para['action_id'] 				= $id;
			
			app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
		}

		return redirect()->route('ListofBeers');
    }

	public function SendOffer(Request $request)
	{
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$data = $request->all();
			$batch_id 	= (!empty($data) && isset($data['batch_id']) && !empty($data['batch_id'])) ? $data['batch_id'] : false;
			$PartnerIDs = (!empty($data) && isset($data['brewer_partner_id']) && !empty($data['brewer_partner_id'])) ? $data['brewer_partner_id'] : false;
			if(!empty($batch_id) && !empty($PartnerIDs))
			{				
				$exculde_Ids = array();
				$getLogIDs = Offerlog::where('batch_id',$batch_id)->whereNotIn('partner_id',$PartnerIDs)->select(DB::raw("group_concat(id) as PriIds"))->first()->toArray();
				if(!empty($getLogIDs) && isset($getLogIDs['PriIds']))
					$exculde_Ids = explode(",",$getLogIDs['PriIds']);
			
				if(!empty($exculde_Ids))
				{
					Offerlog::whereIn('id',$exculde_Ids)->delete();
				}
				
				foreach($PartnerIDs as $key => $PartnerID)
				{
					$array = array();					
					$check = Offerlog::where('batch_id',$batch_id)->where('partner_id',$PartnerID)->exists();
					if(empty($check))
					{
						$array['batch_id'] 		= $batch_id;
						$array['partner_id'] 	= $PartnerID;
						Offerlog::create($array);	

						$para['notification_type_id'] 	= 6; // for 6 Send Offer
						$para['sender_id'] 				= Auth::user()->id;
						$para['receiver_id'] 			= $PartnerID;
						$para['action'] 				= "Send Brewer Offer";
						$para['action_id'] 				= $batch_id;
						
						app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
			
					}
				}
				
				Batchorder::where('id', $batch_id)->update(array( "offer_status" => 1 ));
				
				return response()->json(array('status'=> true, "msg" => "successfully."), 200);
			}	
			else
			{
				return response()->json(array('status'=> true, "msg" => "something went wrong."), 200);
			}
		}		
	}
	
	public function listbrand(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$user_id = Auth::user()->id;
			$data = Brands::where( 'brands.user_id' , $user_id )->get();
			//dd($data);
			return view('admin/my-brand',['data' => $data]);
		}
    }
	
	public function buildbrand(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			return view('admin/build_brand_beer');
		}		
    }
	
	public function storebrand(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			//dd(Auth::user()->id);die;
			$data = $request->all();
			
			$image_original_name  = '';
			$image_unique_name = '';
			if($request->hasFile('image'))
			{
				$fileobj				= $request->file('image');
				$image_original_name 	= $fileobj->getClientOriginalName('image');
				$file_extension_name 	= $fileobj->getClientOriginalExtension('image');
				$image_unique_name 		= time().rand(1000,9999).'.'.$file_extension_name;
				$destinationPath		= public_path('/uploads/');
				$fileobj->move($destinationPath,$image_unique_name);
			}

			$data['img_actual_name'] 	= $image_original_name;
			$data['img_unique_name'] 	= $image_unique_name;
			
			$data['user_id'] = Auth::user()->id;
			Brands::create($data);
			return redirect()->route('listbrand');
		}
    }

	public function DeleteBrand(Request $request)
    {
		$para = $request->all();
		$id = (isset($para['id']) && !empty($para['id'])) ? $para['id'] : false;
		if(!empty($id))
		{
			$user_id = Auth::user()->id;
			Brands::where(array('id' => $id, 'user_id' => $user_id))->delete();
			//Beers::where(array('brand_id' => $id, 'user_id' => $user_id))->delete();
			return response()->json(array('status'=> true , 'msg' => "Brand Successfully Deleted."), 200);
		}
		else
		{
			return response()->json(array('status'=> false , 'msg' => "somethig went wrong."), 200);
		}
    }
}
