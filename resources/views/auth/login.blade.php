@extends('layouts.app_old1')

@section('content')
<div class="container-fluid">
            <div class="row no-gutter">
              <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"><img src="{{ URL::asset('assets/LogoWhite.png')}}" alt=""></div>
              <div class="col-md-8 col-lg-6">
                <div class="SignUp" style="padding: 21px;">
                  <span style="float: right;">Don't have an account? <a href="{{ route('register') }}">SignUp</a></span>
              </div>
                <div class="login d-flex align-items-center py-5">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-9 col-lg-8 mx-auto">
                        <h3 class="login-heading mb-4">Login</h3>
                        <form method="POST" action="{{ route('login') }}">
                        @csrf
                          <div class="form-label-group">
                            <input type="email" id="email" name="email" class="form-control" placeholder="Email Address" required autofocus>
                            <label for="email">Email address</label>
                            
                          </div>
          
                          <div class="form-label-group">
                            <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
                            <label for="password">Password</label>
                            
                          </div>
          
                          <div class="custom-control custom-checkbox mb-3">
                            <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="remember">{{ __('Remember Me') }}</label>
                              @if (Route::has('password.request'))
                                <a class="small" href="{{ route('password.request') }}">{{ __('Forgot Password?') }}</a>
                              @endif
                          </div>
                          <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">{{ __('Login') }}</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
@endsection
