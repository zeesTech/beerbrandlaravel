@extends('layouts.app')

@section('content')
<style>

#buttonStyle{
  padding: 10px;
  color: white;
  background-color: #FF6000;
  border: 1px solid #FF6000;
  border-radius: 5px;
  cursor: pointer;
}

#buttonStyle:hover {
  background-color: #FF6000;
  border: 1px solid #FF6000;
}

#errormsg {
  margin-left: 10px;
  font-family: sans-serif;
  color: #aaa;
}

</style>
<div class="container-fluid">
  <!-- Page Heading -->
 <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Brands</h1>
  </div>

  <!-- Content Row -->

  <div class="row">

    <!-- Area Chart -->
    <div class="col-xl-8 col-lg-7">
      <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 class="m-0 font-weight-bold text-primary">Add Another Brand</h6>
          <div class="dropdown no-arrow">
            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
              <div class="dropdown-header">Dropdown Header:</div>
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">
          <form action="{{ route('brand.store') }}" method="post" enctype="multipart/form-data">
                  @csrf
                    <div class="form-group">
                      <!-- <label for="YourBusinessName">Your Business Name</label> -->
                      <input type="text" id="YourBusinessName" name="brand_name" class="form-control" placeholder="Your Business Name" required autofocus>
                      
                    </div>

					<div class="form-group">
						<input type="file" id="image"  onchange="selectFile();" accept="image/*" name = "image" hidden="hidden" onchange="allowonlyImg(this)" />
						<button type="button" id="buttonStyle" onclick="FileUpload();">Upload Your Brand/Restaurant/Bar Logo</button>
						<span id="errormsg">No file chosen, yet.</span>
					</div>
					
                    <div class="form-group">
                      <!-- <label for="StreetAddress">Street Address</label> -->
                      <input type="text" id="StreetAddress" name="address" class="form-control" placeholder="Street Address" required autofocus>
                    </div>
                    
                    <div class="form-group">
                      <!-- <label for="City">City</label> -->
                      <input type="text" id="City" name="city" class="form-control" placeholder="City" required autofocus>
                      
                    </div>


                    <div class="form-group" style="display: inline-flex;width: 100%;">
                      <input type="text" id="State" name="state" class="form-control" placeholder="State" required autofocus style="line-height: 2;   margin-right: 10px;">
                      

                      <input type="text" id="ZipCode" name="zipcode" class="form-control" placeholder="Zip Code" required autofocus >
                      
                    </div>
                    
					<div class="form-group">
                      <input type="text" id="location" name="location" class="form-control" placeholder="Company Location" autofocus>
                    </div>
					
                    <div class="form-group">
                      <!-- <label for="contactname">Contact Name</label> -->
                      <input type="text" id="contactname" name="contact_name" class="form-control" placeholder="Contact Name" required autofocus>
                      
                    </div>
                    <div class="form-group">
                      <input type="text" id="Email Address" name="email" class="form-control" placeholder="Email Address" required autofocus>
                      <!-- <label for="Email Address">Email Address</label> -->
                    </div>
                    <div class="form-group">
                      <!-- <label for="Phone Number">Phone Number</label> -->
                      <input type="text" id="Phone Number" name="number" class="form-control" placeholder="Phone Number" required autofocus>
                      
                    </div>

                    <div class="form-group hide">
                      <input type="text" id="Liquor License Number ( If any )" name="license" class="form-control" placeholder="Liquor License Number ( If any )" autofocus>
                    </div>

                    <button class="btn btn-lg btn-bbb-notActive btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">SUBMIT</button>
                  </form>
            </div>
      </div>
    </div>
  </div>

  </div>
@endsection

<script>
function FileUpload()
{
	document.getElementById('image').click();	
}

function selectFile()
{
	 if (document.getElementById('image').value) {
		 document.getElementById('errormsg').innerHTML = document.getElementById('image').value.match(
		      /[\/\\]([\w\d\s\.\-\(\)]+)$/
		    )[1];
		  } else {
			  document.getElementById('errormsg').innerHTML = "No file chosen, yet.";
		  }
}
</script>
