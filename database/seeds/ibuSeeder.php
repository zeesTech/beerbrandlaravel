<?php

use Illuminate\Database\Seeder;

class ibuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ibu')->insert([
            ['name' => 	'LOW'],
            ['name' => 	'MEDIUM'],
            ['name' => 	'HIGH']
        ]);
    }
}
