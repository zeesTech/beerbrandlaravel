<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsBatchorder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('batchorders', function (Blueprint $table) {
            $table->float('keg_1_2_size_beer_cost', 11, 2)->nullable()->after('reciepe_id');
            $table->float('keg_1_6_size_beer_cost', 11, 2)->nullable()->after('keg_1_2_size_beer_cost');
            $table->float('total_cost', 11, 2)->nullable()->after('keg_1_6_size_beer_cost');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('batchorders', function (Blueprint $table) {
            //
        });
    }
}
