<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->Integer('beer_style_id');
            $table->Integer('brewers_inspiration_id');
            $table->Integer('abv_id');
            $table->Integer('malt_style_id');
            $table->Integer('ibu_id');
            $table->Integer('hop_id');
            $table->Integer('keg_size_id');
            $table->Integer('beer_color_id')->nullable();
            $table->string('beer_color',100)->nullable();
            $table->string('kegs_sell_week_in_covid',50)->nullable();
            $table->string('kegs_sell_week',50)->nullable();
            $table->Integer('interested_canning_beer')->default(0);
            $table->string('beer_cases',100)->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beers');
    }
}
