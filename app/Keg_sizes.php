<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keg_sizes extends Model
{
    protected $table = "keg_sizes";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
