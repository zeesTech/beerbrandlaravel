<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brewers_inspiration extends Model
{
    protected $table = "brewers_inspiration";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
