<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBatchordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batchorders', function (Blueprint $table) {
            $table->id();
			$table->unsignedInteger('user_id');
            $table->Integer('beer_style_id')->nullable();
            $table->json('beer_ids')->nullable();
            $table->Integer('batch_size')->nullable();
            $table->Integer('partner_id')->nullable();
            $table->Integer('reciepe_id')->nullable();
			$table->Integer('order_status')->default(0);
			$table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batchorders');
    }
}
