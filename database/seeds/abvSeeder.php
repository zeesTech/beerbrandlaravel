<?php

use Illuminate\Database\Seeder;

class abvSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('abv')->insert([
            ['name' => 	'LOW'],
            ['name' => 	'MEDIUM'],
            ['name' => 	'HIGH']
        ]);
    }
}
