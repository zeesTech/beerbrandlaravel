<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToBatchorders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('batchorders', function (Blueprint $table) {
            $table->string('name')->nullable()->after('id');
            $table->Integer('brewers_inspiration_id')->nullable()->after('beer_style_id');
			$table->string('batch_notes')->nullable()->after('order_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('batchorders', function (Blueprint $table) {
            //
        });
    }
}
