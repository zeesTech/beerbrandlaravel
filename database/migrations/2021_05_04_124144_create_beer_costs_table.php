<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeerCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beer_costs', function (Blueprint $table) {
            $table->id();
			$table->string('name')->nullable();
			$table->integer('beer_style_id')->nullable();
			$table->integer('beer_keg_size_id')->nullable();
			$table->integer('cost')->nullable();
			$table->float('rate', 5, 2)->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beer_costs');
    }
}
