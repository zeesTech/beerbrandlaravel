@extends('layouts.app')

@section('content')
<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    #BeerOrderTbl thead {
        background-color: #FF6000;
        color: white;
    }

    #BeerOrderTbl thead tr {
        height: 60px;
    }
    #BeerOrderTbl .even { background-color: white }
    #BeerOrderTbl .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    #BeerOrderTbl tbody tr {
        height: 60px;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Order History</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Order History</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="BeerOrderTbl" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Sr #</th>
            <th>Order Date</th>
            <th>Quantity</th>
            <th>Beer Name</th>
            <th>Beer Style</th>
            <th>Keg Size</th>
            <th>Cost</th>
            <th>Total Cost</th>
          </tr>
        </thead>
       
        <tbody>
			<?php	
				if(!empty($data)):
				$i = 0;
				foreach($data as $dt):
					$order_date = "-";
					if(!empty($dt->order_date))
						$order_date = date("F d,Y", strtotime($dt->order_date));
						
					$i++;
			?>
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$order_date}}</td>
                        <td>{{$dt->qty}}</td>
                        <td>{{$dt->beer_name}}</td>
                        <td>{{$dt->beer_style_name}}</td>
                        <td>{{$dt->keg_sizes_name}}</td>
                        <td>${{number_format($dt->unit_cost)}}</td>
                        <td>${{number_format($dt->total_cost)}}</td>
                    </tr>
			<?php
					endforeach;
				endif;
			?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>

<script>
    $(document).ready( function () {
        $('#BeerOrderTbl').DataTable({
            searching: false, 
			info: false,
            aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ -1 ]
                            }
            ]
        });
    } );
</script>
@endsection
