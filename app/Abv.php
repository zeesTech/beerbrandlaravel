<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Abv extends Model
{
    protected $table = "abv";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
