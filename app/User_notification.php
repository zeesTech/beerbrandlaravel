<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_notification extends Model
{
	 protected $table = "user_notification";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'notification_type_id', 'sender_id','receiver_id','action','action_id','msg'
    ];
}
