<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User_notification;
use App\Notification_type;
use Auth;
use DB;

class NotificationController extends Controller
{
    function GenerateNotification($para)
	{
		$notification_type_id 	= (!empty($para) && isset($para['notification_type_id']) && !empty($para['notification_type_id'])) ? $para['notification_type_id'] : 0;
		$action				 	= (!empty($para) && isset($para['action']) && !empty($para['action'])) ? $para['action'] : 0;
		$action_id				= (!empty($para) && isset($para['action_id']) && !empty($para['action_id'])) ? $para['action_id'] : 0;
		$sender_id				= (!empty($para) && isset($para['sender_id']) && !empty($para['sender_id'])) ? $para['sender_id'] : 0;
		$receiver_id			= (!empty($para) && isset($para['receiver_id']) && !empty($para['receiver_id'])) ? $para['receiver_id'] : 0;
		$msg					= (!empty($para) && isset($para['msg']) && !empty($para['msg'])) ? $para['msg'] : 0;
		
		if(!empty($notification_type_id) && !empty($sender_id) && !empty($receiver_id))
		{
			if(empty($msg))
			{
				$Notification_typeData 	= Notification_type::where(array('id' => $notification_type_id , 'isStatus' => 1 ))->first();
				$msg					= (!empty($Notification_typeData) && isset($Notification_typeData->default_msg) && !empty($Notification_typeData->default_msg)) ? $Notification_typeData->default_msg : "";
			}
			
			$data['notification_type_id'] 	= $notification_type_id; 
			$data['user_id'] 				= $sender_id;
			$data['sender_id'] 				= $sender_id;
			$data['receiver_id'] 			= $receiver_id;
			$data['action'] 				= $action;
			$data['action_id'] 				= $action_id;
			$data['msg'] 					= $msg;
			User_notification::create($data);
		}
	}

	function GetNotification(Request $request)
	{
		$receiver_id = Auth::user()->id;
		
		$receiver_ids[] = $receiver_id;
		
		$html = '';
		$limit = 100;
		
		if(!empty($receiver_id))
		{
			$user = $request->user();
			$adminCheck = false;
			$beerBuilderCheck = false;
			$brewerPartnerCheck = false;
			if($user->hasRole('administrator'))
			{
				$receiver_ids[] = '-1';
				$adminCheck = true;
				//$linkArray = array( "brew_beer" => "orders_unassigned" , "brew_batch" => "order_batches_brewing" , "attach_recipe" => "order_being_built" , "beer_in_process" => "order_batches_brewing");
				$linkArray = array( 1 => "orders_unassigned", 2 => "order_batches_brewing", 3 => "order_being_built", 4 => "order_batches_brewing", 5 => "beersList" , 6 => "order_being_built" , 7 => "order_batches_brewing" );
			}else if($user->hasRole('beer_builder')){
				$beerBuilderCheck = true;
			}else if($user->hasRole('brewer_partner')){
				$brewerPartnerCheck = true;
			}	
			
			$data = User_notification::whereIn('user_notification.receiver_id', $receiver_ids)
											->join('notification_type','notification_type.id','=','user_notification.notification_type_id')
											->join('users','users.id','=','user_notification.sender_id')
											->select('user_notification.*','notification_type.name','notification_type.flag','users.name as senderName')
											->orderBy('user_notification.id', 'DESC')
											->limit($limit)
											->get();						
			$i = 0;
			$unreadCounter = 0;
			foreach($data as $index => $row)
			{
				$i++;
				$isRead = (isset($row['isRead']) && !empty($row['isRead'])) ? $row['isRead'] : false;
				
				$attri = "";
				$link = "-";
				if($adminCheck) 
				{
					$link = (!empty($linkArray) && isset($linkArray[$row['notification_type_id']])) ? $linkArray[$row['notification_type_id']] : "-" ;
				}
				else if($beerBuilderCheck)
				{
					$link = "beers";
				}
				else if($brewerPartnerCheck)
				{
					if($row['notification_type_id'] == 6)
						$link = "offers";
					else
						$link = "order";
				}
					
				$attri = " data-link = '".$link."'";
				$class = "";
				if(empty($isRead))
				{
					$attri .= "data-id='".$row['id']."'";
					$class = "unmark";
				}
				
				$msg = $row['senderName'] . " " . $row['msg'];

				$html .= '
					<a class="dropdown-item d-flex align-items-center unmark" href="javascript:void(0)" '.$attri.' >
					  <div>
						<div class="small text-gray-500">'.date("F d, Y",strtotime($row['created_at'])).'</div>
					';
					
				if(empty($isRead))
				{
					$html .= '<span id="msgSpan'.$row['id'].'" class="font-weight-bold">'.$msg.'</span>';
					$unreadCounter++;
				}
				else
					$html .= $row['msg'];
				
				$html .= '
					  </div>
					</a>';
				
			}
		}
		
		return response()->json(array('status' => true, 'unreadCounter' => $unreadCounter , 'html'=> $html), 200);
	}
	
	function markRead()
	{
		$id 	= (isset($_POST['id']) && !empty($_POST['id'])) ? $_POST['id'] : false;
		if(!empty($id))
		{
			$data['isRead'] 	= 1;
			User_notification::where('id', $id)->update($data);
			
			return response()->json(array('status' => true), 200);
		}
		else
			return response()->json(array('status' => false), 200);
	}
}
