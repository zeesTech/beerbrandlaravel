<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brands;
use App\Beers;
use App\Beer_style;
use App\Brewers_inspiration;
use App\Abv;
use App\Malt_style;
use App\Ibu;
use App\Hop_profile;
use App\Beer_cost;
use App\Keg_sizes;
use App\User_notification;
use App\Notification_type;
use App\User;
use App\Message;
use Auth;

class BeersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	public function __construct()
    {
        $this->total_keg_gallons = 31.5;
    }

    public function index(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			$user_id = Auth::user()->id;
			
			$data = Beers::join('beer_style','beer_style.id','=','beers.beer_style_id')->
			join('keg_sizes','keg_sizes.id','=','beers.keg_size_id')->
			leftjoin('brands','brands.id','=','beers.brand_id')->
			leftjoin('ibu','ibu.id','=','beers.ibu_id')->
			leftjoin('abv','abv.id','=','beers.abv_id')->
			where( 'beers.user_id' , $user_id )->
			select('beers.id as beer_id','beers.beer_name','beers.package_type','beers.notes','beers.beer_status','brands.img_unique_name','beers.unit_cost','beers.status','beer_style.name as beer_style_name','ibu.name as ibu_name','abv.name as abv_name','keg_sizes.name as keg_sizes_name')->orderby('beers.id','DESC')->get();
			//dd($data);
			return view('beer_builder/beers',['data' => $data]);
		}
    }    
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			$user_id = Auth::user()->id;
			$data['brands'] = Brands::where('user_id', $user_id)->get();
			$data['beer_styles'] = Beer_style::all();
			$data['brewers_inspiration'] = Brewers_inspiration::all();
			$data['abv'] = Abv::all();
			$data['malt_style'] = malt_style::all();
			$data['ibu'] = Ibu::all();
			$data['hop_profile'] = Hop_profile::all();
			$data['keg_sizes'] = Keg_sizes::all();
			//dd($data);
			return view('beer_builder/build_beer',$data);
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			$data = $request->all();
			$like_beer = (isset($data['like_beer']) && !empty($data['like_beer'])) ? $data['like_beer'] : 0;
			$data['like_beer'] 			= $like_beer;
			$data['user_id'] 			= Auth::user()->id;
			$data['order_keg_size_id'] 	= $data['keg_size_id'];
			$total_keg_gallons = $this->total_keg_gallons;
			if(isset($data['keg_size_id']) && !empty($data['keg_size_id']))
			{
				$Keg_data = Keg_sizes::where('id',$data['keg_size_id'])->first();
				if(!empty($Keg_data) && isset($Keg_data->size) && !empty($Keg_data->size))
				{
					$beer_size = $Keg_data->size * $data['qty'];
					if(!empty($beer_size))
					{
						$data['beer_size'] = round($beer_size,2);
						$data['beer_size_rate'] = round($beer_size / $total_keg_gallons,2);
					}
				}
			}
			
			$Beer_cost_data = Beer_cost::where(array('beer_style_id' => $data['beer_style_id'],'beer_keg_size_id' => $data['keg_size_id']))->get('cost')->first();
			$unit_cost  = (isset($Beer_cost_data->cost)) ? $Beer_cost_data->cost : 0;
			$total_cost = $unit_cost * $data['qty'];
			$data['unit_cost'] = $unit_cost;
			$data['total_cost'] = $total_cost;
			$data['beer_status'] = 1;
			
			$beer = Beers::create($data);
			return redirect('meet_beer/'.$beer->id);
			
			//return response()->json(['success' => 'true' , 'data' => $beer]);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			$data = $request->all();
			$data['user_id'] = 11;
			$data['beer_color_id'] = "-99";
			
			//dd($data);die;
			//Beers::create($data);
			$request->session()->put('beer_data', $data);
			return redirect()->route('beer.meet_beer');
		}
    }

	
	public function EditBeer($id, Request $request)
	{
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			if(!empty($id))
			{
				$user_id = Auth::user()->id;
				$data['brands'] = Brands::where('user_id', $user_id)->get();
				$data['beer_styles'] = Beer_style::all();
				$data['brewers_inspiration'] = Brewers_inspiration::all();
				$data['abv'] = Abv::all();
				$data['malt_style'] = malt_style::all();
				$data['ibu'] = Ibu::all();
				$data['hop_profile'] = Hop_profile::all();
				$data['keg_sizes'] = Keg_sizes::all();
				$data['beer'] = Beers::where('id',$id)->first();
				
				return view('beer_builder/edit_beer',$data);
			}
			else
			{
				return redirect('beers');
			}	
		}		
	}
	
	public function UpdateBeer(Request $request)
	{
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			$input = $request->all();
			$id = (isset($input['id']) && !empty($input['id'])) ? $input['id'] : false;
			
			unset($input['_token']);
			unset($input['id']);
			$total_keg_gallons = $this->total_keg_gallons;
			$input['order_keg_size_id'] 	= $input['keg_size_id'];
			if(isset($input['keg_size_id']) && !empty($input['keg_size_id']))
			{
				$Keg_data = Keg_sizes::where('id',$input['keg_size_id'])->first();
				if(!empty($Keg_data) && isset($Keg_data->size) && !empty($Keg_data->size))
				{
					$beer_size = $Keg_data->size * $input['qty'];
					if(!empty($beer_size))
					{
						$input['beer_size'] = round($beer_size,2);
						$input['beer_size_rate'] = round($beer_size / $total_keg_gallons,2);
					}
				}
			}
			
			$Beer_cost_data = Beer_cost::where(array('beer_style_id' => $input['beer_style_id'],'beer_keg_size_id' => $input['keg_size_id']))->get('cost')->first();
			$unit_cost  = (isset($Beer_cost_data->cost)) ? $Beer_cost_data->cost : 0;
			$total_cost = $unit_cost * $input['qty'];
			$input['unit_cost'] = $unit_cost;
			$input['total_cost'] = $total_cost;
			
			Beers::where('id', $id)->update($input);			
		}	

		return redirect('beers');
	}
	

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function meetBeer($id, Request $request){
		
		//echo $id;
		//die;
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			
			$beer_data = Beers::findorfail($id);
			
			if(!empty($beer_data['brand_id']))
				$data['brands'] = Brands::findorfail($beer_data['brand_id']);
			
			$data['beer_style'] = Beer_style::findorfail($beer_data['beer_style_id']);
			$data['brewers_inspiration'] = Brewers_inspiration::findorfail($beer_data['brewers_inspiration_id']);
			//$data['abv'] = Abv::findorfail($beer_data['abv_id']);
			//$data['malt_style'] = malt_style::findorfail($beer_data['malt_style_id']);
			//$data['ibu'] = Ibu::findorfail($beer_data['ibu_id']); 
			//$data['hop_profile'] = Hop_profile::findorfail($beer_data['hop_id']); 
			$data['keg_size'] = Keg_sizes::findorfail($beer_data['keg_size_id']);
			
			//dd($data);
			//dd($beer_data);
			return view('beer_builder/meet-beer',['beer_data' => $beer_data , 'data' => $data]);
		}
    }
	
	public function deleteBeer($id)
    {
		if(!empty($id))
		{
			echo $id; die;
			$user_id = Auth::user()->id;
			Beers::where(array('id' => $id, 'user_id' => $user_id))->delete();
		}

		return redirect()->route('beer.index');
    }
	
	public function BeerDelete(Request $request)
    {
		$para = $request->all();
		$id = (isset($para['id']) && !empty($para['id'])) ? $para['id'] : false;
		if(!empty($id))
		{
			$user_id = Auth::user()->id;
			Beers::where(array('id' => $id, 'user_id' => $user_id))->delete();
			return response()->json(array('status'=> true , 'msg' => "Beer Successfully Deleted."), 200);
		}
		else
		{
			return response()->json(array('status'=> false , 'msg' => "somethig went wrong."), 200);
		}
    }
	
	public function Brew_Beer($id)
    {
		if(!empty($id))
		{
			Beers::where('id', $id)->update(array("order_date" => date("Y-m-d") , "status" => 1));

			$para['notification_type_id'] 	= 1; // for 1 brew this beer
			$para['sender_id'] 				= Auth::user()->id;
			$para['receiver_id'] 			= "-1";
			$para['action'] 				= "Brew this Beer";
			$para['action_id'] 				= $id;
			
			app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
			
		}

		return redirect()->route('beerOrder');
    }
	
	public function BrewBeer(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			$input = $request->all();
			if(!empty($input))
			{				
				$BeerId = (isset($input['BeerId']) && !empty($input['BeerId'])) ? $input['BeerId'] : false;
				$Qty 	= (isset($input['qty']) && !empty($input['qty'])) ? $input['qty'] : false;
				
				if(!empty($BeerId))
				{
					$data = Beers::where('id',$BeerId)->first();
					$total_keg_gallons = $this->total_keg_gallons;
					if(isset($data['keg_size_id']) && !empty($data['keg_size_id']))
					{
						$Keg_data = Keg_sizes::where('id',$data['keg_size_id'])->first();
						if(!empty($Keg_data) && isset($Keg_data->size) && !empty($Keg_data->size))
						{
							$beer_size = $Keg_data->size * $Qty;
							if(!empty($beer_size))
							{
								$array['beer_size'] = round($beer_size,2);
								$array['beer_size_rate'] = round($beer_size / $total_keg_gallons,2);
							}
						}
					}

					$Beer_cost_data = Beer_cost::where(array('beer_style_id' => $data['beer_style_id'],'beer_keg_size_id' => $data['keg_size_id']))->get('cost')->first();
					$unit_cost  = (isset($Beer_cost_data->cost)) ? $Beer_cost_data->cost : 0;
					$total_cost = $unit_cost * $Qty;

					$array['qty'] 			= $Qty;
					$array['unit_cost'] 	= $unit_cost;
					$array['total_cost'] 	= $total_cost;
					$array['order_date'] 	= date("Y-m-d");
					$array['status'] 		= 1;
					Beers::where('id', $BeerId)->update($array);

					$para['notification_type_id'] 	= 1; // for 1 brew this beer
					$para['sender_id'] 				= Auth::user()->id;
					$para['receiver_id'] 			= "-1";
					$para['action'] 				= "Brew this Beer";
					$para['action_id'] 				= $BeerId;
					
					app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
				}
			}
		}
		
		return response()->json(array('status'=> true), 200);
    }
	
	public function stopBrewBeer($id)
    {
		if(!empty($id))
		{
			Beers::where('id', $id)->update(array("order_date" => date("Y-m-d") , "status" => 0));

			$para['notification_type_id'] 	= 1; // for 1 brew this beer
			$para['sender_id'] 				= Auth::user()->id;
			$para['receiver_id'] 			= "-1";
			$para['action'] 				= "Stop Brewing this Beer";
			$para['action_id'] 				= $id;
			
			app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
			
		}

		return redirect()->route('beer.index');
    }
	
	public function beerOrder(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			$user_id = Auth::user()->id;
			
			$data = Beers::join('beer_style','beer_style.id','=','beers.beer_style_id')->
			join('keg_sizes','keg_sizes.id','=','beers.keg_size_id')->
			join('brands','brands.id','=','beers.brand_id')->
			leftjoin('ibu','ibu.id','=','beers.ibu_id')->
			leftjoin('abv','abv.id','=','beers.abv_id')->
			leftjoin('users','users.id','=','beers.user_id')->
			where( 'beers.user_id' , $user_id )->
			where( 'beers.batch_status' , ">=" ,4 )->
			select('beers.id as beer_id','beers.beer_name','beers.qty','beers.package_type','beers.notes','brands.img_unique_name','beers.unit_cost','beers.status','beer_style.name as beer_style_name','ibu.name as ibu_name','abv.name as abv_name','keg_sizes.name as keg_sizes_name','users.name as customer_name')->orderby('beers.id','DESC')->get();
			//dd($data);
			return view('beer_builder/beers_order',['data' => $data]);
		}
    }
	
	public function BeerUpdateStatus(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			$input = $request->all();
			if(!empty($input))
			{				
				$BeerIds 	= (isset($input['BeerIds']) && !empty($input['BeerIds'])) ? $input['BeerIds'] : false;
				$StatusIds 	= (isset($input['StatusIds']) && !empty($input['StatusIds'])) ? $input['StatusIds'] : false;
				
				if(!empty($BeerIds))
				{
					$beerStatusArray = array( "1" => "Brew this Beer" , "2" => "Brewery Matching" , "3" => "Brewing in Progress" , "4" => "Ready for Order" );
					
					for($i=0;$i<count($BeerIds);$i++)
					{
						$BeerData = Beers::where('id', $BeerIds[$i])->first(['user_id','beer_status']);
						$beerStatus = (!empty($BeerData) && isset($BeerData->beer_status)) ? $BeerData->beer_status : false;
						if(!empty($beerStatus) && $beerStatus != $StatusIds[$i])
						{
							Beers::where('id', $BeerIds[$i])->update(array("beer_status" => $StatusIds[$i]));
							
							$para['notification_type_id'] 	= 5; // for 5 Change Beer Status
							$para['sender_id'] 				= Auth::user()->id;
							$para['receiver_id'] 			= "-1";
							$para['action'] 				= "Beer Builder Change Beer Status";
							$para['action_id'] 				= $BeerIds[$i];
							$para['msg'] 					= "Change Beer Status form ".$beerStatusArray[$beerStatus] ." to ".$beerStatusArray[$StatusIds[$i]];
							
							app('App\Http\Controllers\NotificationController')->GenerateNotification($para);
						}							
					}
				}
			}
		}
		
		return response()->json(array('status'=> true), 200);
    }

	public function Messages(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			$data['user'] = User::join('users_roles','users_roles.user_id','=','users.id')
						->join('roles','roles.id','=','users_roles.role_id')
						->where('roles.slug',"=","administrator")
						->select('users.id','users.name','users.email','users.phone','users.address','users.notes','users.biography','users.img_unique_name','roles.name as role_name')->get();
			
			$data['contactList'] = Message::join('users','messages.sender_id','=','users.id')
									->where("messages.revicer_id",Auth::user()->id)
									->select('users.id','users.name','users.email','users.phone','users.address','users.notes','users.biography','users.img_unique_name','messages.message')
									->orderBy('messages.created_at','DESC')
									->get();
			
			return view('beer_builder/messages',['data' => $data]);
		}		
    }

	public function saveMessage(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			$array['sender_id'] = Auth::user()->id;
			$array['revicer_id'] = $request['revicerID']; 
			$array['message'] = $request['message-input'];  
			$array['flag'] = 'beer_builder';  
			Message::create($array);
			
			return response()->json(array('status'=> true, "msg" => "successfully"), 200);
		}		
    }
	
	public function loadChat(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('beer_builder'))
		{
			$id 	= Auth::user()->id;
			$html 	= "";
			$Messages = Message::where("revicer_id",$id)->orWhere("sender_id",$id)->get();
			Message::where("flag","admin")->Where(array( "revicer_id" => $id , "is_read" => 0 ))->update( array("is_read" => 1 ));
			
			if(!empty($Messages))
			{
				foreach($Messages as $key => $message)
				{
					if($message['flag'] == "beer_builder")
					{
						$html .= '<div class="UserChat-Left">
									<img src="asset/image/default-image.png" width="70" height="70" alt=""/>
									<div class="InnerChat-Head">
										<h1>'.$message['message'].'</h1>
										<h2>'.date_format($message['created_at'],"h:i A").'</h2>
									</div>
								</div>';
					}
					else
					{
						$html .= '<div class="UserChat-Right">
									<img src="assets/logo.png" width="70" height="70" alt=""/>
									<div class="InnerReply-Head">
										<h1>'.$message['message'].'</h1>
										<h2>'.date_format($message['created_at'],"h:i A").'</h2>
									</div>
								</div>';
					}
				}
			}
			else
			{
				$html .= '<div class="UserChat-Left emptyMessage">
							<div class="InnerChat-Head">
								<h1>No Messages</h1>
							</div>
						</div>';
			}
			return response()->json(array('status'=> true, "html" => $html ), 200);
		}		
    }
	
	public function loadContact(Request $request)
    {
		$user = $request->user();
		if($user->hasRole('administrator'))
		{
			$id 	= Auth::user()->id;
			$html 	= "";
			$contactData =  Message::join('users','messages.revicer_id','=','users.id')
									->where("flag","=","beer_builder")
									->select('users.id','users.name','users.email','users.phone','users.address','users.notes','users.biography','users.img_unique_name','messages.message','messages.is_read','messages.flag')
									->orderBy('messages.created_at','DESC')
									->get();
			
			if(!empty($contactData))
			{
				$contactListArray = array();
				foreach($contactData as $key => $contactList)
				{
					
					if(in_array($contactList['id'],$contactListArray))
						continue;
							
					$className = (empty($contactListArray)) ? "active" : "";
					
					$contactListArray[] = $contactList['id'];
					
					$messageData =  Message::where("revicer_id",$contactList['id'])
											->orWhere("sender_id",$contactList['id'])
											->orderBy('messages.id','DESC')
											->limit(1)
											->first();

					$highlight 		= (!empty($messageData) && isset($messageData['is_read']) && empty($messageData['is_read']) && $messageData['flag'] != "beer_builder") ? "highlight" : "";
					$lastMessage 	= (!empty($messageData) && isset($messageData['message']) && !empty($messageData['message'])) ? $messageData['message'] : $contactList->message;
					
					$html 	.= '<li class="'.$className.'" data-id="'.$contactList->id.'" id="li'.$contactList->id.'">
								<a href="javascript:void(0)" onclick="loadMessage('.$contactList->id.')">
									<div class="ChatDropdown-Head">
										<img src="assets/logo.png" width="36" height="36" alt=""/>
										<h1 class="'.$highlight.'">'.$contactList->name.'<br><span>'.$lastMessage.'</span></h1>
									</div>
								</a>
							</li>';
				}
			}
			else
			{
				$html .= '<li class="emptyContactList">
							<a href="#">
								<div class="ChatDropdown-Head">
									<h1><span>Empty Contact List</span></h1>
								</div>
							</a>
						</li>';
			}
			
			return response()->json(array('status'=> true, "html" => $html ), 200);
		}		
    }

}
