<?php

use Illuminate\Database\Seeder;

class brewerInspirationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brewers_inspiration')->insert([
            ['name' => 	'TRADITIONAL'],
            ['name' => 	'UNIQUE'],
            ['name' => 	'EXTREME']
            ]);
    }
}
