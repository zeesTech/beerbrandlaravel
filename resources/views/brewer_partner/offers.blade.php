@extends('layouts.app')

@section('content')
<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    #table_id thead {
        background-color: #FF6000;
        color: white;
    }

    #table_id thead tr {
        height: 60px;
    }
    #table_id .even { background-color: white }
    #table_id .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    #table_id tbody tr {
        height: 60px;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Pending Offers</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Pending Offers</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="table_id" width="100%" cellspacing="0">
        <thead>
          <tr>
			<th>Sr# </th>
			<th>Batch Name</th>
			<th>Beer Type</th>
			<th>Brewers Motivation</th>
			<th>Batch Size</th>
			<th>Action</th>
          </tr>
        </thead>
       
        <tbody>
		<?php
			if(!empty($data))
			{
				$i = 0;
				foreach($data as $index => $dt)
				{
					$i++; 
					$beer_total_size = 0;
					$beer_total_size_text = 0;
					$total_keg_gallons = 31.5;
					$batch_size = (!empty($dt->batch_size)) ? $dt->batch_size : 0;
					$beer_json = (!empty($dt->beer_ids)) ? json_decode($dt->beer_ids) : 0;
					if(!empty($beer_json))
					{
						$beer_total_size = DB::table('beers')->whereIn('id',$beer_json)->sum('beer_size');
						$beer_total_size_text = round(( $beer_total_size / $total_keg_gallons ),2) . " barrels / " . $beer_total_size ." Gallons";
					}
		?>
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$dt->name}}</td>
                        <td>{{$dt->beer_style_name}}</td>
                        <td>{{$dt->brewers_inspiration_name}}</td>
                        <td>{{$beer_total_size_text}}</td>
						<td><a class="customBrew-Btn" onclick="AcceptOffer(this)" data-beerID="{{$dt->id}}" href="javascript:void(0)">Accept This Offer</a></td>
                    </tr>
			<?php }
			}
		  ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>

<script>
    $(document).ready( function () {
        $('#table_id').DataTable({
            searching: false, info: false,
            aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ -1 ]
                            }
            ]
        });
    } );
	
	function AcceptOffer(e)
	{
		var id = $(e).attr('data-beerID');
		if(id == '')
			return false;
			
		if (confirm('Do you want to accept this offer?')) {
			
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': "{{ csrf_token() }}"
				}
			});
			
			
			var type = "POST";
			var ajaxurl = '/AcceptOffer';
			$.ajax({
				type: type,
				url: ajaxurl,
				data: { "batch_id" : id },
				dataType: 'json',
				success: function (data) {
					if(data.status)
					{
						location.reload();
					}
				},
				error: function (data) {
					console.log(data);
				}
			});	
		}
	}
	
</script>
@endsection
