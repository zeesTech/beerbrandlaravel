@extends('layouts.app')

@section('content')

 <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Optional JavaScript -->

<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    #table_id thead {
        background-color: #FF6000;
        color: white;
    }

    #table_id thead tr {
        height: 60px;
    }
    #table_id .even { background-color: white }
    #table_id .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    #table_id tbody tr {
        height: 60px;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
	<div class="customContainer">
                
		<div class="row" >
			<h2>Customers</h2>
		</div>

		<div class="row tabledesign">
			<table class="table table-bordered" id="table_id" class="display" style="width:100%">
				<thead>
					<tr>
						<th>Name</th>
						<th>Restaurant Name</th>
						<th>Address</th>
						<th>Phone</th>
						<th>Email</th>
						<th>Beer Portfolio</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
			$i = 0;
			foreach($data as $dt):
			$i++;
				
				$beerData = \App\Beers::where([ 'user_id' => $dt->id , 'status' => 1 ])->get(DB::raw('COUNT(beers.id) AS brewed_beers'));
				$brewed_beers = (!empty($beerData) && isset($beerData[0]) && !empty($beerData[0]->brewed_beers)) ? $beerData[0]->brewed_beers : "-";
		?>
                    <tr>
                        <td>{{$dt->name}}</td>
                        <td></td>
                        <td></td>
						<td>{{$dt->phone}}</td>
                        <td>{{$dt->email}}</td>
                        <td>{{$brewed_beers}}</td>
                        <td>
							<div class="dropdown no-arrow">
								<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink<?php echo $i;?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink<?php echo $i;?>">
								  <a class="dropdown-item" href="javascript:void(0)">Send Message</a>
								  <a class="dropdown-item" href="javascript:void(0)" data-id="<?php echo $i;?>">Delete Customer</a>
								  <a class="dropdown-item" href="javascript:void(0)">Modify Info</a>
								</div>
							</div>
						</td>
                    </tr>
            <?php endforeach; ?>
				</tbody>
			</table>
		</div>
    </div>
</div>

<script>
    $(document).ready( function () {
        $('#table_id').DataTable({
            searching: false, info: false,
            aoColumnDefs: [
				{
					bSortable: false,
					aTargets: [ -1 ]
				}
            ]
        });
    } );
</script>
@endsection
