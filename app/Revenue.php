<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Revenue extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'home_brewer_id', 'brewer_partner_id', 'batch_id', 'beer_style_id','reciepe_id','tank_id','keg_1_2_size_counter','keg_1_2_size_beerIDs','keg_1_2_size_revenue','keg_1_6_size_counter','keg_1_6_size_beerIDs','keg_1_6_size_revenue','status','createdBy','updatedBy'
    ];
}
