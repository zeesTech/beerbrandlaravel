<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Revenue;
use App\Beer_cost;
use App\Batchorder;

class RevenueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

	public function CreateRevenueByOrder($para)
	{
		$batch_id 			= (!empty($para) && isset($para['batch_id']) && !empty($para['batch_id'])) ? $para['batch_id'] : 0;
		$kegs				= (!empty($para) && isset($para['kegs']) && !empty($para['kegs'])) ? $para['kegs'] : 0;
		$beer_style_id		= (!empty($para) && isset($para['beer_style_id']) && !empty($para['beer_style_id'])) ? $para['beer_style_id'] : 0;
		$reciepe_id			= (!empty($para) && isset($para['reciepe_id']) && !empty($para['reciepe_id'])) ? $para['reciepe_id'] : 0;
		$home_brewer_id		= (!empty($para) && isset($para['home_brewer_id']) && !empty($para['home_brewer_id'])) ? $para['home_brewer_id'] : 0;
		$brewer_partner_id	= (!empty($para) && isset($para['brewer_partner_id']) && !empty($para['brewer_partner_id'])) ? $para['brewer_partner_id'] : 0;
		$createdBy			= (!empty($para) && isset($para['createdBy']) && !empty($para['createdBy'])) ? $para['createdBy'] : 0;
		
		if(!empty($batch_id) && !empty($reciepe_id) && !empty($kegs) && !empty($beer_style_id) && !empty($home_brewer_id) && !empty($brewer_partner_id))
		{
			
			$data['batch_id'] 			= $batch_id; 
			$data['beer_style_id'] 		= $beer_style_id;
			$data['reciepe_id'] 		= $reciepe_id;
			$data['home_brewer_id'] 	= $home_brewer_id;
			$data['brewer_partner_id'] 	= $brewer_partner_id;
			$data['createdBy'] 			= $createdBy;
			$data['updatedBy'] 			= $createdBy;
			$data['tank_id'] 			= 0;
			
			if(isset($kegs[1]) && !empty($kegs[1]))
			{
				$keg_1_2_size_counter = count($kegs[1]);
				$keg_1_2_size_beerIDs = json_encode($kegs[1]);
				$keg_1_2_size_revenue = $keg_1_2_size_counter * 6;
				
				$data['keg_1_2_size_counter'] 	= $keg_1_2_size_counter;
				$data['keg_1_2_size_beerIDs'] 	= $keg_1_2_size_beerIDs;
				$data['keg_1_2_size_revenue'] 	= $keg_1_2_size_revenue;
			}	
			
			if(isset($kegs[2]) && !empty($kegs[2]))
			{
				$keg_1_6_size_counter = count($kegs[2]);
				$keg_1_6_size_beerIDs = json_encode($kegs[2]);
				$keg_1_6_size_revenue = $keg_1_6_size_counter * 2;
				
				$data['keg_1_6_size_counter'] 	= $keg_1_6_size_counter;
				$data['keg_1_6_size_beerIDs'] 	= $keg_1_6_size_beerIDs;
				$data['keg_1_6_size_revenue'] 	= $keg_1_6_size_revenue;
			}			
			
			Revenue::create($data);
		}
	}
	
 	public function CreateHomeBrewerRevenueByOrder($para)
	{
		$batch_id 			= (!empty($para) && isset($para['batch_id']) && !empty($para['batch_id'])) ? $para['batch_id'] : 0;
		$kegs				= (!empty($para) && isset($para['kegs']) && !empty($para['kegs'])) ? $para['kegs'] : 0;
		$beer_style_id		= (!empty($para) && isset($para['beer_style_id']) && !empty($para['beer_style_id'])) ? $para['beer_style_id'] : 0;
		$reciepe_id			= (!empty($para) && isset($para['reciepe_id']) && !empty($para['reciepe_id'])) ? $para['reciepe_id'] : 0;
		$home_brewer_id		= (!empty($para) && isset($para['home_brewer_id']) && !empty($para['home_brewer_id'])) ? $para['home_brewer_id'] : 0;
		$brewer_partner_id	= (!empty($para) && isset($para['brewer_partner_id']) && !empty($para['brewer_partner_id'])) ? $para['brewer_partner_id'] : 0;
		$createdBy			= (!empty($para) && isset($para['createdBy']) && !empty($para['createdBy'])) ? $para['createdBy'] : 0;
		
		if(!empty($batch_id) && !empty($reciepe_id) && !empty($kegs) && !empty($beer_style_id) && !empty($home_brewer_id) && !empty($brewer_partner_id))
		{
			
			$data['batch_id'] 			= $batch_id; 
			$data['beer_style_id'] 		= $beer_style_id;
			$data['reciepe_id'] 		= $reciepe_id;
			$data['home_brewer_id'] 	= $home_brewer_id;
			$data['brewer_partner_id'] 	= 0;
			$data['createdBy'] 			= $createdBy;
			$data['updatedBy'] 			= $createdBy;
			$data['tank_id'] 			= 0;
			
			if(isset($kegs[1]) && !empty($kegs[1]))
			{
				$keg_1_2_size_counter = count($kegs[1]);
				$keg_1_2_size_beerIDs = json_encode($kegs[1]);
				$keg_1_2_size_revenue = $keg_1_2_size_counter * 6;
				
				$data['keg_1_2_size_counter'] 	= $keg_1_2_size_counter;
				$data['keg_1_2_size_beerIDs'] 	= $keg_1_2_size_beerIDs;
				$data['keg_1_2_size_revenue'] 	= $keg_1_2_size_revenue;
			}	
			
			if(isset($kegs[2]) && !empty($kegs[2]))
			{
				$keg_1_6_size_counter = count($kegs[2]);
				$keg_1_6_size_beerIDs = json_encode($kegs[2]);
				$keg_1_6_size_revenue = $keg_1_6_size_counter * 2;
				
				$data['keg_1_6_size_counter'] 	= $keg_1_6_size_counter;
				$data['keg_1_6_size_beerIDs'] 	= $keg_1_6_size_beerIDs;
				$data['keg_1_6_size_revenue'] 	= $keg_1_6_size_revenue;
			}			
			
			Revenue::create($data);
		}
	}
	
 	public function CreateBreweryPartnerRevenueByOrder($para)
	{
		$batch_id 			= (!empty($para) && isset($para['batch_id']) && !empty($para['batch_id'])) ? $para['batch_id'] : 0;
		$kegs				= (!empty($para) && isset($para['kegs']) && !empty($para['kegs'])) ? $para['kegs'] : 0;
		$beer_style_id		= (!empty($para) && isset($para['beer_style_id']) && !empty($para['beer_style_id'])) ? $para['beer_style_id'] : 0;
		$reciepe_id			= (!empty($para) && isset($para['reciepe_id']) && !empty($para['reciepe_id'])) ? $para['reciepe_id'] : 0;
		$home_brewer_id		= (!empty($para) && isset($para['home_brewer_id']) && !empty($para['home_brewer_id'])) ? $para['home_brewer_id'] : 0;
		$brewer_partner_id	= (!empty($para) && isset($para['brewer_partner_id']) && !empty($para['brewer_partner_id'])) ? $para['brewer_partner_id'] : 0;
		$createdBy			= (!empty($para) && isset($para['createdBy']) && !empty($para['createdBy'])) ? $para['createdBy'] : 0;
		
		if(!empty($batch_id) && !empty($reciepe_id) && !empty($kegs) && !empty($beer_style_id) && !empty($home_brewer_id) && !empty($brewer_partner_id))
		{
			$batchData = Batchorder::where('id', $batch_id)->first(['keg_1_2_size_beer_cost','keg_1_6_size_beer_cost','total_cost']);
			$keg_1_2_size_beer_cost = (!empty($batchData) && isset($batchData['keg_1_2_size_beer_cost']) && !empty($batchData['keg_1_2_size_beer_cost'])) ? $batchData['keg_1_2_size_beer_cost'] : 0;
			$keg_1_6_size_beer_cost = (!empty($batchData) && isset($batchData['keg_1_6_size_beer_cost']) && !empty($batchData['keg_1_6_size_beer_cost'])) ? $batchData['keg_1_6_size_beer_cost'] : 0;
			$total_cost 			= (!empty($batchData) && isset($batchData['total_cost']) && !empty($batchData['total_cost'])) ? $batchData['total_cost'] : 0;
			
			$keg_1_2_size_costData = Beer_cost::where(array("beer_style_id" => $beer_style_id , "beer_keg_size_id" => 1 ))->first(['cost','rate']);
			$keg_1_6_size_costData = Beer_cost::where(array("beer_style_id" => $beer_style_id , "beer_keg_size_id" => 2 ))->first(['cost','rate']);
			
			$keg_1_2_size_costRate 	= (!empty($keg_1_2_size_costData) && isset($keg_1_2_size_costData['rate']) && !empty($keg_1_2_size_costData['rate'])) ? $keg_1_2_size_costData['rate'] : 0;
			$keg_1_6_size_costRate 	= (!empty($keg_1_6_size_costData) && isset($keg_1_6_size_costData['rate']) && !empty($keg_1_6_size_costData['rate'])) ? $keg_1_6_size_costData['rate'] : 0;
			
			$data['batch_id'] 			= $batch_id; 
			$data['beer_style_id'] 		= $beer_style_id;
			$data['reciepe_id'] 		= $reciepe_id;
			$data['home_brewer_id'] 	= 0;
			$data['brewer_partner_id'] 	= $brewer_partner_id;
			$data['createdBy'] 			= $createdBy;
			$data['updatedBy'] 			= $createdBy;
			$data['tank_id'] 			= 0;
			
			if(isset($kegs[1]) && !empty($kegs[1]))
			{
				$keg_1_2_size_counter = count($kegs[1]);
				$keg_1_2_size_beerIDs = json_encode($kegs[1]);
				$keg_1_2_size_revenue = 0;
				if(!empty($keg_1_2_size_beer_cost) && !empty($keg_1_2_size_costRate))
					$keg_1_2_size_revenue = round(($keg_1_2_size_beer_cost * $keg_1_2_size_costRate)/100,2);
				
				$data['keg_1_2_size_counter'] 	= $keg_1_2_size_counter;
				$data['keg_1_2_size_beerIDs'] 	= $keg_1_2_size_beerIDs;
				$data['keg_1_2_size_revenue'] 	= $keg_1_2_size_revenue;
			}	
			
			if(isset($kegs[2]) && !empty($kegs[2]))
			{
				$keg_1_6_size_counter = count($kegs[2]);
				$keg_1_6_size_beerIDs = json_encode($kegs[2]);
				$keg_1_6_size_revenue = 0;
				if(!empty($keg_1_6_size_beer_cost) && !empty($keg_1_6_size_costRate))
					$keg_1_6_size_revenue = round(($keg_1_6_size_beer_cost * $keg_1_6_size_costRate)/100,2);
				
				$data['keg_1_6_size_counter'] 	= $keg_1_6_size_counter;
				$data['keg_1_6_size_beerIDs'] 	= $keg_1_6_size_beerIDs;
				$data['keg_1_6_size_revenue'] 	= $keg_1_6_size_revenue;
			}			
			
			Revenue::create($data);
		}
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
