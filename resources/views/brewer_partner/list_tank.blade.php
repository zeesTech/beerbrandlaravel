@extends('layouts.app')

@section('content')
<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    #table_id thead {
        background-color: #FF6000;
        color: white;
    }

    #table_id thead tr {
        height: 60px;
    }
    #table_id .even { background-color: white }
    #table_id .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    #table_id tbody tr {
        height: 60px;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">List Tank</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">List Tank</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="table_id" width="100%" cellspacing="0">
        <thead>
          <tr>
            <!-- <th>Quantity</th> -->
            <th>ID</th>
            <th>Tank Size (BBL)</th>
            <!--<th>Tank Available Date</th>-->
            <th>Tank Use Frequency</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
       
        <tbody>
		@if(!empty($data))
		@php $i = 0; @endphp
        @foreach($data as $index => $dt)
		@php 
			$i++; 
			$tankFreq = "";
			if($dt->tank_freq == 1)
				$tankFreq = "Single Use";
			else if($dt->tank_freq == 2)
				$tankFreq = "Continuous Use";
				
			$tankStaus = "Unavailable";
			$className = "Deliverdeliver-Btn";
			$actionBtn = "-";
			if($dt->tank_status == 1)
			{
				$tankStaus = "Available";
				$className = "DeliverComplete-Btn";
			}
				
			$tankDate = '-';
			if(!empty($dt->tank_date))
				$tankDate = date("F d,Y", strtotime($dt->tank_date));
				
			$beer_total_size_text = 0;
			$total_keg_gallons = 31.5;
			$beer_total_size_text =  $dt->tank_size . " barrels / " . round(( $dt->tank_size * $total_keg_gallons ),2) ." Gallons";
					
		@endphp
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$beer_total_size_text}}</td>
                        <!--<td>{{$tankDate}}</td>-->
						<td>{{$tankFreq}}</td>
						<td><div class="{{$className}}"><button>{{$tankStaus}}</button></div></td>
						<td>
							<div class="dropdown no-arrow">
								<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink<?php echo $i;?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink<?php echo $i;?>">
									<a class="dropdown-item" style="color:#FF6000;" href="{{ route('edit_tank',['id' => $dt->id]) }}"><i class="fas fa-fw fa-edit"></i> Edit</a>
									<a class="dropdown-item" style="color:#FF6000;" href="{{ route('delete_tank',['id' => $dt->id]) }}"><i class="fas fa-fw fa-trash"></i> Delete</a>
								</div>
							</div>
						</td>
                    </tr>
                    @endforeach
		@endif
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>

<script>
    $(document).ready( function () {
        $('#table_id').DataTable({
            searching: false, info: false,
            aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ -1 ]
                            }
            ]
        });
    } );
</script>
@endsection
