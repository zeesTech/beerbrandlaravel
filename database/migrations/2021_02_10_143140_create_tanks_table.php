<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tanks', function (Blueprint $table) {
            $table->id();
			$table->unsignedInteger('user_id');
            $table->Integer('tank_size')->nullable();
            $table->Integer('tank_cost')->nullable();
            $table->date('tank_date')->nullable();
            $table->Integer('tank_freq')->nullable();
            $table->Integer('tank_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tanks');
    }
}
