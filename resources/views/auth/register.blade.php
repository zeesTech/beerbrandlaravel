@extends('layouts.app_old1')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.field-icon {
	float: right;
	margin-right: 10px;
	margin-top: -35px;
	position: relative;
	z-index: 2;
}
.customAdd-Btn{
	margin: 0px 12px 0 12px;
	padding: 10px 18px;
	background: #FF6000;
	border-radius: 10px;
	border: none;
	outline: none;
	color: #fff;
	font-weight: 900;
	font-size: 18px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
}
.customAdd-Btn:hover{
	background: #fff;
	color: #FF6000;
	text-decoration: none;
}
</style>
<div class="container-fluid">
    <div class="row no-gutter">
        <div class="d-none d-md-flex col-md-4 col-lg-6 signup bg-image"><img src="{{ URL::asset('assets/LogoWhite.png')}}" alt=""></div>
        <div class="col-md-8 col-lg-6">
        <div class="SignUp" style="padding: 21px;">
            <span style="float: right;">Already have an account? <a href="{{ route('login')}}">LOGIN</a></span>
        </div>
        <div class="login d-flex align-items-center py-5">
            <div class="container">
            <div class="row">
                <div class="col-md-9 col-lg-8 mx-auto">
                <h3 class="login-heading mb-4">Sign Up</h3>
                <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                @csrf
                    <div class="form-label-group">
                        <input type="text" id="fname" name="fname" class="form-control @error('fname') is-invalid @enderror" placeholder="First Name" value="{{ old('fname') }}"  required autofocus>
                        <label for="fname">First Name</label>     
                    </div>
					
                    <div class="form-label-group">
                        <input type="text" id="lname" name="lname" class="form-control @error('lname') is-invalid @enderror" placeholder="Last Name" value="{{ old('lname') }}"  required autofocus>
                        <label for="lname">Last Name</label>     
                    </div>
					
					@if($errors->has('email'))
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
						  <strong>Error!</strong> {{ $errors->first('email') }}
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						  </button>
						</div>
					@endif
					
                    <div class="form-label-group">
						<input type="email" id="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email Address" value="{{ old('email') }}" required autofocus>
						<label for="email">Email address</label>
                    </div>
    
					<div class="form-label-group">
						<select class="form-control" placeholder="Role" name="roleName" required>
							<option>Select Role</option>
							<?php
								$roles = \App\Role::all();
								if(!empty($roles))
								{
									foreach($roles as $role):

										$selected = '';
										if(old('roleName') == $role['slug'])
											$selected = 'selected';
											
										if( $role['slug'] != "administrator" && $role['slug'] != "home_brewer_partner" ):
							?>
											<option value="<?php echo $role['slug']; ?>" <?php echo $selected; ?>><?php echo $role['name']; ?></option>
							<?php
										endif;
									endforeach;
								}
							?>
						</select>
                    </div>
					
					@if($errors->has('password'))
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
						  <strong>Error!</strong> {{ $errors->first('password') }}
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						  </button>
						</div>
					@endif
						
                    <div class="form-label-group">
						<input type="password" id="password" data-action="hide" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required>
						<span toggle="#password" id="passwordIcon" class="fa fa-fw fa-eye field-icon toggle-password" onclick="myFunction('password')"></span>
						<label for="password">Password</label>
                    </div>

                    <div class="form-label-group">
						<input type="password" id="password-confirm" name="password_confirmation" class="form-control" placeholder="Verify Password" required>
						<span toggle="#password-confirm" id="password-confirmIcon" class="fa fa-fw fa-eye field-icon toggle-password" onclick="myFunction('password-confirm')"></span>
						<label for="password-confirm">Verfiy Password</label>
                    </div>
    
					<div class="form-label-group">
						<input type="file" id="image" name="image" class="form-control" placeholder="image" onchange="allowonlyImg(this)" >
						<label for="image">Upload Image</label>
                    </div>
					
                    <div class="custom-control custom-checkbox mb-3" style="margin-top: 1rem!important;margin-bottom: 2rem!important;">
						<input type="checkbox" class="custom-control-input" id="customCheck1">
						<label class="custom-control-label" for="customCheck1" onclick="openPopup()">Agree to terms and conditions</label>
                    </div>
                    <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">{{ __('Register') }}</button>
                </form>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>

	<div class="modal fade in" id="TermsConditionsPopup" data-backdrop="static">
	  <div class="modal-dialog modal-lg">
		<div class="modal-content">
		  <div class="modal-header">
			<h4 class="modal-title" id="modalTitle">Terms and Conditions</h4>
			<button type="button" class="close" onclick="closePopup()" aria-label="Close"><span aria-hidden="true">×</span></button>
		  </div>
		  <div class="modal-body">
			<div class="row">
				<div class="col-md-12" >
					<div class="form-group" >
						<?php
							echo \App\Content::where(['flag' => "termsConditions"])->first()->content;
						?>
					</div>
				</div>
			</div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="customAdd-Btn" onclick="closePopup()">Close</button>
		  </div>
		</div>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>

@endsection

<script>
function openPopup()
{
	if($("#customCheck1").prop("checked") == false)
		$("#TermsConditionsPopup").modal('show');
}
function closePopup()
{
	$("#TermsConditionsPopup").modal('hide');
}

function myFunction(id) {
	var x = document.getElementById(id);
	if (x.type === "password") {
		$("#"+id+"Icon").addClass('fa-eye-slash');
		$(x).attr('data-action','show');
		x.type = "text";
	} else {
		$("#"+id+"Icon").removeClass('fa-eye-slash');
		$(x).attr('data-action','hide');
		x.type = "password";
	}
}
</script>
