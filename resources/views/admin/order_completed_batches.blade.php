@extends('layouts.app')

@section('content')

 <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Optional JavaScript -->

<!-- Custom styles for this page -->
<link href="asset/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugins -->
<script src="asset/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="asset/vendor/datatables/dataTables.bootstrap4.min.js"></script>
   
<style>
    .btn-login {
        width: auto !important;
        height: 53px;
        margin-top: 50px;
    }
    .logo {
        text-align: center;
        display: block;
                    margin-left: auto;
                    margin-right: auto;    width: auto !important;
    }
    .logo > img {
        width: 50%;
    }
    .dataTables_wrapper {
        width: 100%;
    }

    .table thead {
        background-color: #FF6000;
        color: white;
    }

    .table thead tr {
        height: 60px;
    }
    .table .even { background-color: white }
    .table .odd { background-color: white }
    table.dataTable.display tbody tr.odd > .sorting_1, table.dataTable.order-column.stripe tbody tr.odd > .sorting_1
    {background-color: white}

    table.dataTable.display tbody tr.even > .sorting_1, table.dataTable.order-column.stripe tbody tr.even > .sorting_1 {
        background-color: white 
    }
    .table tbody tr {
        height: 60px;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
	<div class="customContainer">
                
		<div class="row" >
			<div class="col-md-9">
				<h2>Completed Batches</h2>
			</div>
		</div>
		
		<form action="javascript:void(0)" method="post" id="OrderFilter">
			<div class="row" >
				<div class="col-md-2" >
					<div class="form-group" >
						<select class="form-control formdata" placeholder="Beer Style" name="beer_style_id" id="beer_style_id">
							<option value="" >Beer Style</option>
							@foreach($filter_data['beer_styles'] as $dt)
								<option value="{{$dt['id']}}">{{$dt['name']}}</option>
							@endforeach
						</select>
                    </div>
				</div>
				
				<div class="col-md-2" >
					<div class="form-group" >
						<button class="customAdd-Btn" type="submit" onclick="filter()">Filter</button>
                    </div>
				</div>
			</div>
		</form>
		
			
		<div class="row tabledesign">
			<table class="table table-bordered" id="table_id" class="display" style="width:100%">
				<thead>
					<tr>
						<th>Sr# </th>
						<th>Batch Name</th>
						<th>Beer Type</th>
						<th>Brewer Assigned</th>
						<th>Date Completed</th>
						<th>Recipe</th>
						<th>Notes</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="OrderTbody">
					
				</tbody>
			</table>
		</div>
    </div>

</div>

	<div class="modal fade in" id="DetailPopup" data-backdrop="static">
	  <div class="modal-dialog modal-xl">
			<div class="modal-content">
			  <div class="modal-header">
				<h4 class="modal-title" id="modalTitle">Partner Detail</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <div class="modal-body">
				<div class="row">
					<div class="col-md-12" >
						<div class="table-responsive">
						  <table class="table table-bordered" id="Detailtable" width="100%" cellspacing="0">
							<thead>
							  <tr>
								<th>Partner Name</th>
								<th>Partner Email</th>
								<th>Partner Phone</th>
							  </tr>
							</thead>
							<tbody id="Detailtbody">
							
							</tbody>
						  </table>
						</div>
					</div>
				</div>
			  </div>
			</div>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>
	
	<div class="modal fade in" id="BeerDetailPopup" data-backdrop="static">
	  <div class="modal-dialog modal-xl">
			<div class="modal-content">
			  <div class="modal-header">
				<h4 class="modal-title" id="BeermodalTitle">Beer Detail</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <div class="modal-body">
				<div class="row">
					<div class="col-md-12" >
						<div class="table-responsive">
						  <table class="table table-bordered" id="BeerDetailtable" width="100%" cellspacing="0">
							<thead>
							  <tr>
								<th>Beer Name</th>
								<th>Beer Style</th>
								<th>Quantity</th>
								<th>Keg Size</th>
							  </tr>
							</thead>
							<tbody id="BeerDetailtbody">
							
							</tbody>
						  </table>
						</div>
					</div>
				</div>
			  </div>
			</div>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>



<script>

var OrderTable_obj = "";
function OrderTable_Init(html = '')
{
	if(OrderTable_obj != '' && OrderTable_obj != null)
	{
		$('#table_id').dataTable().fnDestroy();
		$('#table_id tbody').empty();
		OrderTable_obj = '';
	}
	
	if(html !='')
		$('#table_id tbody').html(html);
	
	OrderTable_obj = $('#table_id').DataTable({
		searching: false, 
		info: false,
		aoColumnDefs: [
			{
				bSortable: false,
				aTargets: [ 0 ]
			}
		],
	});
}

$(document).ready( function () {
        
	$('.all-checkbox').click(function(){
		if($(this).prop("checked") == true){
			$(".single-checkbox").prop("checked",true);
		}
		else if($(this).prop("checked") == false){
			$(".single-checkbox").prop("checked",false);
		}
	});
	
	orderFilter();
});
	
function orderFilter()
{	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var type = "POST";
	var ajaxurl = '/orderCompletedBatchesFilter';
	$.ajax({
		type: type,
		url: ajaxurl,
		dataType: 'json',
		success: function (data) {
			OrderTable_Init(data.html);
		},
		error: function (data) {
			console.log(data);
		}
	});		
}

function filter()
{
	if($("#beer_style_id").val() == '')
	{
		alert("Please Select Beer Style dropdown");
		return false;
	}
	
	var check = false;
	$(".formdata").each(function (item){
		if($(this).val() != "")
		{
			if(!check)
				check = true;
		}
	});
	
	if(!check)
	{
		alert("Please Select atleast one dropdown");
		return false;
	}
			
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var formData = $("#OrderFilter").serialize();		
	var type = "POST";
	var ajaxurl = '/orderCompletedBatchesFilter';
	$.ajax({
		type: type,
		url: ajaxurl,
		data: formData,
		dataType: 'json',
		success: function (data) {
			OrderTable_Init(data.html);
			$("#createBatch-Btn").show();
		},
		error: function (data) {
			console.log(data);
			$("#createBatch-Btn").hide();
		}
	});	
}

function resetFilter()
{
	$(".all-checkbox").prop("checked",false);
	$("#OrderFilter")[0].reset();
	$("#createBatch-Btn").hide();
}

function getPartnerInfo(e)
{
	return false;
	var name = $(e).attr("data-name");
	var email = $(e).attr("data-email");
	var phone = $(e).attr("data-phone");
	$("#Detailtbody").html('');	
	var TrHtml = '<tr><td>'+name+'</td><td>'+email+'</td><td>'+phone+'</td></tr>';		
	$("#Detailtbody").html(TrHtml);
	$("#DetailPopup").modal('show');
}

function getBeerInfo(e)
{
	var beerIDs = $(e).attr("data-beerIDs");
	if(beerIDs == '')
	{
		alert("Something went wrong");
		return false;
	}
			
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var type = "POST";
	var ajaxurl = '/loadOrderBeerList';
	$.ajax({
		type: type,
		url: ajaxurl,
		data: {"beerIDs" : beerIDs },
		dataType: 'json',
		success: function (data) {
			$("#BeerDetailtbody").html("");
			$("#BeerDetailtbody").html(data.html);
			$("#BeerDetailPopup").modal('show');
		},
		error: function (data) {
			console.log(data);
			$("#BeerDetailtbody").html("");
		}
	});
}

function backToBrewing(e)
{
	var BatchID = $(e).attr("data-id");
	if(BatchID == '')
	{
		alert("Something went wrong");
		return false;
	}
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': "{{ csrf_token() }}"
		}
	});
	
	var type = "POST";
	var ajaxurl = '/batchBackToBrewing';
	$.ajax({
		type: type,
		url: ajaxurl,
		data: {"batch_id" : BatchID },
		dataType: 'json',
		success: function (data) {
			orderFilter();
		},
		error: function (data) {
			console.log(data);
		}
	});
}

</script>
@endsection
